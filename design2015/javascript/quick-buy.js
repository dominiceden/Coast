$(document).ready(function(){

  /******* START OF util/simpleModal.js*******/

  /**
  * $.fn.simpleModal by: tkiddle
  *
  * - Built for use in various scenarios - not just quickbuy
  * - A modal window with ajax & non-ajax content support
  * - Ajax content: set ajax:true and supply an ajaxUrl
  * - Non-ajax content: supply contentObject with jQuery object i.e. $('<p>Example</p>')
  *
  */

  ;(function ($){

    $.fn.simpleModal = function (opts) {

      var	$b = $('body'),
        $s = $.extend({
          customClass:'',
          overlayId:'simple-overlay',
          overlayColor:'#000',
          overlayOpacity:'0.5',
          modalId:'simple-modal',
          ajax:false,
          ajaxUrl:null,
          ajaxElement:null,
          contentObject:null,
          closeLink:true,
          revealCallback:undefined
        }, opts);

      function windowWidth() {

        var windowWidth = 0;

        if (typeof(window.innerWidth) == 'number') {
          windowWidth = window.innerWidth;
        } else {

          if (document.documentElement && document.documentElement.clientWidth) {
            windowWidth = document.documentElement.clientWidth;
          } else {

            if (document.body && document.body.clientWidth) {
              windowWidth = document.body.clientWidth;
            }

          }
        }

        return windowWidth;
      };

      function buildModal(callback) {

        if (!$b.find('#' + $s.modalId).length){
          $('<div>', {id : $s.modalId, class: $s.customClass}).appendTo($b).css({
            display: 'none'
          })
          .append('<span class="simple-loader"></span><div class="simple-content"></div>');
        }

        if ($s.closeLink && !$b.find('#' + $s.closeLink).length) {
          $('#' + $s.modalId).prepend('<span class=simple-close>close</span>');
        }

        $('#' + $s.modalId).data({'active' : true});

        if (typeof callback === 'function') {
          callback();
        }
      };

      function buildOverlay(callback) {

        if (!$b.find('#' + $s.overlayId).length){
          $('<div>', {id : $s.overlayId}).appendTo($b);
        }
        reveal();

      };

      function getContent(callback) {

        var $content;

        if ($s.ajax) {

          //Ajax content
          if(!$s.ajaxUrl) {
            throw('Simple Modal: Ajax is set to true but no url has been supplied to make the request.');
          }

          $.ajax({
            url : $s.ajaxUrl,
            dataType: 'html',
            success: function (data) {

              $content = $(data);

              if ($s.ajaxElement) {
                $content = $content.find($s.ajaxElement);
              }

            },
            error: function () {
              $content = $('<h4>Something went wrong, please try again later.</h4>')
            },
            complete: function () {
              injectContent($content, function () {
                $('.simple-loader').hide();
                $('.simple-content').show();
              });
              if(typeof $s.revealCallback === 'function') {
                $s.revealCallback();
              }
            }

          });

        } else {

          // Standard content
          if(!$s.contentObject) {
            throw('Simple Modal: No jQuery contentObject has been supplied for the modal to render.');
          }

          $content = $($s.contentObject);

          injectContent($content, function () {
            $('.simple-loader').hide();
            $('.simple-content').show();
          });

        }



      };

      function injectContent(content, callback) {

        $('.simple-content').append(content);

        if(typeof callback === 'function') {
          callback();
        }

      };

      function setRemoveEvent() {

        $('.simple-close')
        .add('#' + $s.overlayId)
        .on('click', function () {
          $('#' + $s.modalId).hide();
          $('#' + $s.overlayId).fadeOut();
          $('.simple-content').empty();
          $('#' + $s.modalId).data({'active' : false});
        });
      };

      function reveal() {
        $('.simple-loader').css('display','block');
        $('#' + $s.overlayId).fadeIn();
        $('#' + $s.modalId).show();
      };


      return (function() {
        if ($('#' + $s.modalId).data('active') === true) {
          return false;
        }
        buildModal( function() {
          buildOverlay();
          getContent();
          setRemoveEvent();
        });

      })();

    };

  })(jQuery);
   /******* END OF util/simpleModal.js*******/

  $("a.quick-buy").unbind(); // Unbind all interfering click handlers on this element before initialising a new one below.

  $("a.quick-buy").on("click", function(event) {
      event.preventDefault();

      $.fn.simpleModal({
          ajax: true,
          ajaxUrl: $(this).attr("data-ajax-url"),
          customClass: "quickbuy-popup",
          revealCallback: function() {
              btf.basket.ajaxBasket();
              var c = $("[name=ProductID]");
              $("[name=ProductID]").change(function() {
                  c.not(this).parent().removeClass("size_selected");
                  $(this).parent().addClass("size_selected")
              });

              // Generate a link to the product page and append it to the bottom of the modal window.
              var quickBuyProductID = $("img.product_image").attr("src").split("/").splice(-3, 1); // Slice off product ID from product image filename.
              var linkToProduct = "http://www.coast-stores.com/coast/fcp-product/" + quickBuyProductID;
              $("form#add-to-bag").after('<a id="quick-buy-link-to-product" href="' + linkToProduct + '">View full details</a>');
          }
      });
      var a = $("#basket_response").clone();
      $("#basket_response").remove();
      a.after("#add-buttons");
  });


    /******* END OF Quick Buy modal AJAX *******/

});