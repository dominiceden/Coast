/*  Define the Coast SKU colour identification system.
    We'll use this to match the last two digits of product IDs with the IDs in this list to determine the product colour.

    Last updated on 11-03-2015.
*/

var coastColoursDatabase = [
    {"id":"00", "colour":"null", "parentColour":"null"},
    {"id":"01", "colour":"White", "parentColour":"White"},
    {"id":"02", "colour":"Almost White", "parentColour":"White"},

    {"id":"03", "colour":"Champagne", "parentColour":"Naturals"},
    {"id":"04", "colour":"Cream", "parentColour":"Naturals"},
    {"id":"05", "colour":"Neutral", "parentColour":"Naturals"},
    {"id":"06", "colour":"Ivory", "parentColour":"Naturals"},
    {"id":"07", "colour":"Oyster", "parentColour":"Naturals"},
    {"id":"08", "colour":"Pearl", "parentColour":"Naturals"},
    {"id":"09", "colour":"Natural", "parentColour":"Naturals"},
    {"id":"10", "colour":"Almond", "parentColour":"Naturals"},
    {"id":"11", "colour":"Cream 2", "parentColour":"Naturals"},


    {"id":"12", "colour":"Teal", "parentColour":"Blues"},
    {"id":"13", "colour":"Peacock", "parentColour":"Blues"},
    {"id":"14", "colour":"Azure", "parentColour":"Blues"},
    {"id":"15", "colour":"Blue", "parentColour":"Blues"},
    {"id":"16", "colour":"Duck Egg", "parentColour":"Blues"},
    {"id":"17", "colour":"Cornflower", "parentColour":"Blues"},
    {"id":"18", "colour":"French Navy", "parentColour":"Blues"},
    {"id":"19", "colour":"Midnight", "parentColour":"Blues"},
    {"id":"20", "colour":"Navy", "parentColour":"Blues"},
    {"id":"21", "colour":"Pale Blue", "parentColour":"Blues"},
    {"id":"22", "colour":"Cobalt Blue", "parentColour":"Blues"},
    {"id":"23", "colour":"Turq 2", "parentColour":"Blues"},
    {"id":"24", "colour":"Sky Blue", "parentColour":"Blues"},
    {"id":"25", "colour":"Turquoise", "parentColour":"Blues"},


    {"id":"26", "colour":"Beige", "parentColour":"Beiges Browns"},
    {"id":"27", "colour":"Biscuit", "parentColour":"Beiges Browns"},
    {"id":"28", "colour":"Porcini", "parentColour":"Beiges Browns"},
    {"id":"29", "colour":"Sand", "parentColour":"Beiges Browns"},
    {"id":"30", "colour":"Cappucino", "parentColour":"Beiges Browns"},
    {"id":"31", "colour":"Caramel", "parentColour":"Beiges Browns"},
    {"id":"32", "colour":"Chocolate", "parentColour":"Beiges Browns"},
    {"id":"33", "colour":"Gold", "parentColour":"Beiges Browns"},
    {"id":"34", "colour":"Mocha", "parentColour":"Beiges Browns"},
    {"id":"35", "colour":"Mink", "parentColour":"Beiges Browns"},
    {"id":"36", "colour":"Pebble", "parentColour":"Beiges Browns"},
    {"id":"37", "colour":"Tan", "parentColour":"Beiges Browns"},
    {"id":"38", "colour":"Mole", "parentColour":"Beiges Browns"},


    {"id":"39", "colour":"Grey", "parentColour":"Greys"},
    {"id":"40", "colour":"Gun Metal", "parentColour":"Greys"},
    {"id":"41", "colour":"Silver", "parentColour":"Greys"},
    {"id":"42", "colour":"Smoke", "parentColour":"Greys"},


    {"id":"43", "colour":"Forest", "parentColour":"Greens"},
    {"id":"44", "colour":"Emerald", "parentColour":"Greens"},
    {"id":"45", "colour":"Green", "parentColour":"Greens"},
    {"id":"46", "colour":"Bright Green", "parentColour":"Greens"},
    {"id":"47", "colour":"Peppermint", "parentColour":"Greens"},
    {"id":"48", "colour":"Jade", "parentColour":"Greens"},
    {"id":"49", "colour":"Verdigris", "parentColour":"Greens"},
    {"id":"50", "colour":"Kingfisher", "parentColour":"Greens"},
    {"id":"51", "colour":"Lime", "parentColour":"Greens"},
    {"id":"52", "colour":"Mint", "parentColour":"Greens"},
    {"id":"53", "colour":"Olive", "parentColour":"Greens"},
    {"id":"54", "colour":"Soft Green", "parentColour":"Greens"},
    {"id":"55", "colour":"Thyme", "parentColour":"Greens"},


    {"id":"56", "colour":"Coral", "parentColour":"Oranges"},
    {"id":"57", "colour":"Fuchsia", "parentColour":"Pinks"},
    {"id":"58", "colour":"Orange", "parentColour":"Oranges"},
    {"id":"59", "colour":"Bronze", "parentColour":"Oranges"},

    {"id":"60", "colour":"Pink", "parentColour":"Pinks"},
    {"id":"61", "colour":"Raspberry", "parentColour":"Pinks"},
    {"id":"62", "colour":"Watermelon", "parentColour":"Pinks"},
    {"id":"63", "colour":"Peach", "parentColour":"Pinks"},
    {"id":"64", "colour":"Dusky Pink", "parentColour":"Pinks"},
    {"id":"65", "colour":"Old Rose", "parentColour":"Pinks"},
    {"id":"66", "colour":"Hot Pink", "parentColour":"Pinks"},
    {"id":"67", "colour":"Rose", "parentColour":"Pinks"},

    {"id":"68", "colour":"Merlot", "parentColour":"Reds"},
    {"id":"69", "colour":"Soft Pink", "parentColour":"Pinks"},

    {"id":"70", "colour":"Purple", "parentColour":"Purples Lilacs"},
    {"id":"71", "colour":"Amethyst", "parentColour":"Purples Lilacs"},
    {"id":"72", "colour":"Mauve", "parentColour":"Purples Lilacs"},
    {"id":"73", "colour":"Lilac", "parentColour":"Purples Lilacs"},
    {"id":"74", "colour":"Pale Lilac", "parentColour":"Purples Lilacs"},
    {"id":"75", "colour":"Plum", "parentColour":"Purples Lilacs"},
    {"id":"76", "colour":"Mulberry", "parentColour":"Purples Lilacs"},
    {"id":"77", "colour":"Grape", "parentColour":"Purples Lilacs"},
    {"id":"78", "colour":"Aubergine", "parentColour":"Purples Lilacs"},
    {"id":"79", "colour":"Violet", "parentColour":"Purples Lilacs"},

    {"id":"80", "colour":"Black", "parentColour":"Black"},

    {"id":"81", "colour":"Crimson", "parentColour":"Reds"},
    {"id":"82", "colour":"Claret", "parentColour":"Reds"},
    {"id":"83", "colour":"Mist", "parentColour":"Reds"},
    {"id":"84", "colour":"Red", "parentColour":"Reds"},
    {"id":"85", "colour":"Brick", "parentColour":"Reds"},
    {"id":"86", "colour":"Russet", "parentColour":"Reds"},
    {"id":"87", "colour":"Lipstick", "parentColour":"Reds"},
    {"id":"88", "colour":"Scarlet", "parentColour":"Reds"},

    {"id":"89", "colour":"Mono", "parentColour":"Mono"},

    {"id":"90", "colour":"Yellow", "parentColour":"Yellows"},
    {"id":"91", "colour":"Lemon", "parentColour":"Yellows"},
    {"id":"92", "colour":"Chartreuse", "parentColour":"Yellows"},

    {"id":"93", "colour":"Magenta", "parentColour":"Purples Lilacs"},
    {"id":"94", "colour":"Grey Melange", "parentColour":"Greys"},
    {"id":"95", "colour":"Sage", "parentColour":"Greens"},
    {"id":"96", "colour":"Apricot", "parentColour":"Oranges"},
    {"id":"97", "colour":"Black2", "parentColour":"Black"},
    {"id":"98", "colour":"Multi", "parentColour":"Multi"},
    {"id":"99", "colour":"Other", "parentColour":"Other"}
];