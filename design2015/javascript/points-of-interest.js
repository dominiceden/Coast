$(document).ready(function($){
	// Open interest point description on hover
	$('.cd-single-point').children('a').hover(function(){ // Hover on...
		var selectedPoint = $(this).parent('li');
      if (selectedPoint.hasClass('is-open')) {
        selectedPoint.removeClass('is-open').addClass('visited');
      }
      else {
        selectedPoint.addClass('is-open').siblings('.cd-single-point.is-open').removeClass('is-open').addClass('visited');
      }
     },
     function(){ // Hover off...
      var openDiv = $(this).parents('.cd-single-point').eq(0);
        setTimeout(function(){ // Wait a bit before closing the div that opens upon hover
          openDiv.removeClass('is-open').addClass('visited');
        }, 3000)
		}
	);

	// Close interest point description when Close is clicked (on mobile)
	$('.cd-close-info').on('click', function(event){
		event.preventDefault();
		$(this).parents('.cd-single-point').eq(0).removeClass('is-open').addClass('visited');
	});
});