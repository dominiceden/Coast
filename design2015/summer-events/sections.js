// Replace product ID with image link URL if product not available


var summerEvents = { "sections": [
    {
      "sectionid": "after-work-cocktails",
      "name": "After Work Cocktails",
      "description": "Style things up for after-work drinks with our cocktail evening wear. Transition effortlessly from office to cocktail party in contemporary wide leg trousers and a gorgeous evening top.",
      "items": {
          "Mimosa Halter Cami": ["mimosa-halter-cami", "&pound;45.00", "&euro;55.00", "1427891", "product-available", "quick-buy-works"],
          "Carter Trousers": ["carter-trousers", "&pound;75.00", "&euro;95.00", "1454606", "product-available", "quick-buy-doesnt-work"],
          "Dazzie Top": ["dazzie-top", "&pound;85.00", "&euro;110.00", "1465320", "product-available", "quick-buy-doesnt-work"],
          "Paloma Trouser": ["paloma-trouser", "&pound;79.00", "&euro;99.00", "1452206", "product-available", "quick-buy-doesnt-work"]
      }
    },
    {
      "sectionid": "at-the-polo",
      "name": "At The Polo",
      "description": "Amp up the femininity with our gorgeous edit of dresses for polo. Attention to detail is key with lace embroidery and flattering cuts leading the way. Style Tip: Don’t forget to wear heels for treading-in at half time.",
      "items": {
          "Eve Floral Necklace": ["eve-floral-necklace", "&pound;45.00", "&euro;55.00", "1387201", "product-available", "quick-buy-works"],
          "Lilianna Print Dress": ["lilianna-print-dress", "&pound;95.00", "&euro;120.00", "1454098", "product-available", "quick-buy-doesnt-work"],
          "Floral Necklace": ["floral-necklace-black", "&pound;35.00", "&euro;45.00", "1318580", "product-available", "quick-buy-works"],
          "Rina Dress": ["rina-dress", "&pound;139.00", "&euro;175.00", "1462521", "product-available", "quick-buy-doesnt-work"]
      }
    },
    {
      "sectionid": "lunch-with-the-girls",
      "name": "Lunch With The Girls",
      "description": "Summer is the perfect time to get together with friends for al fresco dining. Opt for a gorgeous jersey dress or choose separates for a stylish lunch out with the girls.",
      "items": {
          "Orchid Print Jersey Dress": ["orchid-print-jersey-dress", "&pound;89.00", "&euro;115.00", "1449698", "product-available", "quick-buy-works"],
          "Tezel Spot Blouse": ["tezel-spot-blouse", "&pound;59.00", "&euro;75.00", "1330606", "product-available", "quick-buy-works"],
          "Leather Tilly Zip Top Bag": ["leather-tilly-zip-top-bag", "&pound;70.00", "&euro;100.00", "1447159", "product-available", "quick-buy-works"],
          "Magna Trousers": ["magna-trousers", "&pound;75.00", "&euro;95.00", "1364980", "product-available", "quick-buy-works"]
      }
    },
    {
      "sectionid": "race-day",
      "name": "Race Day",
      "description": "Embrace the glamour of race day and be best dressed at Ascot and beyond. Stand out from the crowd and bring a summery vibe to your look with a vibrant statement skirt.",
      "items": {
          "Super Cerys Necklace": ["super-cerys-necklace", "&pound;35.00", "&euro;45.00", "1437660", "product-available", "quick-buy-works"],
          "Suzy Beaded Top": ["suzy-beaded-top", "&pound;75.00", "&euro;95.00", "1447606", "product-available", "quick-buy-works"],
          "Coro Skirt": ["coro-skirt", "&pound;95.00", "&euro;120.00", "1467466", "product-available", "quick-buy-doesnt-work"],
          "Maggie Mesh Clutch": ["maggie-mesh-clutch", "&pound;50.00", "Now &euro;44.50, was &euro;64.00", "1434406", "product-available", "quick-buy-works"]
      }
    },
    {
      "sectionid": "riverside-event",
      "name": "Riverside Event",
      "description": "Feel day-time gorgeous in floral inspired pretty dresses. Add more excitement with shimmering cover-ups and delicate accessories perfect for summer days out.",
      "items": {
          "Karis Embroidered Dress": ["karis-embroidered-dress", "&pound;129.00", "&euro;159.00", "1460898", "product-available", "quick-buy-doesnt-work"],
          "Lily Wrap": ["lily-wrap", "&pound;15.00", "&euro;19.00", "1388873", "product-available", "quick-buy-works"],
          "Floral Print Necklace": ["floral-print-necklace", "&pound;35.00", "&euro;45.00", "1318524", "product-available", "quick-buy-works"],
          "Wyatt Print Dress": ["wyatt-print-dress", "&pound;89.00", "&euro;115.00", "1461498", "product-available", "quick-buy-doesnt-work"]
      }
    },
    {
      "sectionid": "shopping-trip",
      "name": "Shopping Trip",
      "description": "Weekend shopping trip in the diary this summer? Look on-trend in our surprisingly versatile lace shirt and pair with this summer’s hottest trousers for a stylish shopping day with friends.",
      "items": {
          "Rosie May Shirt": ["rosie-may-shirt", "&pound;75.00", "&euro;95.00", "1467606", "product-available", "quick-buy-doesnt-work"],
          "Leather Metallic Tilly Clutch": ["leather-metallic-tilly-clutch", "&pound;55.00", "&euro;70.00", "1439459", "product-available", "quick-buy-works"],
          "Mattise Bracelet": ["mattise-bracelet", "&pound;15.00", "&euro;19.00", "1349267", "product-available", "quick-buy-works"],
          "Florence Trouser": ["florence-trouser", "&pound;75.00", "&euro;95.00", "1454939", "product-available", "quick-buy-doesnt-work"]
      }
    },
    {
      "sectionid": "summer-ball",
      "name": "Summer Ball",
      "description": "Find your ultimate summer ball look! Go for all out glamour in a sculptural maxi dress and feel dressed-up and feminine. Keep accessories simple and let the evening dress do the talking.",
      "items": {
          "Sasha Bow Maxi Dress": ["sasha-bow-maxi-dress", "&pound;195.00", "&euro;245.00", "1458384", "product-available", "quick-buy-doesnt-work"],
          "Deco Clutch": ["deco-clutch", "&pound;55.00", "&euro;70.00", "1404789", "product-available", "quick-buy-works"],
          "Tina Earrings": ["tina-earrings", "&pound;15.00", "&euro;19.00", "1386933", "product-available", "quick-buy-works"],
          "Theresa Wrap": ["theresa-wrap", "&pound;35.00", "&euro;45.00", "9180", "product-available", "quick-buy-works"]
      }
    },
    {
      "sectionid": "summer-bbq",
      "name": "Summer BBQ",
      "description": "Make the most of a warm summer in a floor length maxi and go bold with the blooms. Alternatively, choose a short summer occasion dress with pretty embellishment.",
      "items": {
          "Zuzanna Maxi Dress": ["zuzanna-maxi-dress", "&pound;139.00", "&euro;175.00", "1454298", "product-available", "quick-buy-doesnt-work"],
          "Crinkle Metal Box Clutch": ["crinkle-metal-box-clutch", "&pound;65.00", "&euro;80.00", "1434033", "product-available", "quick-buy-works"],
          "Malvina Dress": ["malvina-dress", "&pound;115.00", "&euro;145.00", "1466758", "product-available", "quick-buy-doesnt-work"],
          "Gigi Cuff": ["gigi-cuff", "&pound;20.00", "&euro;26.00", "1404933", "product-available", "quick-buy-works"]
      }
    },
    {
      "sectionid": "summer-concert",
      "name": "Summer Concert",
      "description": "Pull off an elegant look for a summer concert in a gorgeous evening maxi dress. Stick to cool, classic silhouettes, accessorise with delicate jewellery and feel amazing.",
      "items": {
          "Effy Feather Trim Top": ["effy-top", "&pound;65.00", "&euro;80.00", "1431406", "product-available", "quick-buy-works"],
          "Summer Belle Clutch": ["summer-belle-clutch", "&pound;45.00", "&euro;55.00", "1434790", "product-available", "quick-buy-works"],
          "Open Wire Cuff": ["open-wire-cuff", "&pound;15.00", "&euro;19.00", "1438241", "product-available", "quick-buy-works"],
          "Rue Skirt": ["rue-skirt", "&pound;129.00", "&euro;159.00", "1448590", "product-available", "quick-buy-doesnt-work"]
      }
    },
    {
      "sectionid": "summer-party",
      "name": "Summer Party",
      "description": "Look the part on those long summer nights in beautifully bright tones and chic accessories. Work statement skirts and lightweight tops for a cool and contemporary party look.",
      "items": {
          "Rose Gold Clutch": ["rose-gold-clutch", "&pound;60.00", "&euro;78.00", "1434533", "product-available", "quick-buy-works"],
          "Betty Print Maxi Dress": ["betty-print-maxi-dress", "&pound;115.00", "&euro;145.00", "1442198", "product-available", "quick-buy-works"],
          "Malia Maxi Dress": ["malia-maxi-dress", "&pound;175.00", "&euro;220.00", "http://www.coast-stores.com/pws/client/design2015/summer-events/malia-maxi-dress.jpg", "product-not-available", "Coming in June"],
          "Open Wire Necklace": ["open-wire-necklace", "&pound;18.00", "&euro;23.00", "1437433", "product-available", "quick-buy-works"]
      }
    }

  ]
}