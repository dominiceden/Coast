$(document).ready(function(){

  $('.bxslider').bxSlider({
    nextText: '',
    prevText: '',
    auto: true,
    speed: 1000
  });

  // countryISO = "GB"; test country setting

  $('.model-item').click(function(){
    var clickedSectionID = $(this).attr('data-section-id');

    $('#arrow-up-container').remove();  // remove any previous arrows
    $(this).append('<div id="arrow-up-container"><div id="arrow-up"></div></div>');

    $('#model-item-more-information').remove();
    $(this).parent().after('<div class="row" id="model-item-more-information" style="display: none;"></div>');

    for (i = 0; i < summerEvents.sections.length; i++) { // Match clicked section ID with section ID in sections.js database.
      if (summerEvents.sections[i].sectionid == clickedSectionID) {

        $('#model-item-more-information').append('<h2 class="guadalupe uppercase centred">' + summerEvents.sections[i].name + '</h2>');
        $('#model-item-more-information').append('<p class="section-subtext push-two eight centred">' + summerEvents.sections[i].description + '</p>');

        var sectionItems = summerEvents.sections[i].items; // get all items associated with section
        for(var key in sectionItems) { // Loop through the items and create divs with the key name and the values of the section items.

          if (sectionItems[key][4] == "product-available") {
            $('#model-item-more-information').append('<div class="product-item three column" id="' + sectionItems[key][0] + '"></div>');

            // Make an exception for the Theresa Wrap product. Show a different image when rendering.
            if (sectionItems[key][0] == "theresa-wrap") {
              $('#' + sectionItems[key][0]).append('<img src="http://www.coast-stores.com/pws/client/design2015/summer-events/theresa-wrap-9180.jpg">');
            }

            else { // Else generate an image URL based on the product ID.
              $('#' + sectionItems[key][0]).append('<img src="http://media.coast-stores.com/pws/client/images/catalogue/products/' + sectionItems[key][3] + '/large/' + sectionItems[key][3] + '.jpg">');
            }


            $('#' + sectionItems[key][0]).append('<p class="centred">' + [key] + '</p>'); // Add name of product

            if (countryISO == "IE") { // If Ireland, set Euro prices
              $('#' + sectionItems[key][0]).append('<p class="centred price">' + sectionItems[key][2] + '</p>');
            }

            else { // If any other country, show GBP
              $('#' + sectionItems[key][0]).append('<p class="centred price">' + sectionItems[key][1] + '</p>');
            }

            // If Quick Buy does work, then add QB functionality
            if (sectionItems[key][5] == "quick-buy-works") {
              $('#' + sectionItems[key][0]).append('<a href="http://www.coast-stores.com/coast/fcp-product/' + sectionItems[key][3] + '" class="quick-buy btn btn-neutral" data-ajax-url="/pws/AJProductDetails.ice?layout=quickbuy.pop.layout&amp;ProductID=' + sectionItems[key][3] + '">Shop</a>');

              $("a.quick-buy").on("click", function(event) { // Add Quick Buy handler to Shop Now buttons
              event.preventDefault();

              $.fn.simpleModal({
                  ajax: true,
                  ajaxUrl: $(this).attr("data-ajax-url"),
                  customClass: "quickbuy-popup",
                  revealCallback: function() {
                      btf.basket.ajaxBasket();
                      var c = $("[name=ProductID]");
                      $("[name=ProductID]").change(function() {
                          c.not(this).parent().removeClass("size_selected");
                          $(this).parent().addClass("size_selected")
                      });
                      var quickBuyProductID = $("img.product_image").attr("src").split("/").splice(-3, 1);
                      var linkToProduct = "http://www.coast-stores.com/coast/fcp-product/" + quickBuyProductID;
                      $("form#add-to-bag").after('<a id="quick-buy-link-to-product" href="' + linkToProduct + '">View full details</a>');
                  }
              });
              var a = $("#basket_response").clone();
              $("#basket_response").remove();
              a.after("#add-buttons");
              });
            } // end if

            else if (sectionItems[key][5] == "quick-buy-doesnt-work") { // If Quick Buy doesn't work, just add a simple link to the Shop Now button.
              $('#' + sectionItems[key][0]).append('<a href="http://www.coast-stores.com/coast/fcp-product/' + sectionItems[key][3] + '" class="btn btn-neutral">Shop</a>');
            } // end else if

          } // end if product available

          else if (sectionItems[key][4] == "product-not-available") {

            $('#model-item-more-information').append('<div class="product-item three column" id="' + sectionItems[key][0] + '"></div>');
            $('#' + sectionItems[key][0]).append('<img src="'+ sectionItems[key][3] + '">');
            $('#' + sectionItems[key][0]).append('<p class="centred">' + [key] + '</p>');
            $('#' + sectionItems[key][0]).append('<p class="centred">' + sectionItems[key][5] + '</p>');

          } // end if product not available

        }; // end 'for var in key'

      } // end if (summerEvents.sections[i].sectionid == clickedSectionID)
    } // end for loop

    $('#model-item-more-information').slideDown(1000);

    $('html,body').animate({
        scrollTop: $('#model-item-more-information').offset().top -30
    }, 700);

  }); // end click function

});