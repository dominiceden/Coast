/*  This script will submit product views on a product page, and clicks to add an item to a user's shopping bag,
    to Google Analytics (Enhanced Ecommerce).

    It fires the 'viewProductDetails' and 'addToBag' events which must be listened for in Google Tag Manager,
    with the Google Analytics tag being fired in response to collect the data and send it to Google Analytics.
*/

// Start Google Tag Manager - Enhanced Ecommerce - Universal Analytics integration

    $(document).ready(function(){
      var productName = $('.product-info > h1').text();
      var productID = $('a.zoom').attr("href").split('/').pop().split('.').splice(0,1).toString();
      var GTMproductValue = parseInt(productValue).toFixed(2); // 2 decimal places for prices
      var productCategory = $("#crumbs li:nth-child(2)").text();

      // Work out the colour of the product being viewed, using the colours database
      var colourId = parseInt(productID.slice(-2));
      var productColour = coastColoursDatabase[colourId].colour;
      var productColourParent = coastColoursDatabase[colourId].parentColour;

      // Get number of product reviews
      if ($('span.BVRRCount.BVRRNonZeroCount > span.BVRRNumber').html() >= 1) {
        var productReviewsCount = $('span.BVRRCount.BVRRNonZeroCount > span.BVRRNumber').html();
      }
      else {
        var productReviewsCount = "0";
      }

      // Measure a view of product details. This example assumes the detail view occurs on pageload,
      // and also tracks a standard pageview of the details page.
      dataLayer.push({
        'event': 'viewProductDetails',
        'nonInteraction': 1,
        'ecommerce': {
          'detail': {
            'products': [{
              'name': productName,
              'id': productID,
              'price': GTMproductValue,
              'category': productCategory,
              'metric1': productReviewsCount,
              'variant': productColour,
              'dimension1': productColourParent
              // 'dimension4': userSignedIn
             }]
           }
         }
      });

    /* Add to Bag tracking */

	    $('.add-to-bag').click(function(e){

    	  dataLayer.push({
        	'event': 'addToBag',
        	'ecommerce': {
          	  'add': {
            	'products': [{
              	'name': productName,
              	'id': productID,
              	'price': GTMproductValue,
              	'category': ($("#crumbs li:nth-child(2)").text()),
              	'metric1': productReviewsCount,
              	'metric2': ($('label[class*=selected] span').html()), // get size selected
              	'variant': productColour,
              	'dimension1': productColourParent,
              	// 'dimension4': userSignedIn,
              	'quantity': parseInt($('select#quantity').val())
             	}]
          	}
        	}
      	});

    	}); // end click function

    	$('li.no-stock').click(function(){ // if a product size is out of stock, fire an event when the user clicks on it.
  			dataLayer.push({'event': 'sizeOutOfStock'});
		  });

    }); // end document.ready function

// End Google Tag Manager - Enhanced Ecommerce - Universal Analytics integration -->