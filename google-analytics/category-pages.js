/*  This script will submit product impressions on a category page, and product clicks from a category page, to Google Analytics (Enhanced Ecommerce).
    It fires the 'viewProductCategory' and 'productClick' events which must be listened for in Google Tag Manager,
     with the Google Analytics tag being fired in response to collect the data and send it to Google Analytics.
*/

// Start Google Tag Manager - Enhanced Ecommerce - Universal Analytics integration

    $(document).ready(function(){

      dataLayer.push({ // initialise the ecommerce impressions array so we can push to it later
        'ecommerce': {
            'impressions': []
          }
      });

      var allCategoryProducts = $('li.product'); // Get all products in category

      allCategoryProducts.each(function(index) { // Iterate through them to get values

        $(this).attr('data-item-position', index + 1); // add a data-item-position DOM attribute to each product in category. Use this later to report the item's position

        var categoryProductName = $(this).find('a.title').html(); // get product name
        var categoryProductID = $(this).find('a.title').attr('href').split('/').pop().split('?').splice(0,1).pop(); // get product ID

        // Sometimes items are on sale. If so, their prices will be contained next to a 'now' div. If not, it's just in a price div.
        var categoryProductPrice = $(this).find('span.price');
          if ( $(this).has('.now').length) {
            var categoryProductPrice = $(this).clone().remove('.price_prefix').find('.now').text().substring(4);
          }

          else if ( $(this).has('.price').length) {
            var categoryProductPrice = $(this).find('span.price').html().trim().substring(1);
          }

        var categoryProductCategory = $("#crumbs li:nth-child(2)").text();

        var categoryProductColourID = $(this).find('a.title').attr('href').split('/').pop().split('?').splice(0,1).pop().slice(-2);
        // If the colour ID begins with a 0, strip off this 0 so we can match it with a value from the colours database array
        if (categoryProductColourID.substring(0,1) == "0") {
          var categoryProductColourID = categoryProductColourID.substring(1);
        }

        var categoryProductColour = coastColoursDatabase[categoryProductColourID].colour;
        var categoryProductColourParent = coastColoursDatabase[categoryProductColourID].parentColour;

        var categoryProductPosition = $(this).attr('data-item-position'); // Collect the position of the item in the page results.

        var categoryProductObject = { // Build the object that will be pushed into the data layer
         'name': categoryProductName,
         'id': categoryProductID,
         'price': categoryProductPrice,
         'category': categoryProductCategory,
         'variant': categoryProductColour,
         'list': categoryProductCategory,
         'position': categoryProductPosition
        }

        dataLayer[2].ecommerce.impressions.push(categoryProductObject); // Push this object into the data layer. Position [2] on production, [3] for testing in the console.

        return (index < 55); // Stop iterating once 56 products have been collected as we are limited to 8KB of data to send to Google.

      }); // end each function

      dataLayer.push({'event':'viewProductCategory'}); // signal the viewProductCategory event has occurred. Use this to trigger the Analytics tag to fire to send data back to GA.

      /* GTM - track product clicks */

      $('li.product').click(function(e){

        var clickedProductItemURL = $(this).find('a.image').attr('href');
        var clickedProductItemName = $(this).find('.info > .title').text();
        var clickedProductItemID = $(this).find('a.image > img.product_image').not('.alt_image').attr('src').split('/').pop().split('.').splice(0,1).toString();

        // Sometimes items are on sale. If so, their prices will be contained next to a 'now' div. If not, it's just in a price div.
        var clickedProductItemPrice = $(this).find('span.price');
          if ( $(this).has('.now').length) {
            var clickedProductItemPrice = $(this).clone().remove('.price_prefix').find('.now').text().substring(4);
          }

          else if ( $(this).has('.price').length) {
            var clickedProductItemPrice = $(this).find('span.price').html().trim().substring(1);
          }

        var clickedProductItemColourId = clickedProductItemID.toString().slice(-2);
        // If the colour ID begins with a 0, strip off this 0 so we can match it with a value from the colours database array
        if (clickedProductItemColourId.substring(0,1) == "0") {
          var clickedProductItemColourId = clickedProductItemColourId.substring(1);
        }

        var clickedProductItemColour = coastColoursDatabase[clickedProductItemColourId].colour;
        var clickedProductItemColourParent = coastColoursDatabase[clickedProductItemColourId].parentColour;

        var clickedProductItemPosition = $(this).attr('data-item-position');

        dataLayer.push({
          'event': 'productClick',
          'ecommerce': {
            'click': {
              'actionField': {'list': ($("#crumbs li:nth-child(2)").text())},      // Optional list property.
              'products': [{
                'name': clickedProductItemName,                      // Name or ID is required.
                'id': clickedProductItemID,
                'price': clickedProductItemPrice,
                'category': ($("#crumbs li:nth-child(2)").text()),
                'variant': clickedProductItemColour,
                'dimension1': clickedProductItemColourParent,
                'position': clickedProductItemPosition
               }]
             }
           } /*,
           'eventCallback': function() { // handle navigation after the ecommerce data has been sent to Google Analytics.
             document.location = clickedProductItemURL
           } */
        });

        // e.preventDefault(); use for testing only

      }); //end click function

      /* end GTM - track product clicks */

    }); // end document.ready

// End Google Tag Manager - Enhanced Ecommerce - Universal Analytics integration -->