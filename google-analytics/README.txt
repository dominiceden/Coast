Google Universal Analytics (Enhanced Ecommerce) Integration - README

All scripts are to be placed in /pws/client/google-analytics.

------------------------------------------------------------------------------------------------------------------------------------------------

///**Brief explanation of how it works:**\\\

* We use Google Tag Manager to deploy Google Analytics on coast-stores.com. We have enabled Enhanced Ecommerce in Google Analytics too - this
  is enabled in Google Analytics itself.

* Google Analytics is deployed on all Coast web pages anyway (to provide general pageview stats etc.), but we also want it to report specific
  ecommerce data too. It won't do this automatically. In order to report this data, we must collect the data we wish to send and submit it
  to Google Analytics using our own code.

* On each page that we want to report ecommerce data for (see below), we use JS scripts to scrape the data and push it into a array named
  'dataLayer'. Once the data is collected into dataLayer and is ready to submit to Google Analytics, we fire an event.

* Google Tag Manager is set up with triggers - it listens for us firing these events, and is told to fire the Google Analytics code in response.
  When the Google Analytics code fires, it reads the dataLayer and sends the information to Google Analytics.

------------------------------------------------------------------------------------------------------------------------------------------------

///**Scripts and where they are placed:**\\\

* Category JS code goes in Main or MainStart field in the category itself (e.g. wcdept_all-dresses).
* Product Details Page code goes in HeadEnd
* View Basket Page code to detect product removes goes in HeadEnd
* Payment Details code goes in MainEnd

------------------------------------------------------------------------------------------------------------------------------------------------

///**Dimensions and Metrics**\\\

Dimension 1: This is the parent category of the colour in question. See http://www.coast-stores.com/pws/client/design2015/javascript/colours-database.js
             for a full breakdown of colours and categories. Google Analytics calls the precise colour of a product its 'variant'.

Metric 1: This is the number of reviews for an item - submitted when a user goes on to the Product Details Page.

Metric 2: This is the size of the product.