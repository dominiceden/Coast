/*  This script is for when the payment details are viewed in the checkout funnel.

    It fires the 'checkout' event which must be listened for in Google Tag Manager,
    with the Google Analytics tag being fired in response to collect the data and send it to Google Analytics.
*/

// Start Google Tag Manager - Enhanced Ecommerce - Universal Analytics integration

      $(document).ready(function(){

        dataLayer.push({ // Initialise empty data layer that we can push objects to later. Fire checkout event later
          'ecommerce': {
            'checkout': {
              'actionField': {'step': 2, 'option': 'Payment Page'},
              'products': []
           }
         }
        });


        var allBasketProducts = $('.product-row').not('.heading'); // Get all products in basket

        allBasketProducts.each(function(){ // Iterate through them to get values

          var BasketProductName = $(this).find('p.product_title > a').text(); // get product name
          var BasketProductID = $(this).find('.basket-image a').attr('href').split('/').pop().split('?').splice(0,1).toString(); // get product ID
          var BasketProductPrice = $(this).find('.basket-price').html().trim().substring(1); // get product price, trimmed of whitespace and stripped of currency
          var BasketProductQuantity = $(this).find('span.qty').text();
          var BasketProductSize = parseInt($(this).find('p.product_size > span.value').text());

          var BasketProductColourID = BasketProductID.slice(-2);
          // If the colour ID begins with a 0, strip off this 0 so we can match it with a value from the colours database array
          if (BasketProductColourID.substring(0,1) == "0") {
            var BasketProductColourID = BasketProductColourID.substring(1);
          }

          var BasketProductColour = coastColoursDatabase[BasketProductColourID].colour;
          var BasketProductColourParent = coastColoursDatabase[BasketProductColourID].parentColour;

          var BasketProductObject = { // Build the object that will be pushed into the data layer
           'name': BasketProductName,
           'id': BasketProductID,
           'price': BasketProductPrice,
           'variant': BasketProductColour,
           'dimension1': BasketProductColourParent,
           'quantity': BasketProductQuantity,
           'metric2': BasketProductSize // get size selected
          }

          dataLayer[2].ecommerce.checkout.products.push(BasketProductObject); // Push this object into the data layer. Position [2] on production, [3] for testing in the console.

        }); // end each function

        dataLayer.push({'event':'checkout'}); // signal the checkout event has occurred. Use this to trigger the Analytics tag to fire to send data back to GA.

      }); // end document.ready

// End Google Tag Manager - Enhanced Ecommerce - Universal Analytics integration -->