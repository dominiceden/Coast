/*  This script is for when the delivery options are viewed in the checkout funnel.

    It fires the 'checkoutDeliveryOptions' event which must be listened for in Google Tag Manager,
    with the Google Analytics tag being fired in response to collect the data and send it to Google Analytics.
*/

// Start Google Tag Manager - Enhanced Ecommerce - Universal Analytics integration

      $(document).ready(function(){

        dataLayer.push({
          'event': 'checkoutDeliveryOptions',
          'ecommerce': {
            'checkout_option': {
              'actionField': {'step': 1, 'option': 'checkoutDeliveryOptions'}
            }
          }
        });

      }); // end document.ready

// End Google Tag Manager - Enhanced Ecommerce - Universal Analytics integration -->