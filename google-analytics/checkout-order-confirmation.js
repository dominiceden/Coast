/*  This script is for when the user is shown the order confirmation page. It submits all purchase information to Google Analytics.
    It fires the 'orderConfirmation' event which must be listened for in Google Tag Manager, with the Google Analytics tag being
    fired in response to collect the data and send it to Google Analytics.
*/

// Start Google Tag Manager - Enhanced Ecommerce - Universal Analytics integration

      $(document).ready(function(){

        dataLayer.push({ // Initialise data layer for purchase event, with an empty array for purchases. We can push product objects to this later. Fire orderConfirmation event later
          'ecommerce': {
            'currencyCode': currency,
            'purchase': {
              'actionField': {
                'id': orderID,
                'affiliation': 'Website Store',
                'revenue': parseInt(orderValue).toFixed(2).toString() // Total transaction value (incl. tax and shipping)
                // 'coupon': '$promoCode'
              },
              'products': []
            }
          }
        });


        var allBasketProducts = $('tbody > tr'); // Get all products in basket

        allBasketProducts.each(function(){ // Iterate through them to get values

          var BasketProductName = $(this).find('a.product_link').text(); // get product name
          var BasketProductID = $(this).find('.basket-image a').attr('href').split('/').pop().split('?').splice(0,1).toString(); // get product ID
          var BasketProductPrice = $(this).find('.basket_price').html().trim().substring(1); // get product price, trimmed of whitespace and stripped of currency
          var BasketProductQuantity = $(this).find('a.product_link').nextAll().eq(1).html().split('&nbsp;').pop();
          var BasketProductSize = $(this).find('a.product_link').next().html().split('&nbsp;').pop();

          var BasketProductColourID = BasketProductID.slice(-2);
          // If the colour ID begins with a 0, strip off this 0 so we can match it with a value from the colours database array
          if (BasketProductColourID.substring(0,1) == "0") {
            var BasketProductColourID = BasketProductColourID.substring(1);
          }

          var BasketProductColour = coastColoursDatabase[BasketProductColourID].colour;
          var BasketProductColourParent = coastColoursDatabase[BasketProductColourID].parentColour;

          var BasketProductObject = { // Build the object that will be pushed into the data layer
           'name': BasketProductName,
           'id': BasketProductID,
           'price': BasketProductPrice,
           'variant': BasketProductColour,
           'dimension1': BasketProductColourParent,
           'quantity': BasketProductQuantity,
           'metric2': BasketProductSize // get size selected
          }

          dataLayer[2].ecommerce.purchase.products.push(BasketProductObject); // Push this object into the data layer. Position [2] on production, [3] for testing in the console.

        }); // end each function

        dataLayer.push({'event':'orderConfirmation'}); // signal the orderConfirmation event has occurred. Use this to trigger the Analytics tag to fire to send data back to GA.

      }); // end document.ready

// End Google Tag Manager - Enhanced Ecommerce - Universal Analytics integration -->