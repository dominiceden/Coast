/*  This script will submit clicks to remove products from the minibag hover (near top nav) to Google Analytics (Enhanced Ecommerce).

    It fires the 'removeFromBag' event which must be listened for in Google Tag Manager,
    with the Google Analytics tag being fired in response to collect the data and send it to Google Analytics.
*/

// Start Google Tag Manager - Enhanced Ecommerce - Universal Analytics integration

    $(document).ready(function(){

      // Products are added using AJAX, so we need to use setInterval function to keep adding click handlers to new .remove-item elements that appear as products are added.
      setInterval(function(){

        $('a.remove-item').unbind(); // clear any previous click handlers bound on this element
        $('a.remove-item').click(function(){ // start scraping the removed product information from the DOM

          var productBasketItemName = $(this).next().html();
          var productBasketItemID = $(this).next().attr('href').split('/').pop().split('?').splice(0,1);
          var productBasketItemPrice = $(this).nextAll().eq(1).find('li.price').html().substring(1); // remove currency symbol

          var productBasketItemColourId = productBasketItemID.toString().slice(-2);
          // If the colour ID begins with a 0, strip off this 0 so we can match it with a value from the colours database array
          if (productBasketItemColourId.substring(0,1) == "0") {
            var productBasketItemColourId = productBasketItemColourId.substring(1);
          }
          var productBasketItemColour = coastColoursDatabase[productBasketItemColourId].colour;
          var productBasketItemColourParent = coastColoursDatabase[productBasketItemColourId].parentColour;

          var productBasketItemQuantity = $(this).nextAll().eq(1).find('li.quantity').html().split('&nbsp;').pop();
          var productBasketItemSize = $(this).nextAll().eq(1).find('li.size').html().split('&nbsp;').pop();

          console.log(productBasketItemID);

          dataLayer.push({
            'event': 'removeFromBag',
            'ecommerce': {
              'remove': {                               // 'remove' actionFieldObject measures.
                'products': [{                          //  removing a product to a shopping cart.
                    'name': productBasketItemName,
                    'id': productBasketItemID,
                    'price': productBasketItemPrice,
                    'variant': productBasketItemColour,
                    'dimension1': productBasketItemColourParent, // parent colour of variant
                    // 'dimension4': userSignedIn,
                    'metric2': productBasketItemSize, // get size selected
                    'quantity': productBasketItemQuantity
                }]
              }
            }
          });

        }); // end click function
      }, 2000); // end setTimeout function

    }); // end document.ready

// End Google Tag Manager - Enhanced Ecommerce - Universal Analytics integration -->