/*  This script will submit clicks to remove products on the View Basket page to Google Analytics (Enhanced Ecommerce).

    It fires the 'removeFromBag' event which must be listened for in Google Tag Manager,
    with the Google Analytics tag being fired in response to collect the data and send it to Google Analytics.
*/

// Start Google Tag Manager - Enhanced Ecommerce - Universal Analytics integration

      $(document).ready(function(){

        /* Start basket contents submission */

          dataLayer.push({ // Initialise empty data layer that we can push objects to later. Fire checkout event later
            'ecommerce': {
              'checkout': {
                'actionField': {'step': 1, 'option': 'View Basket'},
                'products': []
             }
           }
          });

          var allBasketProducts = $('.basket-line'); // Get all products in basket

          allBasketProducts.each(function(){ // Iterate through them to get values

            var BasketProductName = $(this).find('.info > p.product-title > a').text(); // get product name
            var BasketProductID = $(this).find('p.product-title a').attr('href').split('/').pop().split('?').splice(0,1).toString(); // get product ID
            var BasketProductPrice = $(this).find('.unit-price').html().trim().substring(1); // get product price, trimmed of whitespace and stripped of currency
            var BasketProductQuantity = $(this).find('.quantity > p.product_quantity > span.qty').text();
            var BasketProductSize = parseInt($(this).find('.size-cont').text().trim());

            var BasketProductColourID = BasketProductID.slice(-2);
            // If the colour ID begins with a 0, strip off this 0 so we can match it with a value from the colours database array
            if (BasketProductColourID.substring(0,1) == "0") {
              var BasketProductColourID = BasketProductColourID.substring(1);
            }

            var BasketProductColour = coastColoursDatabase[BasketProductColourID].colour;
            var BasketProductColourParent = coastColoursDatabase[BasketProductColourID].parentColour;

            var BasketProductObject = { // Build the object that will be pushed into the data layer
             'name': BasketProductName,
             'id': BasketProductID,
             'price': BasketProductPrice,
             'variant': BasketProductColour,
             'dimension1': BasketProductColourParent,
             'quantity': BasketProductQuantity,
             'metric2': BasketProductSize // get size selected
            }

            dataLayer[2].ecommerce.checkout.products.push(BasketProductObject); // Push this object into the data layer. Position [2] on production, [3] for console testing.

          }); // end each function

          dataLayer.push({'event':'checkout'}); // signal the checkout event has occurred. Use this to trigger the Analytics tag to fire to send data back to GA.

        /* End basket contents submission */



        /* Start product remove click event submission */

        $('a.product-remove').unbind(); // clear any previous click handlers bound on this element
        $('a.product-remove').click(function(e){ // start scraping the removed product information from the DOM

          // e.preventDefault(); testing only

          var productBasketItemName = $(this).parents().eq(2).find('.info > p.product-title > a').text();
          var productBasketItemID = $(this).parents().eq(2).find('.image-cont > a > img').attr('src').split('/').pop().split('.').splice(0,1).toString();
          var productBasketItemPrice = $(this).parents().eq(2).find('.unit-price').text().trim().substring(1); // trim and remove currency symbol

          var productBasketItemColourId = productBasketItemID.toString().slice(-2);
          // If the colour ID begins with a 0, strip off this 0 so we can match it with a value from the colours database array
          if (productBasketItemColourId.substring(0,1) == "0") {
            var productBasketItemColourId = productBasketItemColourId.substring(1);
          }
          var productBasketItemColour = coastColoursDatabase[productBasketItemColourId].colour;
          var productBasketItemColourParent = coastColoursDatabase[productBasketItemColourId].parentColour;

          var productBasketItemQuantity = $(this).parents().eq(2).find('.quantity > p.product_quantity > span.qty').text();
          var productBasketItemSize = $(this).parents().eq(2).find('.size-cont').text().trim();


          dataLayer.push({
            'event': 'removeFromBag',
            'ecommerce': {
              'remove': {                               // 'remove' actionFieldObject measures.
                'products': [{                          //  removing a product to a shopping cart.
                    'name': productBasketItemName,
                    'id': productBasketItemID,
                    'price': productBasketItemPrice,
                    'variant': productBasketItemColour,
                    'dimension1': productBasketItemColourParent, // parent colour of variant
                    // 'dimension4': userSignedIn,
                    'metric2': productBasketItemSize, // get size removed from bag
                    'quantity': productBasketItemQuantity
                }]
              }
            }
          });

        }); // end click function

      /* End product remove click event submission */

    }); // end document.ready

// End Google Tag Manager - Enhanced Ecommerce - Universal Analytics integration -->