---------COAST BRIDESMAIDS LOOKBOOK V1.0-----------
--------Last updated on 18th February 2015---------

CONTENTS:

1/ DATABASE

---------------------------------------------------------------------------

1/ DATABASE

The file products.js holds all the product information in JSON format for all Bridesmaid Style Book 2015 products. We iterate through this
to populate the DOM with product information.

The .manifest file holds the filenames of all the files that will be stored offline.
If you want to update the manifest, you can generate a list of all the files (including
directory paths) by using the following command in Terminal (Mac OS X).

find ~/Documents/Coast -type f

(where Documents/Coast is the folder containing all files).

*** ALL MANIFEST FILENAMES AND FOLDER NAMES MUST BE LOWER CASE. THE MANIFEST IS CASE SENSITIVE ***

---------------------------------------------------------------------------



