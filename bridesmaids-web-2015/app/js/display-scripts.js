$(document).ready(function(){

// add FastClick.js to document to remove 300ms touch lag on touch devices

  $(function() {
      FastClick.attach(document.body);
  });

// When the document is ready, populate the bottom thumbnails section, main hero section and right hand side images

  for (var i = 0; i < products.dresses.length; i++) {
    productID = products.dresses[i].productid; // store the URL for the dress bottom thumbnail image in this variable
    // Now create a div to hold this thumbnail, give it the same ID as the ID of the product and write it to the DOM.
    $("#bottom-thumbnail-container").append('<div class="bottom-thumbnail" data-product-id="' + products.dresses[i].productid + '"><img src="http://www.coast-stores.com/pws/client/bridesmaid-style-book-2015/images/' + productID + '/' + productID + '-1.jpg"></div>');
  };

// When a bottom thumbnail is clicked on, run the following function

  $(".bottom-thumbnail").click(function(){

    $("#slider").remove(); // remove the slider if it's present, as most products do not have one (only 5/15 products do).

    var clickedProductID = $(this).attr("data-product-id"); // get product id that was clicked on and store it in a variable called clickedProductID.

    // create a set of variables to hold the attributes of the product that was clicked on

    for (i = 0; i < products.dresses.length; i++) {
        if (products.dresses[i].productid == clickedProductID) {
          var clickedProductName = products.dresses[i].name;
          var clickedProductPrice = products.dresses[i].price;
          var clickedProductEURPrice = products.dresses[i].EURprice;
          var clickedProductSize = products.dresses[i].size_range;
          var productColours = products.dresses[i].colours;
          var clickedProductDebenhamsExclusive = products.dresses[i].debenhams_exclusive;
          var clickedProductAlsoAvailableInText = products.dresses[i].also_available_in;
          var clickedProductIn5Or15 = products.dresses[i].in_5_or_15;
          var clickedProductDateAvailable = products.dresses[i].date_available;
          var clickedProductIn5Or15Images = products.dresses[i].in_5_or_15_images; // list of images that will be used in the 5 or 15 slider
        } // end if statement
    }; // end for loop

    // Generate the URLs for the images for this particular product and store them in variables
    var clickedProductSmallImageFront = "http://www.coast-stores.com/pws/client/bridesmaid-style-book-2015/images/" + clickedProductID + "/" + clickedProductID + "-1.jpg";
    var clickedProductSmallImageBack = "http://www.coast-stores.com/pws/client/bridesmaid-style-book-2015/images/" + clickedProductID + "/" + clickedProductID + "-2.jpg";;
    var clickedProductSmallImageDetail = "http://www.coast-stores.com/pws/client/bridesmaid-style-book-2015/images/" + clickedProductID + "/" + clickedProductID + "-3.jpg";;
    var clickedProductLargeImageFront = "http://www.coast-stores.com/pws/client/bridesmaid-style-book-2015/images/" + clickedProductID + "/" + clickedProductID + "-4.jpg";;
    var clickedProductLargeImageBack = "http://www.coast-stores.com/pws/client/bridesmaid-style-book-2015/images/" + clickedProductID + "/" + clickedProductID + "-5.jpg";;
    var clickedProductLargeImageDetail = "http://www.coast-stores.com/pws/client/bridesmaid-style-book-2015/images/" + clickedProductID + "/" + clickedProductID + "-6.jpg";;

    // Now write the product information to the DOM
    $("#productName").html(clickedProductName);

    // Debenhams Exclusive
    $('p#productBottomText').html('');
    if (clickedProductDebenhamsExclusive == true) {
      $('p#productBottomText').html('Please ask store for availability. This style is exclusive to Debenhams.');
    }

    if (countrycode == "UK") {
      $("#productPrice").html(clickedProductPrice);
    }
    else if (countrycode == "IE") {
      $("#productPrice").html(clickedProductEURPrice);
    }

    $("#productSize").html(clickedProductSize);

    $("#productDateAvailable").html('');
    $("#productDateAvailable").html(clickedProductDateAvailable);

    // Generate product colours

    $("#colours").html(""); // Clear any lingering colours first before adding new ones.

    for(var key in productColours) { // Loop through the colours and create divs with the key name and the value as a background.
      $("#colours").append('<div class="colour-swatch-item" style="display:inline-block"><div class="colour-swatch-item-circle" style="background:' + productColours[key][0] + '" data-product-id="' + productColours[key][1] + '" data-product-available="' + productColours[key][2] + '" data-debenhams-exclusive="' + productColours[key][3] + '" data-petite="' + productColours[key][4] + '"></div>');
    };

    // if productAlsoAvailable text is present for the product, display it. Otherwise write an empty div.
    if (clickedProductAlsoAvailableInText.length > 0) {
      $("#productAlsoAvailable").html(clickedProductAlsoAvailableInText);
    }
    else {
      $("#productAlsoAvailable").html("");
    };

    // Test if the product has 5 or 15 ways to wear (i.e. Corwin dresses). If it does, then we need to create a slideshow when side images 2 and 3 are clicked.
    // If it doesn't, then we need to make sure that when side images 2 and 3 are clicked, they load just a single image in the #main-hero section.
    // Make sure we write the data-product-id attribute to the side-image div so that when it's clicked on, we can load the corresponding large image.

    if (clickedProductIn5Or15 == true) {

      $('.colour-swatch-item-circle').each(function(){
        $(this).addClass('fifteen-ways');
      });

      $("#main-hero").css({"background" : "url('" + clickedProductLargeImageFront + "')", "background-size" : "cover"});
      $("#side-image-1").attr('data-product-id', clickedProductID).addClass('fifteen-ways').css({"background" : "url('" + clickedProductSmallImageFront + "')", "background-size" : "cover"});
      $("#side-image-2").addClass('fifteen-ways').css({"background" : "url(../images/corwin-15-ways-to-wear/fifteenways.jpg)", "background-size" : "cover"});
      $("#side-image-3").addClass('fifteen-ways').css({"background" : "url(../images/corwin-15-ways-to-wear/back.jpg)", "background-size" : "cover"});

    } // end if statement

    else {

      $("#main-hero > #slider").remove(); // remove any sliders that already exist
      $("#main-hero").css({"background" : "url('" + clickedProductLargeImageFront + "')", "background-size" : "cover"});
      $("#side-image-1").attr('data-product-id', clickedProductID).removeClass('fifteen-ways').css({"background" : "url('" + clickedProductSmallImageFront + "')", "background-size" : "cover"});
      $("#side-image-2").attr('data-product-id', clickedProductID).removeClass('fifteen-ways').css({"background" : "url('" + clickedProductSmallImageBack + "')", "background-size" : "cover"});
      $("#side-image-3").attr('data-product-id', clickedProductID).removeClass('fifteen-ways').css({"background" : "url('" + clickedProductSmallImageDetail + "')", "background-size" : "cover"});

    }; // END IF '5 OR 15 WAYS TO WEAR'

    /* SIDE IMAGE CLICK FUNCTIONS */

    $("#side-image-1").click(function(){
      $("#slider").remove(); // remove any sliders that already exist
      $("#main-hero").css({"background" : "url('http://www.coast-stores.com/pws/client/bridesmaid-style-book-2015/images/" + $(this).attr('data-product-id') + "/" + $(this).attr('data-product-id') + "-4.jpg')", "background-size" : "cover"});
    });

    $("#side-image-2, #side-image-3").click(function(){
      if ( $(this).hasClass('fifteen-ways') ) {
        $("#main-hero").css("background", ""); // remove background image to make way for slider
        $("#slider").remove(); // remove any sliders that already exist
        $("#main-hero").prepend("<div id='slider' class='swipe'><div class='swipe-wrap'></div><span class='nav prev'></span><span class='nav next'></span></div>") // create slider div in #main-hero

        for (var key in clickedProductIn5Or15Images) { // Loop through the images that make up the slider for the 5 or 15 ways to wear this dress
          var sliderElement = "<div><img src='" + clickedProductIn5Or15Images[key] + "'width='448' height='655'/></div>"; // generate HTML for slider
          $(".swipe-wrap").append(sliderElement); // Append the HTML of slide elements to slide-wrap container
        }

        // initialise slider for 5 or 15 products, once DOM structure is in place as above
        Slider = $('#slider').Swipe({auto: 5000,speed: 1000,continuous: true}).data('Swipe');
        $('.next').on('click', Slider.next); // listener for click/tap on .next div on slider (chevron)
        $('.prev').on('click', Slider.prev);
      }

      else {
        $("#slider").remove(); // remove slider if it is there

        // Using the data-product-id attribute set the corresponding large image as the main-hero background image.
        if ( $(this).attr('id') == "side-image-2" ) {
          $("#main-hero").css({"background" : "url('http://www.coast-stores.com/pws/client/bridesmaid-style-book-2015/images/" + $(this).attr('data-product-id') + "/" + $(this).attr('data-product-id') + "-5.jpg')", "background-size" : "cover"});
        }

        if ( $(this).attr('id') == "side-image-3" ) {
          $("#main-hero").css({"background" : "url('http://www.coast-stores.com/pws/client/bridesmaid-style-book-2015/images/" + $(this).attr('data-product-id') + "/" + $(this).attr('data-product-id') + "-6.jpg')", "background-size" : "cover"});
        }
      }
    });

    /* END SIDE IMAGE CLICK FUNCTIONS */

    $("#bottom-slider").animate({marginBottom: "-421px"}, 1000); // when a thumbnail clicked, bring the bottom slider down to the bottom so it's ready to be clicked and slid up
    $("#ask-for-availability-container").css("display", "none");

    // Click handler for when user clicks a swatch

    $('.colour-swatch-item-circle').click(function(){
      var clickedColourProductID = $(this).attr('data-product-id');

      for (i = 0; i < products.dresses.length; i++) { // Replace the date available for the selected dress when a colour swatch is clicked.
        if (products.dresses[i].productid == clickedColourProductID) {
          console.log(products.dresses[i].date_available);
          $('#productDateAvailable').html(products.dresses[i].date_available);
        }
      }

      if ( $(this).hasClass('fifteen-ways') ) { // if this colour swatch is for a 15 Ways To Wear product
        $("#slider").remove(); // remove any sliders that already exist
        $("#main-hero").css({'background' : 'url("http://www.coast-stores.com/pws/client/bridesmaid-style-book-2015/images/' + clickedColourProductID + '/' + clickedColourProductID + '-4.jpg")', "background-size" : "cover"});
        $("#side-image-1").attr('data-product-id', clickedColourProductID).addClass('fifteen-ways').css({'background' : 'url("http://www.coast-stores.com/pws/client/bridesmaid-style-book-2015/images/' + clickedColourProductID + '/' + clickedColourProductID + '-1.jpg")', "background-size" : "cover"});
        $("#side-image-2").addClass('fifteen-ways').css({"background" : "url(../images/corwin-15-ways-to-wear/fifteenways.jpg)", "background-size" : "cover"});
        $("#side-image-3").addClass('fifteen-ways').css({"background" : "url(../images/corwin-15-ways-to-wear/back.jpg)", "background-size" : "cover"});
      }

      else {  // if this colour swatch is NOT for a 15 Ways To Wear product
        $("#main-hero").css({'background' : 'url("http://www.coast-stores.com/pws/client/bridesmaid-style-book-2015/images/' + clickedColourProductID + '/' + clickedColourProductID + '-4.jpg")', "background-size" : "cover"});
        $("#side-image-1").attr('data-product-id', clickedColourProductID).removeClass('fifteen-ways').css({'background' : 'url("http://www.coast-stores.com/pws/client/bridesmaid-style-book-2015/images/' + clickedColourProductID + '/' + clickedColourProductID + '-1.jpg")', "background-size" : "cover"});
        $("#side-image-2").attr('data-product-id', clickedColourProductID).removeClass('fifteen-ways').css({'background' : 'url("http://www.coast-stores.com/pws/client/bridesmaid-style-book-2015/images/' + clickedColourProductID + '/' + clickedColourProductID + '-2.jpg")', "background-size" : "cover"});
        $("#side-image-3").attr('data-product-id', clickedColourProductID).removeClass('fifteen-ways').css({'background' : 'url("http://www.coast-stores.com/pws/client/bridesmaid-style-book-2015/images/' + clickedColourProductID + '/' + clickedColourProductID + '-3.jpg")', "background-size" : "cover"});
      }
    }); // end colour swatch click function

  }); // end bottom thumbnail click function


// simulate click on the first thumbnail to load all the images
    $('.bottom-thumbnail').first().trigger('click');

// add in jQuery Mobile touch events for bottom slider

    $(function(){
      $("#bottom-slider").on("swipeup", swipeUp); // add swipeUp event listener

      function swipeUp(event) {
        $("#bottom-slider").animate({marginBottom: "0px"}, 1000); // set margin-bottom to 0 instead of -411px, so it's displayed full height and not hidden
        $("#ask-for-availability-container").css("display", "block"); // show product information
        $("#sizes").css("display", "block");
        $("#colours, #colours2").css("display", "block");
        $("#productPetite, #productPetite2").css("display", "block");
        $("#productAlsoAvailable, #productAlsoAvailable2").css("display", "block");
        console.log("swipe up");
      }

      $("#bottom-slider").on("swipedown", swipeDown); // add swipeDown event listener

      function swipeDown(event) {
        $("#bottom-slider").animate({marginBottom: "-455px"}, 1000);
        console.log("swipe down");
      }

    });

  // When the bottom slider is clicked, change hidden divs to display: block and slide it up

  $("#bottom-slider").click(function(){
    $("#ask-for-availability-container").css("display", "block"); // show product information
    $("#sizes").css("display", "block");
    $("#colours, #colours2").css("display", "block");
    $("#productPetite, #productPetite2").css("display", "block");
    $("#productAlsoAvailable, #productAlsoAvailable2").css("display", "block");
  });

    // Prevent vertical scrolling bounce on iPad - solution from http://gregsramblings.com/

    var xStart, yStart = 0;

    document.addEventListener('touchstart',function(e) {
        xStart = e.touches[0].screenX;
        yStart = e.touches[0].screenY;
    });

    document.addEventListener('touchmove',function(e) {
        var xMovement = Math.abs(e.touches[0].screenX - xStart);
        var yMovement = Math.abs(e.touches[0].screenY - yStart);
        if((yMovement * 1.5) > xMovement) {
            e.preventDefault();
        }
    });

    // WHEN THE APP HAS CACHED SUCCESSFULLY, RUN THE FOLLOWING FUNCTIONS:
    window.applicationCache.addEventListener('cached', function() {

        // Check to see if cookies have been set. If the cookie of 'country' doesn't exist...
        if (document.cookie.indexOf("country") == -1) {
          $("#app-setup-country-modal").modal("show"); // ...fire the Bootstrap JS modal window so the user can set it.
        }

        else {
        }

        alert("The Coast Bridesmaids Lookbook app has cached successfully. You can now use this offline.");

    }, false );

    // add an event listener to create a div that notifies the user that the app is downloading.

    window.applicationCache.addEventListener('progress', function(event) {
      $("#caching-progress").remove();
      $("body").prepend("<div id='caching-progress'></div>");
      $("#caching-progress").css({"position" : "absolute", "top" : "0", "background" : "green", "color" : "white", "text-align" : "center",
        "width" : "100%", "padding" : "20px 0px", "z-index" : "1", "font-size" : "24px"});

      $("#caching-progress").html("Downloading " + event.loaded + " items of " + event.total + " total. Please wait.");
    }, false);

    // remove this downloading div once the app has cached
    window.applicationCache.addEventListener('cached', function() {
        $("#caching-progress").remove();
    }, false );

}); // end script.