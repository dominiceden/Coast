/* This script controls the initial setup of the Bridesmaid app. It checks to see if cookies have been set for the
country and store that the app is being used in, and fires a modal window to set these cookies if they have not been
set yet.
*/

// Define a function to read cookies. We'll use this later to set JS variables for use in the app.

function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

$(document).ready(function(){

  // Initialise global variables
  countrycode = "";
  storeName = "other";
  storeHasGoodWiFi = false;

  // Set cookie that the user has used the Bridesmaids App - for purchase analytics. We'll test for this later.
  document.cookie = "customerHasUsedBridesmaidsTabletApp=true;";

  /* After the user has selected a country, they need to select a store so we can enable features that depend on good WiFi.
  $('#app-setup-country-modal').on('hidden.bs.modal', function() {
    if (document.cookie.indexOf("storeHasGoodWiFi") == -1) { // If the cookie 'storeHasGoodWiFi' doesn't exist and the country is UK (we have no Irish stores that have good WiFi)...
      $("#app-setup-store-modal").modal("show"); // ...fire the Bootstrap JS modal window so the user can set it.
    }
  }); */

  // Set cookies and JS variables when the country is selected

  $("#country-select-ireland").click(function() {
    document.cookie = "country=ireland;expires=Sun, 31 Dec 2034 23:59:59 GMT;";
    console.log("ireland clicked");
    countrycode = "IE";
    $(this).css("background", "#f9f9f9"); // style it so the user knows it is selected
    $("#country-select-uk").css("background", "#ffffff");
  });

  $("#country-select-uk").click(function() {
    document.cookie = "country=uk;expires=Sun, 31 Dec 2034 23:59:59 GMT;";
    console.log("uk clicked");
    countrycode = "UK";
    $(this).css("background", "#f9f9f9");
    $("#country-select-ireland").css("background", "#ffffff");
  });

  // When the app is opened, set JavaScript variables for country if cookies already exist from a previous session

  if (document.cookie.indexOf("country=ireland") >= 0) {
    countrycode = "IE";
  }

  else if (document.cookie.indexOf("country=uk") >= 0) {
    countrycode = "UK";
  }

  // Set cookies and JS variables when the store is selected

  $("#store-has-good-wifi button").click(function() {
    document.cookie = "storeHasGoodWiFi=true; expires=Sun, 31 Dec 2034 23:59:59 GMT;";
    console.log("store has got good wifi");
    storeHasGoodWiFi = true;

    storeName = $(this).attr("data-store-name"); // Get the data attribute data-store-name from the DOM when it is clicked and set it as a global variable
    console.log(storeName);
    document.cookie = "storeName=" + storeName + ";expires=Sun, 31 Dec 2034 23:59:59 GMT;" // Set the store name as a cookie.
  });

  $("#store-hasnt-got-good-wifi button").click(function() {
    document.cookie = "storeHasGoodWiFi=false; expires=Sun, 31 Dec 2034 23:59:59 GMT;";
    console.log("store hasnt got good wifi");
    storeHasGoodWiFi = false;

    document.cookie = "storeName=other;expires=Sun, 31 Dec 2034 23:59:59 GMT;";
    storeName = "other";
  });

  // When the app is opened, set JavaScript variables for store if cookies already exist from a previous session

  if (document.cookie.indexOf("storeHasGoodWiFi=true") >= 0) {
    storeHasGoodWiFi = true;
  }

  else if (document.cookie.indexOf("storeHasGoodWiFi=false") >= 0) {
    storeHasGoodWiFi = false;
  }

  if (document.cookie.indexOf("storeName") >= 0) { // If the storeName cookie exists...
    storeName = readCookie("storeName"); // Store the name of the store in a JS variable.
  }


}); // end script.