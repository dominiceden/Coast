/* README
   We have two scenarios. A product is available to buy on the website, or it is not. Regardless, we show a slideshow of images for the product.

   1) If it is not available yet, we have to retrieve the product information from a JSON object (in products.js) and write it to the DOM,
      mimicking Quick Buy. We also show an iframed Email Me When In Stock form (using eDialog) so customers can be notified when it is in stock.

   2) If it is available (available being defined as being on the website), we show the Quick Buy functionality via AJAX. We don't need to access
      the JSON object.

   The data-product-id attribute in the DOM is the connector between all the elements. When a product is clicked, we capture this product ID and
   use it to generate the iframe, AJAX'd Quick Buy and image slideshow. So the product ID must be correct, and the EMWIS HTML file, image folder and image files
   must be named according to this product ID.

   To add a new product (unavailable):
    * In the HTML, give the product element (<div class="product">) a descriptive and unique data-product-id attribute (e.g. "pale-blush-lori-lee").
    * Give it the attribute data-product-available="no".
    * Then create an entry for it in the products.js object.
    * Add in images into its folder (named by the descriptive product ID) and name the image files accordingly.
    * Create an EMWIS HTML file and name it according to the descriptive product ID.
    * When the product is clicked, the JS code will see data-product-available="no", show an EMWIS form and write other product information to the DOM.

   To add a new product (available):
    * In the HTML, place the 7 digit website product ID in the data-product-id attribute field for the product element (<div class="product">)
      (e.g. data-product-id="1405203").
    * Give it the attribute data-product-available="yes".
    * Add in images into its folder (named by the product ID) and name the image files appropriately.
    * When the product is clicked, the JS code will see data-product-available="yes" and load Quick Buy via AJAX.

   To upgrade a product from unavailable to available:
    * In tracking-product-ids.txt, make a note of the product ID you are changing and its new numerical ID.
    * Duplicate and upload the image folder - using the 7 digit product ID for the folder name and name the image files using the 7 digit product ID. Don't just rename it as we may need to switch back to the descriptive product ID (e.g. "pale-blush-lori-lee").
    * In products.js, change the colour variant for this product to "product-is-available". Make sure you do this for all other variants as variants reference each other.
    * In products.js, change the temporary descriptive product ID (e.g. "pale-blush-lori-lee") for the product and wherever else it is referred to the new 7 digit product ID. This is so when its colour swatch is clicked it will return a Quick Buy form.
    * In the HTML, place the 7 digit website product ID in the data-product-id attribute field for the product element (<div class="product">)
      (e.g. data-product-id="1405203").
    * Give it the attribute data-product-available="yes".
    * When the product is clicked, the JS code will see data-product-available="yes" and load the Quick Buy iframe. NB - check if Quick Buy works for this product first - it may be broken!
    * You don't need to rename or remove the EMWIS iframe as it won't be loaded.

*/

$(document).ready(function(){

  $('ul#filter-buttons-group li').click(function(){ // Scroll down to tab when tab button is clicked.
    $('html,body').animate({
        scrollTop: $(window).scrollTop() + 300
    }, 1000);
  });

  $('.product').click(function(){

    $('#clicked-product-info').remove(); // Remove any instances of the clicked product info div that shows product information.

    $(this).parent().prepend('<div id="clicked-product-info"><div id="clicked-product-info-left"></div><div id="clicked-product-info-right"></div>');

    var clickedProductID = $(this).attr('data-product-id'); // Capture product ID from item clicked on
    console.log(clickedProductID);

    $("#clicked-product-info").fadeIn(1000, function() {
      $('#clicked-product-info').show(); // If hidden, show the slideshow div so bxSlider can access it and initialise the slideshow. If it can't see it, it can't initialise.
    });

    $('html, body').animate({ // Smooth scroll to clicked-product-info div when it opens.
        scrollTop: $("#clicked-product-info").offset().top -110
    }, 1000);

    // Generate a bxSlider div and fill it with the product images
    $('#clicked-product-info-left').append('<ul class="bxslider">');
    $('.bxslider').append('<li><img src="http://www.coast-stores.com/pws/client/bridesmaid-style-book-2015/images/' + clickedProductID + '/' + clickedProductID + '-1.jpg" /></li>');
    $('.bxslider').append('<li><img src="http://www.coast-stores.com/pws/client/bridesmaid-style-book-2015/images/' + clickedProductID + '/' + clickedProductID + '-2.jpg" /></li>');
    $('.bxslider').append('<li><img src="http://www.coast-stores.com/pws/client/bridesmaid-style-book-2015/images/' + clickedProductID + '/' + clickedProductID + '-3.jpg" /></li>');
    $('.bxslider').append('</ul>');

    $('.bxslider').bxSlider({ // Initialise slider for this product's images
      pagerType: 'short',
      preloadImages: 'all',
      auto: true,
      onSliderLoad: function(){
        $('#tabs .bx-controls .bx-controls-direction a').hide().css('display', 'block').fadeIn(1000);
      }
    });

    /* START IFRAME & AJAX
    If product is not available, show an iframe for Email Me When Available.
    If product is not available, we also have to access the products.js JSON to retrieve and show other product information
    (e.g. petite & other colour ways).
    */
    if ( $(this).attr('data-product-available') == 'no') {
      console.log('product not available');
      $('#clicked-product-info-right').append('<div id="close-window"></div><div id="colour-swatch-container"></div>');

      $('#close-window').click(function(){ // Close the clicked product window when close icon clicked.
        $('#clicked-product-info').slideUp(1300);
      });

      // Take product ID and match it with a product in the JSON file products.js.
      for (i = 0; i < products.dresses.length; i++) {
        if (products.dresses[i].productid == clickedProductID) {
          $('#clicked-product-info-right').prepend('<div id="current-colour"><p>Selected colour: </p></div>');

          if ( (products.dresses[i].also_available_in).length > 0 ) {
            $('#clicked-product-info-right').prepend('<p id="petite">' + products.dresses[i].also_available_in + '</p>');
          }

          $('#clicked-product-info-right').prepend('<p id="size-range">' + products.dresses[i].size_range + '</p>');
          $('#clicked-product-info-right').prepend('<p>' + products.dresses[i].price + '</p>');
          $('#clicked-product-info-right').prepend('<h4>' + products.dresses[i].name + '</h4>');

          // Add Email Me When In Stock iframe only if it is not a Debenhams Exclusive and is not currently available.
          if ( products.dresses[i].debenhams_exclusive == false ) {
            $('#clicked-product-info-right').append('<h4 id="email-me" style="margin-top:20px;">Email Me When Available</h4>');
            $('#clicked-product-info-right').append('<div id="email-me-iframe"></div>');
            $('#email-me-iframe').load('http://www.coast-stores.com/pws/client/bridesmaid-style-book-2015/email-me-iframes/' + clickedProductID + '.html', function() {
              $('#q70532').datepicker({ dateFormat: 'dd/mm/yy' }); // Initialise datepicker on Date of Wedding field
            }); // end iframe load

          }

          else {
            $('#clicked-product-info-right').append('<p id="debenhams">Style only available at Debenhams</p>');
          }

          var productColours = products.dresses[i].colours;

          for (var key in productColours) { // Write the name of the current colour to the DOM.
            $('#current-colour').append('<p>&nbsp;' + [key] + '</p>');
            break // stop after the first result. In products.js, the first colour is always the colour of that product. All subsequent colours are colour variants of that product. So all we want to do is write the first result to the DOM.
          }

          for (var key in productColours) { // Loop through the colours and create divs with the product ID, hex and the colour name.
            $('#colour-swatch-container').append('<div class="colour-swatch-item"><div class="colour-swatch-item-circle" style="background:' + productColours[key][0] + '" data-product-id="' + productColours[key][1] + '" data-product-available="' + productColours[key][2] + '" data-debenhams-exclusive="' + productColours[key][3] + '" data-petite="' + productColours[key][4] + '" data-product-colour="' + [key] +'"></div></div>');
          };

        } // end if statement
      }; // end for loop

      $('.colour-swatch-item').first().css('background', '#c1c1c1'); // Highlight the first colour swatch offered. This is the current colour of the product, as in products.js, the first colour is always the colour of that product. All subsequent colours are colour variants of that product.

      // Add a click handler for when colour swatch circles are clicked on. This removes the slider, replaces with new images and reinitialises the slider.
      $('.colour-swatch-item-circle').click(function(){
        var clickedColourProductID = $(this).attr('data-product-id'); // Get the product ID of the colour swatch that was clicked on.
        $('#current-colour').html('<p>Selected colour: </p><p>&nbsp;' + $(this).attr('data-product-colour') + '</p>');

        $('#clicked-product-info-left').html(''); // Remove the existing slider
        $('<ul class="bxslider">').hide().appendTo($ ('#clicked-product-info-left') ).fadeIn(1000); // Add new slides
        $('.bxslider').append('<li><img src="http://www.coast-stores.com/pws/client/bridesmaid-style-book-2015/images/' + clickedColourProductID + '/' + clickedColourProductID + '-1.jpg" /></li>');
        $('.bxslider').append('<li><img src="http://www.coast-stores.com/pws/client/bridesmaid-style-book-2015/images/' + clickedColourProductID + '/' + clickedColourProductID + '-2.jpg" /></li>');
        $('.bxslider').append('<li><img src="http://www.coast-stores.com/pws/client/bridesmaid-style-book-2015/images/' + clickedColourProductID + '/' + clickedColourProductID + '-3.jpg" /></li>');
        $('.bxslider').append('</ul>');

        $('.bxslider').bxSlider({ // Initialise slider for this product's images
          pagerType: 'short',
          preloadImages: 'all',
          auto: true,
          onSliderLoad: function(){
            $('#tabs .bx-controls .bx-controls-direction a').hide().css('display', 'block').fadeIn(1000);
          }
        });

        $('.colour-swatch-item').each(function(){ // When the colour swatch is clicked, remove the highlight of the selected colour swatch.
          $(this).css('background', 'none');
        });

        $(this).parent().css('background', '#c1c1c1'); // When the colour swatch is clicked, set the highlight of the newly selected colour swatch.

        if ( $(this).attr('data-product-available') == 'product-not-available') { // If this product variant is NOT available, then show an Email Me When In Stock form.

          if ( $(this).attr('data-petite') == 'no-petite-version') { // If the product is not available in a Petite style or is Petite...
            $('p#petite').remove(); // Remove petite text from DOM
          }
          else { // If the product is available in a Petite style or is Petite, print the text stored in products.js.
            $('p#petite').remove();
            $('#size-range').append('<p id="petite">' + $(this).attr('data-petite') +'</p');
          } // End 'if product petite' test

          if ( $(this).attr('data-debenhams-exclusive') == 'debenhams-exclusive') { // If a Debenhams Exclusive, do not show Email Me When In Stock. Show a Debenhams message.
            $('h4#email-me').remove(); // Remove Email Me When Available text
            $('iframe#email-me-iframe').remove(); // Remove Email Me iframe
            $('#clicked-product-info-right').append('<p id="debenhams">Style only available at Debenhams</p>'); // Add Debenhams Exclusive text
          }
          else { // If the product isn't a Debenhams exclusive, then show the Email Me When In Stock iframe.
            $('#email-me-iframe').remove(); // Remove existing Email Me When In Stock iframe
            $('p#debenhams').remove(); // Remove Debenhams Exclusive text if left over from a Debenhams Exclusive
            $('h4#email-me').remove(); // Remove h4 if left over from a Debenhams Exclusive
            $('#clicked-product-info-right').append('<h4 id="email-me" style="margin-top:20px;">Email Me When Available</h4>');
            $('#clicked-product-info-right').append('<div id="email-me-iframe"></div>');
            $('#email-me-iframe').hide().load('http://www.coast-stores.com/pws/client/bridesmaid-style-book-2015/email-me-iframes/' + clickedColourProductID + '.html', function() {
              $('#q70532').datepicker({ dateFormat: 'dd/mm/yy' }); // Initialise datepicker on Date of Wedding field
              $('#email-me-iframe').fadeIn(1000);
            });
          } // End 'if Debenhams Exclusive' test


        } // End 'if product not available' test

        else { // If this product variant IS available, then load Quick Buy via AJAX.
          $('#clicked-product-info-right').load( ('http://www.coast-stores.com/pws/AJProductDetails.ice?layout=quickbuy.pop.layout&ProductID=' + clickedColourProductID), function() {

            $('#clicked-product-info-right').append('<div id="close-window"></div>'); // Add a close icon
            $('#close-window').click(function(){ // Close the clicked product window when close icon clicked.
              $('#clicked-product-info').slideUp(1300);
            });

            $('#colours').html('<div id="colour-swatch-container"></div>');
            $('#colour-swatch-container').html('<div id="current-colour"><p>Selected colour: </p></div>');

            for (i = 0; i < products.dresses.length; i++) {
              if (products.dresses[i].productid == clickedProductID) {
                console.log(products.dresses[i].name);

                var productColours = products.dresses[i].colours;

                for (var key in productColours) { // Write the name of the current colour to the DOM.
                  $('#current-colour').append('<p>&nbsp;' + [key] + '</p>');
                  break // stop after the first result. In products.js, the first colour is always the colour of that product. All subsequent colours are colour variants of that product.
                }

                for (var key in productColours) { // Loop through the colours and create divs with the product ID, hex and the colour name.
                  $('#colour-swatch-container').append('<div class="colour-swatch-item"><div class="colour-swatch-item-circle" style="background:' + productColours[key][0] + '" data-product-id="' + productColours[key][1] + '" data-product-available="' + productColours[key][2] + '" data-product-colour="' + [key] +'"></div></div>');
                };

              } // end if statement
            }; // end for loop

            var c = $("[name=ProductID]"); // Add a highlight to selected size on Quick Buy
            $("[name=ProductID]").change(function() {
                c.not(this).parent().removeClass("size_selected");
                $(this).parent().addClass("size_selected")
            });

            // Show a success message once a product is added to basket and prevent a redirect.
            $('[action="/pws/AJUpdateBasket.ice"]').submit(function() {
                var b = $(this),
                    c = $(".add-to-bag input[type=submit]").attr("id"),
                    a = b.serialize();
                productButton = b.find("#btn-buy-product");
                $.post(b.attr("action"), a, function(e) {
                    var d = Mustache.render(btf.tpl.basketResponse, JSON.parse(e));
                    $(".alert-success, .alert-error").remove();
                    $("#add-buttons").after(d);
                    btf.basket.ssnAjaxBasket.ssnRequests(true);
                    if (productButton) {
                        $(document).on("ssnload", function(g, f) {
                            btf.basket.ajaxBasket.stockMessages.generateMessage(a, f);
                            $(this).off("ssnload")
                        })
                    }
                    if (c === "btn-checkout") {
                        window.location = $("#mini-basket").find("a.mb-lnk").attr("href")
                    }
                });
                return false
            });

          }); // end AJAX .load
        }

      }); // End colour swatch click function

    } // End the if statement for when the clicked product is unavailable

    else { // If the product IS available, use AJAX to show Quick Buy. Use a callback to run code when load complete.
      $('#clicked-product-info-right').load( ('http://www.coast-stores.com/pws/AJProductDetails.ice?layout=quickbuy.pop.layout&ProductID=' + clickedProductID), function() {

        $('#clicked-product-info-right').append('<div id="close-window"></div>'); // Add a close icon
        $('#close-window').click(function(){ // Close the clicked product window when close icon clicked.
          $('#clicked-product-info').slideUp(1300);
        });

        $('#colours').html('<div id="colour-swatch-container"></div>');
        $('#colour-swatch-container').html('<div id="current-colour"><p>Selected colour: </p></div>');

        for (i = 0; i < products.dresses.length; i++) {
          if (products.dresses[i].productid == clickedProductID) {
            console.log(products.dresses[i].name);

            var productColours = products.dresses[i].colours;

            for (var key in productColours) { // Write the name of the current colour to the DOM.
              $('#current-colour').append('<p>&nbsp;' + [key] + '</p>');
              break // stop after the first result. In products.js, the first colour is always the colour of that product. All subsequent colours are colour variants of that product.
            }

            for (var key in productColours) { // Loop through the colours and create divs with the product ID, hex and the colour name.
              $('#colour-swatch-container').append('<div class="colour-swatch-item"><div class="colour-swatch-item-circle" style="background:' + productColours[key][0] + '" data-product-id="' + productColours[key][1] + '" data-product-available="' + productColours[key][2] + '" data-product-colour="' + [key] +'"></div></div>');
            };

          } // end if statement
        }; // end for loop

        var c = $("[name=ProductID]"); // Add a highlight to selected size on Quick Buy
        $("[name=ProductID]").change(function() {
            c.not(this).parent().removeClass("size_selected");
            $(this).parent().addClass("size_selected")
        });

        // Show a success message once a product is added to basket and prevent a redirect.
        $('[action="/pws/AJUpdateBasket.ice"]').submit(function() {
            var b = $(this),
                c = $(".add-to-bag input[type=submit]").attr("id"),
                a = b.serialize();
            productButton = b.find("#btn-buy-product");
            $.post(b.attr("action"), a, function(e) {
                var d = Mustache.render(btf.tpl.basketResponse, JSON.parse(e));
                $(".alert-success, .alert-error").remove();
                $("#add-buttons").after(d);
                btf.basket.ssnAjaxBasket.ssnRequests(true);
                if (productButton) {
                    $(document).on("ssnload", function(g, f) {
                        btf.basket.ajaxBasket.stockMessages.generateMessage(a, f);
                        $(this).off("ssnload")
                    })
                }
                if (c === "btn-checkout") {
                    window.location = $("#mini-basket").find("a.mb-lnk").attr("href")
                }
            });
            return false
        });

      }); // end AJAX .load
    } // end else statement
    // END IFRAMES & AJAX

  }); // End product click function

}); // End document.ready function
