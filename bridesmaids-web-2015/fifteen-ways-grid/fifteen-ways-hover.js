$(document).ready(function(){

  $("#fifteen-ways-grid-item-1").hover(
    function() {
      var overlay = $("<div class='fifteen-ways-overlay'><div class='fifteen-ways-overlay-circle'><div class='fifteen-ways-overlay-text'>Take hold of one strap and take over to the opposite shoulder. Join both straps at the shoulder and start twisting around each other to create a &lsquo;rope effect&rsquo;. Once you have twisted down to the waist, separate the straps and wrap around to the front. Return to the back of the body and secure by tying a bow.</div></div></div>");
      $(overlay).hide().appendTo(this).fadeIn(500);
    },
    function() {
      $(".fifteen-ways-overlay").remove();
    }
  );

  $("#fifteen-ways-grid-item-2").hover(
    function() {
      var overlay = $("<div class='fifteen-ways-overlay'><div class='fifteen-ways-overlay-circle'><div class='fifteen-ways-overlay-text'>Recruit best friend alert! This style requires a bit of help. Hold each strap just above the chest and cross them over and under the armpits. Wrap the straps around your chest a couple of times to shorten the excess material and then secure at the back with a big beautiful bow.</div></div></div>");
      $(overlay).hide().appendTo(this).fadeIn(500);
    },
    function() {
      $(".fifteen-ways-overlay").remove();
    }
  );

  $("#fifteen-ways-grid-item-3").hover(
    function() {
      var overlay = $("<div class='fifteen-ways-overlay'><div class='fifteen-ways-overlay-circle'><div class='fifteen-ways-overlay-text'>Take one strap over each shoulder and start twisting each strap separately from the shoulder point. Cross over at the back and wrap around the waist. Keep twisting single straps all the way to the front of the body and then return to the back. Secure the straps by finishing off with a bow.</div></div></div>");
      $(overlay).hide().appendTo(this).fadeIn(500);
    },
    function() {
      $(".fifteen-ways-overlay").remove();
    }
  );

  $("#fifteen-ways-grid-item-4").hover(
    function() {
      var overlay = $("<div class='fifteen-ways-overlay'><div class='fifteen-ways-overlay-circle'><div class='fifteen-ways-overlay-text'>Take the straps straight up over the shoulders and under the armhole. Wrap around the bust to create the bandeau and then secure at the back by tying in to a bow. You can adjust the width of the sleeves by pulling out at the shoulder.</div></div></div>");
      $(overlay).hide().appendTo(this).fadeIn(500);
    },
    function() {
      $(".fifteen-ways-overlay").remove();
    }
  );

  $("#fifteen-ways-grid-item-5").hover(
    function() {
      var overlay = $("<div class='fifteen-ways-overlay'><div class='fifteen-ways-overlay-circle'><div class='fifteen-ways-overlay-text'>Knot the straps at your collarbone and then take over the shoulders. Twist both  of the straps at the centre of the back all the way down to the waist point. Separate the straps and wrap around the front of the body. Secure the straps by tying  them in to a bow at the back.</div></div></div>");
      $(overlay).hide().appendTo(this).fadeIn(500);
    },
    function() {
      $(".fifteen-ways-overlay").remove();
    }
  );

  $("#fifteen-ways-grid-item-6").hover(
    function() {
      var overlay = $("<div class='fifteen-ways-overlay'><div class='fifteen-ways-overlay-circle'><div class='fifteen-ways-overlay-text'>Take the straps straight up and over the shoulder. Cross them over at the back and then return to the front of the body at the waist level. Wrap around the body and then tie at the back with a knot or a bow.</div></div></div>");
      $(overlay).hide().appendTo(this).fadeIn(500);
    },
    function() {
      $(".fifteen-ways-overlay").remove();
    }
  );

  $("#fifteen-ways-grid-item-7").hover(
    function() {
      var overlay = $("<div class='fifteen-ways-overlay'><div class='fifteen-ways-overlay-circle'><div class='fifteen-ways-overlay-text'>Take one strap over each shoulder and one at a time, start twisting each strap from the shoulder point down the back. Cross over at the back and then wrap the straps around the front of the waist. To finish, tie at the back with a knot or bow.</div></div></div>");
      $(overlay).hide().appendTo(this).fadeIn(500);
    },
    function() {
      $(".fifteen-ways-overlay").remove();
    }
  );

  $("#fifteen-ways-grid-item-8").hover(
    function() {
      var overlay = $("<div class='fifteen-ways-overlay'><div class='fifteen-ways-overlay-circle'><div class='fifteen-ways-overlay-text'>Cross the straps at the front and over the opposite shoulders. Hook one strap over the other by crossing the straps at the centre of the back. Wrap the straps around the front of the waist and then return to the back of the body. Secure the straps by tying in to a bow.</div></div></div>");
      $(overlay).hide().appendTo(this).fadeIn(500);
    },
    function() {
      $(".fifteen-ways-overlay").remove();
    }
  );

  $("#fifteen-ways-grid-item-9").hover(
    function() {
      var overlay = $("<div class='fifteen-ways-overlay'><div class='fifteen-ways-overlay-circle'><div class='fifteen-ways-overlay-text'>Take both straps and &lsquo;fan&rsquo; out over each shoulder. Cross the straps, flat, at the back and return to the front of the body. Wrap around the waist and then tie at the back with a knot or bow.</div></div></div>");
      $(overlay).hide().appendTo(this).fadeIn(500);
    },
    function() {
      $(".fifteen-ways-overlay").remove();
    }
  );

  $("#fifteen-ways-grid-item-10").hover(
    function() {
      var overlay = $("<div class='fifteen-ways-overlay'><div class='fifteen-ways-overlay-circle'><div class='fifteen-ways-overlay-text'>Easy Peasy...Take both straps over each shoulder and simply tie at the back with a big beautiful bow. Let the tie hang loose all the way to the hem.</div></div></div>");
      $(overlay).hide().appendTo(this).fadeIn(500);
    },
    function() {
      $(".fifteen-ways-overlay").remove();
    }
  );

  $("#fifteen-ways-grid-item-11").hover(
    function() {
      var overlay = $("<div class='fifteen-ways-overlay'><div class='fifteen-ways-overlay-circle'><div class='fifteen-ways-overlay-text'>Take one strap over each shoulder and start twisting each strap separately from the shoulder point (without crossing at the back). Bring the twisted straps to the front of the body and wrap around the waist returning the straps to the back of the body. &lsquo;Hook&rsquo; each strap to the straps already at the back, and then tie at the back of the body with a knot or bow.</div></div></div>");
      $(overlay).hide().appendTo(this).fadeIn(500);
    },
    function() {
      $(".fifteen-ways-overlay").remove();
    }
  );

  $("#fifteen-ways-grid-item-12").hover(
    function() {
      var overlay = $("<div class='fifteen-ways-overlay'><div class='fifteen-ways-overlay-circle'><div class='fifteen-ways-overlay-text'>Take both straps up and over one shoulder. Twist the straps together until you get to the waist position. Then separate the straps and wrap around the front of the body. Bring the ties to the back and then secure with a knot or bow.</div></div></div>");
      $(overlay).hide().appendTo(this).fadeIn(500);
    },
    function() {
      $(".fifteen-ways-overlay").remove();
    }
  );

  $("#fifteen-ways-grid-item-13").hover(
    function() {
      var overlay = $("<div class='fifteen-ways-overlay'><div class='fifteen-ways-overlay-circle'><div class='fifteen-ways-overlay-text'>Take both straps up and over one shoulder. Fan out both straps to create a wide sleeve. Once happy with sleeve length, start twisting both straps together diagonally across the back. Separate the straps when the waist level has been reached and then wrap around the front of the body. To finish, tie the straps at the back with a knot or a bow.</div></div></div>");
      $(overlay).hide().appendTo(this).fadeIn(500);
    },
    function() {
      $(".fifteen-ways-overlay").remove();
    }
  );

  $("#fifteen-ways-grid-item-14").hover(
    function() {
      var overlay = $("<div class='fifteen-ways-overlay'><div class='fifteen-ways-overlay-circle'><div class='fifteen-ways-overlay-text'>Knot the straps at the collarbone level and fan straps out over the shoulders. Cross over at the back (flat) and then wrap around the front of the waist. Bring the ties to the back of the body and finish off with a knot or a bow.</div></div></div>");
      $(overlay).hide().appendTo(this).fadeIn(500);
    },
    function() {
      $(".fifteen-ways-overlay").remove();
    }
  );

  $("#fifteen-ways-grid-item-15").hover(
    function() {
      var overlay = $("<div class='fifteen-ways-overlay'><div class='fifteen-ways-overlay-circle'><div class='fifteen-ways-overlay-text'>Take one strap over each shoulder and cross over and twist twice at the centre of the back. Wrap the ties around the front of the waist. Bring the ties to the back of the body and  secure with a knot or a bow.</div></div></div>");
      $(overlay).hide().appendTo(this).fadeIn(500);
    },
    function() {
      $(".fifteen-ways-overlay").remove();
    }
  );

}); // end document.ready function