/* PRODUCTS WITH NO IMAGES YET. Add these back in to the main file once we have images.



{
  "productid": "pale-blush-jimena-sparkle",
  "name": "Jimena Sparkle Dress",
  "price": "&pound;95.00",
  "EURprice": "&euro;120.00",
  "date_available": "Coming in July",
      "size_range": "Size range: 6-18",
  "colours": {
      "Pale Pink": ["#FFE3E8", "pale-blush-jimena-sparkle", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
  },
  "in_5_or_15": false,
      "also_available_in": "",
  "debenhams_exclusive": false,
  "limited_edition": false
},
{
  "productid": "black-and-white-lori-lee-cluster-maxi",
  "name": "Lori Lee Cluster Maxi Dress",
  "price": "&pound;189.00",
  "EURprice": "&euro;245.00",
  "date_available": "Coming in October/November",
      "size_range": "Size range: 6-18",
  "colours": {
      "Black": ["#000000", "black-and-white-lori-lee-cluster-maxi", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
      "Thyme": ["#75A3A3", "greens-lori-lee-cluster-maxi", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
      "Pale Blue": ["#D1E8FF", "blue-hues-lori-lee-cluster-maxi", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
  },
  "in_5_or_15": false,
      "also_available_in": "",
  "debenhams_exclusive": false,
  "limited_edition": false
},
{
  "productid": "blue-hues-lori-lee-cluster-maxi",
  "name": "Lori Lee Cluster Maxi Dress",
  "price": "&pound;189.00",
  "EURprice": "&euro;245.00",
  "date_available": "Coming in April/May",
      "size_range": "Size range: 6-18",
  "colours": {
      "Pale Blue": ["#D1E8FF", "blue-hues-lori-lee-cluster-maxi", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
      "Black": ["#000000", "black-and-white-lori-lee-cluster-maxi", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
      "Thyme": ["#75A3A3", "greens-lori-lee-cluster-maxi", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
  },
  "in_5_or_15": false,
      "also_available_in": "",
  "debenhams_exclusive": false,
  "limited_edition": false
},
{
  "productid": "greens-lori-lee-cluster-maxi",
  "name": "Lori Lee Cluster Maxi Dress",
  "price": "&pound;189.00",
  "EURprice": "&euro;245.00",
  "date_available": "Coming in April/May",
      "size_range": "Size range: 6-18",
  "colours": {
      "Thyme": ["#75A3A3", "greens-lori-lee-cluster-maxi", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
      "Pale Blue": ["#D1E8FF", "blue-hues-lori-lee-cluster-maxi", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
      "Black": ["#000000", "black-and-white-lori-lee-cluster-maxi", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
  },
  "in_5_or_15": false,
      "also_available_in": "",
  "debenhams_exclusive": false,
  "limited_edition": false
},
 {
  "productid": "blue-hues-lori-ella",
  "name": "Lori Ella Dress",
  "price": "&pound;189.00",
  "EURprice": "&euro;245.00",
  "date_available": "",
      "size_range": "Size range: 6-18",
  "colours": {
      "Navy": ["#00004A", "blue-hues-lori-ella", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
  },
  "in_5_or_15": false,
      "also_available_in": "",
  "debenhams_exclusive": false,
  "limited_edition": false
},
{
  "productid": "blue-hues-lori-lee-lace-short",
  "name": "Lori Lee Lace Short Dress",
  "price": "&pound;129.00",
  "EURprice": "&euro;159.00",
  "date_available": "",
      "size_range": "Size range: 6-18",
  "colours": {
      "Sky Blue": ["#3366FF", "blue-hues-lori-lee-lace-short", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
      "Navy": ["#00004A", "1314520", "product-is-available", "not-debenhams-exclusive", "no-petite-version"]
  },
  "in_5_or_15": false,
      "also_available_in": "",
  "debenhams_exclusive": false,
  "limited_edition": false
},
{
  "productid": "blue-hues-sapphire",
  "name": "Sapphire Dress",
  "price": "&pound;195.00",
  "EURprice": "&euro;245.00",
  "date_available": "Coming in July",
      "size_range": "Size range: 6-18",
  "colours": {
      "Cobalt": ["#2E2EB8", "blue-hues-sapphire", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
  },
  "in_5_or_15": false,
      "also_available_in": "",
  "debenhams_exclusive": false,
  "limited_edition": false
},


*/