/* README

   This is the file that contains the product information for all products for the Bridesmaid Style Book 2015 - for the website and the app.

   The "colours" key sometimes holds multiple array values (including the product ID for the colour variant) so that we can change the slideshow to
   show multiple colour ways when the user clicks a colour swatch (the product ID is required to load images). The schema for the colours is:

   "Blush": ["#FFEAEA", "pale-blush-lori-lee-sparkle-maxi", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
   WHERE:
   "NAME OF COLOUR": ["HEX CODE", "PRODUCT ID", "CURRENT AVAILABILITY FOR THIS PRODUCT VARIANT", "PRODUCT IS NOT A DEBENHAMS EXCLUSIVE", "THERE IS NO PETITE VERSION OF THIS PRODUCT OR THIS PRODUCT IS NOT PETITE"]

   We have to set current product availability in the database (products.js) as it may well be that an available product is accessed from an unavailable
   product through the colour swatches.

*/

var products = {
  "dresses": [
    {
      "productid": "pale-blush-lori-lee-sparkle-maxi",
      "name": "Lori Lee Sparkle Maxi Dress",
      "price": "&pound;189.00",
      "EURprice": "&euro;245.00",
      "date_available": "Coming in July",
      "size_range": "Size range: 6-18",
      "colours": {
          "Blush": ["#FFEAEA", "pale-blush-lori-lee-sparkle-maxi", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "pale-blush-lori-lee-short",
      "name": "Lori Lee Short Dress",
      "price": "&pound;129.00",
      "EURprice": "&euro;159.00",
      "date_available": "Coming in July",
      "size_range": "Size range: 6-18",
      "colours": {
          "Dusky Pink": ["#EEC9CF", "pale-blush-lori-lee-short", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Dusky Lilac": ["#A389BE", "warm-pastels-lori-lee-short", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Slate": ["#344E69", "blue-hues-lori-lee-short-slate", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Cobalt": ["#2E2EB8", "blue-hues-lori-lee-short-cobalt", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Navy": ["#00004A", "blue-hues-lori-lee-short-navy", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Forest Green": ["#003F00", "greens-lori-lee-short", "product-not-available", "not-debenhams-exclusive", "Also available in Maxi length"]
      },
      "in_5_or_15": false,
      "also_available_in": "Also available in Maxi length",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "pale-blush-minnie-maxi",
      "name": "Minnie Maxi Dress",
      "price": "&pound;225.00",
      "EURprice": "&euro;285.00",
      "date_available": "Coming in July",
      "size_range": "Size range: 6-18",
      "colours": {
          "Blush": ["#FFEAEA", "pale-blush-minnie-maxi", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Navy": ["#00004A", "blue-hues-minnie-maxi", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": true
    },
    {
      "productid": "pale-blush-tina-rose",
      "name": "Tina Rose Dress",
      "price": "&pound;429.00",
      "EURprice": "&euro;549.00",
      "date_available": "Coming in October/November",
      "size_range": "Size range: 6-18",
      "colours": {
          "Blush": ["#FFEAEA", "pale-blush-tina-rose", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "pale-blush-iridesa-skirt",
      "name": "Iridesa Skirt",
      "price": "&pound;129.00",
      "EURprice": "&euro;159.00",
      "date_available": "In Store Now",
      "size_range": "Size range: 6-18",
      "colours": {
          "Blush": ["#FFEAEA", "pale-blush-iridesa-skirt", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "pale-blush-natalia-top",
      "name": "Natalia Top",
      "price": "&pound;119.00",
      "EURprice": "&euro;150.00",
      "date_available": "Coming in July",
      "size_range": "Size range: 6-18",
      "colours": {
          "Silver": ["#CEC3C1", "pale-blush-natalia-top", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "pale-blush-corwin-maxi-dusky-pink",
      "name": "Corwin Maxi Dress",
      "price": "&pound;129.00",
      "EURprice": "&euro;159.00",
      "date_available": "Coming in July",
      "size_range": "Size range: 6-18",
      "colours": {
          "Dusky Pink": ["#EEC9CF", "pale-blush-corwin-maxi-dusky-pink", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Mocha": ["#A07E7E", "black-and-white-corwin-maxi-mocha", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Black": ["#000000", "black-and-white-corwin-maxi-black", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Cobalt": ["#2E2EB8", "blue-hues-corwin-maxi-cobalt", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Raspberry": ["#B8004A", "red-and-pink-corwin-maxi", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Navy": ["#00004A", "blue-hues-corwin-maxi-navy", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Blush": ["#FFEAEA", "pale-blush-corwin-maxi-blush", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Pale Blue": ["#D1E8FF", "blue-hues-corwin-maxi-pale-blue", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": true,
      "in_5_or_15_images": {
        "in5or15images1": "../images/corwin-15-ways-to-wear/01.jpg",
        "in5or15images2": "../images/corwin-15-ways-to-wear/02.jpg",
        "in5or15images3": "../images/corwin-15-ways-to-wear/03.jpg",
        "in5or15images4": "../images/corwin-15-ways-to-wear/04.jpg",
        "in5or15images5": "../images/corwin-15-ways-to-wear/05.jpg",
        "in5or15images6": "../images/corwin-15-ways-to-wear/06.jpg",
        "in5or15images1": "../images/corwin-15-ways-to-wear/07.jpg",
        "in5or15images2": "../images/corwin-15-ways-to-wear/08.jpg",
        "in5or15images3": "../images/corwin-15-ways-to-wear/09.jpg",
        "in5or15images4": "../images/corwin-15-ways-to-wear/10.jpg",
        "in5or15images5": "../images/corwin-15-ways-to-wear/11.jpg",
        "in5or15images6": "../images/corwin-15-ways-to-wear/12.jpg",
        "in5or15images1": "../images/corwin-15-ways-to-wear/13.jpg",
        "in5or15images2": "../images/corwin-15-ways-to-wear/14.jpg",
        "in5or15images3": "../images/corwin-15-ways-to-wear/15.jpg",
      },
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "pale-blush-corwin-maxi-blush",
      "name": "Corwin Maxi Dress",
      "price": "&pound;129.00",
      "EURprice": "&euro;159.00",
      "date_available": "Coming in April/May",
      "size_range": "Size range: 6-18",
      "colours": {
          "Blush": ["#FFEAEA", "pale-blush-corwin-maxi-blush", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Dusky Pink": ["#EEC9CF", "pale-blush-corwin-maxi-dusky-pink", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Mocha": ["#A07E7E", "black-and-white-corwin-maxi-mocha", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Black": ["#000000", "black-and-white-corwin-maxi-black", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Cobalt": ["#2E2EB8", "blue-hues-corwin-maxi-cobalt", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Raspberry": ["#B8004A", "red-and-pink-corwin-maxi", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Navy": ["#00004A", "blue-hues-corwin-maxi-navy", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Pale Blue": ["#D1E8FF", "blue-hues-corwin-maxi-pale-blue", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": true,
      "in_5_or_15_images": {
        "in5or15images1": "../images/corwin-15-ways-to-wear/01.jpg",
        "in5or15images2": "../images/corwin-15-ways-to-wear/02.jpg",
        "in5or15images3": "../images/corwin-15-ways-to-wear/03.jpg",
        "in5or15images4": "../images/corwin-15-ways-to-wear/04.jpg",
        "in5or15images5": "../images/corwin-15-ways-to-wear/05.jpg",
        "in5or15images6": "../images/corwin-15-ways-to-wear/06.jpg",
        "in5or15images1": "../images/corwin-15-ways-to-wear/07.jpg",
        "in5or15images2": "../images/corwin-15-ways-to-wear/08.jpg",
        "in5or15images3": "../images/corwin-15-ways-to-wear/09.jpg",
        "in5or15images4": "../images/corwin-15-ways-to-wear/10.jpg",
        "in5or15images5": "../images/corwin-15-ways-to-wear/11.jpg",
        "in5or15images6": "../images/corwin-15-ways-to-wear/12.jpg",
        "in5or15images1": "../images/corwin-15-ways-to-wear/13.jpg",
        "in5or15images2": "../images/corwin-15-ways-to-wear/14.jpg",
        "in5or15images3": "../images/corwin-15-ways-to-wear/15.jpg",
      },
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "pale-blush-corwin-short",
      "name": "Corwin Short Dress",
      "price": "&pound;89.00",
      "EURprice": "&euro;115.00",
      "date_available": "Coming in April/May",
      "size_range": "Size range: 6-18",
      "colours": {
          "Blush": ["#FFEAEA", "pale-blush-corwin-short", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": true,
      "in_5_or_15_images": {
        "in5or15images1": "../images/corwin-15-ways-to-wear/01.jpg",
        "in5or15images2": "../images/corwin-15-ways-to-wear/02.jpg",
        "in5or15images3": "../images/corwin-15-ways-to-wear/03.jpg",
        "in5or15images4": "../images/corwin-15-ways-to-wear/04.jpg",
        "in5or15images5": "../images/corwin-15-ways-to-wear/05.jpg",
        "in5or15images6": "../images/corwin-15-ways-to-wear/06.jpg",
        "in5or15images1": "../images/corwin-15-ways-to-wear/07.jpg",
        "in5or15images2": "../images/corwin-15-ways-to-wear/08.jpg",
        "in5or15images3": "../images/corwin-15-ways-to-wear/09.jpg",
        "in5or15images4": "../images/corwin-15-ways-to-wear/10.jpg",
        "in5or15images5": "../images/corwin-15-ways-to-wear/11.jpg",
        "in5or15images6": "../images/corwin-15-ways-to-wear/12.jpg",
        "in5or15images1": "../images/corwin-15-ways-to-wear/13.jpg",
        "in5or15images2": "../images/corwin-15-ways-to-wear/14.jpg",
        "in5or15images3": "../images/corwin-15-ways-to-wear/15.jpg",
      },
      "also_available_in": "Also available in Maxi length",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "pale-blush-lori-lee-maxi",
      "name": "Lori Lee Maxi Dress",
      "price": "&pound;185.00",
      "EURprice": "&euro;230.00",
      "date_available": "Coming in July",
      "size_range": "Size range: 6-18",
      "colours": {
          "Dusky Pink": ["#EEC9CF", "pale-blush-lori-lee-maxi", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Dusky Lilac": ["#A389BE", "warm-pastels-lori-lee-maxi-dusky-lilac", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Slate": ["#344E69", "blue-hues-lori-lee-maxi-slate", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Navy": ["#00004A", "1411920", "product-is-available", "not-debenhams-exclusive", "no-petite-version"],
          "Forest Green": ["#003F00", "greens-lori-lee-maxi", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "warm-pastels-adelina",
      "name": "Adelina Dress",
      "price": "&pound;195.00",
      "EURprice": "&euro;245.00",
      "date_available": "In Store Now",
      "size_range": "Size range: 6-18",
      "colours": {
          "Dusky Lilac": ["#A389BE", "warm-pastels-adelina", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Merlot": ["#4E1226", "berry-tones-adelina", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "warm-pastels-holmes",
      "name": "Holmes Dress",
      "price": "&pound;179.00",
      "EURprice": "&euro;225.00",
      "date_available": "Coming in August/September",
      "size_range": "Size range: 6-18",
      "colours": {
          "Dusky Lilac": ["#A389BE", "warm-pastels-holmes", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "warm-pastels-lori-lee-maxi-dusky-lilac",
      "name": "Lori Lee Maxi Dress",
      "price": "&pound;185.00",
      "EURprice": "&euro;230.00",
      "date_available": "Coming in July",
      "size_range": "Size range: 6-18",
      "colours": {
          "Dusky Lilac": ["#A389BE", "warm-pastels-lori-lee-maxi-dusky-lilac", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Dusky Pink": ["#EEC9CF", "pale-blush-lori-lee-maxi", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Slate": ["#344E69", "blue-hues-lori-lee-maxi-slate", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Navy": ["#00004A", "1411920", "product-is-available", "not-debenhams-exclusive", "no-petite-version"],
          "Forest Green": ["#003F00", "greens-lori-lee-maxi", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "warm-pastels-lori-lee-short",
      "name": "Lori Lee Short Dress",
      "price": "&pound;129.00",
      "EURprice": "&euro;159.00",
      "date_available": "Coming in July",
      "size_range": "Size range: 6-18",
      "colours": {
          "Dusky Lilac": ["#A389BE", "warm-pastels-lori-lee-short", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Dusky Pink": ["#EEC9CF", "pale-blush-lori-lee-short", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Slate": ["#344E69", "blue-hues-lori-lee-short-slate", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Cobalt": ["#2E2EB8", "blue-hues-lori-lee-short-cobalt", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Navy": ["#00004A", "blue-hues-lori-lee-short-navy", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Forest Green": ["#003F00", "greens-lori-lee-short", "product-not-available", "not-debenhams-exclusive", "Also available in Maxi length"]
      },
      "in_5_or_15": false,
      "also_available_in": "Also available in Maxi length",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "warm-pastels-fernanda",
      "name": "Fernanda Dress",
      "price": "&pound;179.00",
      "EURprice": "&euro;225.00",
      "date_available": "Coming in August/September",
      "size_range": "Size range: 6-18",
      "colours": {
          "Smokey Grey": ["#C9D2E6", "warm-pastels-fernanda", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Thyme": ["#B5CECE", "greens-fernanda", "product-not-available", "debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "warm-pastels-natalia-skirt",
      "name": "Natalia Skirt",
      "price": "&pound;139.00",
      "EURprice": "&euro;175.00",
      "date_available": "Coming in July",
      "size_range": "Size range: 6-18",
      "colours": {
          "Dusky Lilac": ["#A389BE", "warm-pastels-natalia-skirt", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "berry-tones-adelina",
      "name": "Adelina Dress",
      "price": "&pound;195.00",
      "EURprice": "&euro;245.00",
      "date_available": "Coming in July",
      "size_range": "Size range: 6-18",
      "colours": {
          "Merlot": ["#4E1226", "berry-tones-adelina", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Dusky Lilac": ["#A389BE", "warm-pastels-adelina", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "berry-tones-lori-may-short",
      "name": "Lori May Short Dress",
      "price": "&pound;129.00",
      "EURprice": "&euro;159.00",
      "date_available": "Coming in July",
      "size_range": "Size range: 6-18",
      "colours": {
          "Grape": ["#440044", "berry-tones-lori-may-short", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Kingfisher": ["#166161", "greens-lori-may-short", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Navy": ["#00004A", "blue-hues-lori-may-short", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "berry-tones-lori-may-maxi",
      "name": "Lori May Maxi Dress",
      "price": "&pound;189.00",
      "EURprice": "&euro;245.00",
      "date_available": "Coming in July",
      "size_range": "Size range: 6-18",
      "colours": {
          "Grape": ["#440044", "berry-tones-lori-may-maxi", "product-not-available", "not-debenhams-exclusive", "Also available in Petite"],
          "Kingfisher": ["#166161", "greens-lori-may-maxi", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Navy": ["#00004A", "blue-hues-lori-may-maxi", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "Also available in Petite",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "berry-tones-sleeved-lori-lee-short",
      "name": "Sleeved Lori Lee Short Dress",
      "price": "&pound;139.00",
      "EURprice": "&euro;175.00",
      "date_available": "Coming in July",
      "size_range": "Size range: 6-18",
      "colours": {
          "Grape": ["#440044", "berry-tones-sleeved-lori-lee-short", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "berry-tones-lucianna",
      "name": "Lucianna Dress",
      "price": "&pound;195.00",
      "EURprice": "&euro;245.00",
      "date_available": "Coming in August/September",
      "size_range": "Size range: 6-18",
      "colours": {
          "Mocha": ["#A07E7E", "berry-tones-lucianna", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Ivory": ["#FFFFFF", "black-and-white-lucianna", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "red-and-pink-sandrina",
      "name": "Sandrina Dress",
      "price": "&pound;169.00",
      "EURprice": "&euro;215.00",
      "date_available": "Coming in July",
      "size_range": "Size range: 6-18",
      "colours": {
          "Red": ["#CC0000", "red-and-pink-sandrina", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "red-and-pink-corwin-maxi",
      "name": "Corwin Maxi Dress",
      "price": "&pound;129.00",
      "EURprice": "&euro;159.00",
      "date_available": "Coming in October/November",
      "size_range": "Size range: 6-18",
      "colours": {
          "Raspberry": ["#B8004A", "red-and-pink-corwin-maxi", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Blush": ["#FFEAEA", "pale-blush-corwin-maxi-blush", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Dusky Pink": ["#EEC9CF", "pale-blush-corwin-maxi-dusky-pink", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Mocha": ["#A07E7E", "black-and-white-corwin-maxi-mocha", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Black": ["#000000", "black-and-white-corwin-maxi-black", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Cobalt": ["#2E2EB8", "blue-hues-corwin-maxi-cobalt", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Navy": ["#00004A", "blue-hues-corwin-maxi-navy", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Pale Blue": ["#D1E8FF", "blue-hues-corwin-maxi-pale-blue", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": true,
      "in_5_or_15_images": {
        "in5or15images1": "../images/corwin-15-ways-to-wear/01.jpg",
        "in5or15images2": "../images/corwin-15-ways-to-wear/02.jpg",
        "in5or15images3": "../images/corwin-15-ways-to-wear/03.jpg",
        "in5or15images4": "../images/corwin-15-ways-to-wear/04.jpg",
        "in5or15images5": "../images/corwin-15-ways-to-wear/05.jpg",
        "in5or15images6": "../images/corwin-15-ways-to-wear/06.jpg",
        "in5or15images1": "../images/corwin-15-ways-to-wear/07.jpg",
        "in5or15images2": "../images/corwin-15-ways-to-wear/08.jpg",
        "in5or15images3": "../images/corwin-15-ways-to-wear/09.jpg",
        "in5or15images4": "../images/corwin-15-ways-to-wear/10.jpg",
        "in5or15images5": "../images/corwin-15-ways-to-wear/11.jpg",
        "in5or15images6": "../images/corwin-15-ways-to-wear/12.jpg",
        "in5or15images1": "../images/corwin-15-ways-to-wear/13.jpg",
        "in5or15images2": "../images/corwin-15-ways-to-wear/14.jpg",
        "in5or15images3": "../images/corwin-15-ways-to-wear/15.jpg",
      },
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "red-and-pink-giuglia",
      "name": "Giuglia Dress",
      "price": "&pound;95.00",
      "EURprice": "&euro;120.00",
      "date_available": "Coming in July",
      "size_range": "Size range: 6-18",
      "colours": {
          "Pink": ["#E62E8A", "red-and-pink-giuglia", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Navy": ["#00004A", "blue-hues-giuglia-navy", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Cornflower": ["#75A3FF", "blue-hues-giuglia-cornflower", "product-not-available", "not-debenhams-exclusive", "Petite"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "black-and-white-silvia",
      "name": "Silvia Dress",
      "price": "&pound;250.00",
      "EURprice": "&euro;315.00",
      "date_available": "Coming in July",
      "size_range": "Size range: 6-18",
      "colours": {
          "Neutral": ["#FFF3F3", "black-and-white-silvia", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "black-and-white-kimly-bow-maxi-dress",
      "name": "Kimly Bow Maxi Dress",
      "price": "&pound;195.00",
      "EURprice": "&euro;245.00",
      "date_available": "Coming in July",
      "size_range": "Size range: 6-18",
      "colours": {
          "Mono": ["#000000", "black-and-white-kimly-bow-maxi-dress", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "black-and-white-ceceil",
      "name": "Ceceil Dress",
      "price": "&pound;225.00",
      "EURprice": "&euro;285.00",
      "date_available": "Coming in July",
      "size_range": "Size range: 6-18",
      "colours": {
          "Mono": ["#000000", "black-and-white-ceceil", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "black-and-white-lucianna",
      "name": "Lucianna Dress",
      "price": "&pound;195.00",
      "EURprice": "&euro;245.00",
      "date_available": "Coming in August/September",
      "size_range": "Size range: 6-18",
      "colours": {
          "Ivory": ["#FFFFFF", "black-and-white-lucianna", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Mocha": ["#A07E7E", "berry-tones-lucianna", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "black-and-white-veeda-maxi-dress",
      "name": "Veeda Maxi Dress",
      "price": "&pound;350.00",
      "EURprice": "&euro;450.00",
      "date_available": "Coming in August/September",
      "size_range": "Size range: 6-18",
      "colours": {
          "White": ["#FFFFFF", "black-and-white-veeda-maxi-dress", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "black-and-white-kinsella-maxi",
      "name": "Kinsella Maxi Dress",
      "price": "&pound;195.00",
      "EURprice": "&euro;245.00",
      "date_available": "Coming in July",
      "size_range": "Size range: 6-18",
      "colours": {
          "Mono": ["#000000", "black-and-white-kinsella-maxi", "product-not-available", "debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": true,
      "limited_edition": false
    },
    {
      "productid": "black-and-white-catrin",
      "name": "Catrin Dress",
      "price": "&pound;139.00",
      "EURprice": "&euro;175.00",
      "date_available": "Coming in August/September",
      "size_range": "Size range: 6-18",
      "colours": {
          "Black": ["#000000", "black-and-white-catrin", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "black-and-white-astrid",
      "name": "Astrid Dress",
      "price": "&pound;225.00",
      "EURprice": "&euro;285.00",
      "date_available": "Coming in August/September",
      "size_range": "Size range: 6-18",
      "colours": {
          "Black and Blush": ["#000000", "black-and-white-astrid", "product-not-available", "debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": true,
      "limited_edition": false
    },
    {
      "productid": "black-and-white-corwin-maxi-black",
      "name": "Corwin Maxi Dress",
      "price": "&pound;129.00",
      "EURprice": "&euro;159.00",
      "date_available": "Coming in April/May",
      "size_range": "Size range: 6-18",
      "colours": {
          "Black": ["#000000", "black-and-white-corwin-maxi-black", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Raspberry": ["#B8004A", "red-and-pink-corwin-maxi", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Blush": ["#FFEAEA", "pale-blush-corwin-maxi-blush", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Dusky Pink": ["#EEC9CF", "pale-blush-corwin-maxi-dusky-pink", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Mocha": ["#A07E7E", "black-and-white-corwin-maxi-mocha", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Cobalt": ["#2E2EB8", "blue-hues-corwin-maxi-cobalt", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Navy": ["#00004A", "blue-hues-corwin-maxi-navy", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Pale Blue": ["#D1E8FF", "blue-hues-corwin-maxi-pale-blue", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": true,
      "in_5_or_15_images": {
        "in5or15images1": "../images/corwin-15-ways-to-wear/01.jpg",
        "in5or15images2": "../images/corwin-15-ways-to-wear/02.jpg",
        "in5or15images3": "../images/corwin-15-ways-to-wear/03.jpg",
        "in5or15images4": "../images/corwin-15-ways-to-wear/04.jpg",
        "in5or15images5": "../images/corwin-15-ways-to-wear/05.jpg",
        "in5or15images6": "../images/corwin-15-ways-to-wear/06.jpg",
        "in5or15images1": "../images/corwin-15-ways-to-wear/07.jpg",
        "in5or15images2": "../images/corwin-15-ways-to-wear/08.jpg",
        "in5or15images3": "../images/corwin-15-ways-to-wear/09.jpg",
        "in5or15images4": "../images/corwin-15-ways-to-wear/10.jpg",
        "in5or15images5": "../images/corwin-15-ways-to-wear/11.jpg",
        "in5or15images6": "../images/corwin-15-ways-to-wear/12.jpg",
        "in5or15images1": "../images/corwin-15-ways-to-wear/13.jpg",
        "in5or15images2": "../images/corwin-15-ways-to-wear/14.jpg",
        "in5or15images3": "../images/corwin-15-ways-to-wear/15.jpg",
      },
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "black-and-white-corwin-maxi-mocha",
      "name": "Corwin Maxi Dress",
      "price": "&pound;129.00",
      "EURprice": "&euro;159.00",
      "date_available": "Coming in October/November",
      "size_range": "Size range: 6-18",
      "colours": {
          "Mocha": ["#A07E7E", "black-and-white-corwin-maxi-mocha", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Black": ["#000000", "black-and-white-corwin-maxi-black", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Cobalt": ["#2E2EB8", "blue-hues-corwin-maxi-cobalt", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Raspberry": ["#B8004A", "red-and-pink-corwin-maxi", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Dusky Pink": ["#EEC9CF", "pale-blush-corwin-maxi-dusky-pink", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Navy": ["#00004A", "blue-hues-corwin-maxi-navy", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Blush": ["#FFEAEA", "pale-blush-corwin-maxi-blush", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Pale Blue": ["#D1E8FF", "blue-hues-corwin-maxi-pale-blue", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": true,
      "in_5_or_15_images": {
        "in5or15images1": "../images/corwin-15-ways-to-wear/01.jpg",
        "in5or15images2": "../images/corwin-15-ways-to-wear/02.jpg",
        "in5or15images3": "../images/corwin-15-ways-to-wear/03.jpg",
        "in5or15images4": "../images/corwin-15-ways-to-wear/04.jpg",
        "in5or15images5": "../images/corwin-15-ways-to-wear/05.jpg",
        "in5or15images6": "../images/corwin-15-ways-to-wear/06.jpg",
        "in5or15images1": "../images/corwin-15-ways-to-wear/07.jpg",
        "in5or15images2": "../images/corwin-15-ways-to-wear/08.jpg",
        "in5or15images3": "../images/corwin-15-ways-to-wear/09.jpg",
        "in5or15images4": "../images/corwin-15-ways-to-wear/10.jpg",
        "in5or15images5": "../images/corwin-15-ways-to-wear/11.jpg",
        "in5or15images6": "../images/corwin-15-ways-to-wear/12.jpg",
        "in5or15images1": "../images/corwin-15-ways-to-wear/13.jpg",
        "in5or15images2": "../images/corwin-15-ways-to-wear/14.jpg",
        "in5or15images3": "../images/corwin-15-ways-to-wear/15.jpg",
      },
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "black-and-white-rolando",
      "name": "Rolando Dress",
      "price": "&pound;169.00",
      "EURprice": "&euro;215.00",
      "date_available": "Coming in August/September",
      "size_range": "Size range: 6-18",
      "colours": {
          "Navy and Black": ["#000000", "black-and-white-rolando", "product-not-available", "debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": true,
      "limited_edition": false
    },
    {
      "productid": "1403502",
      "name": "Bliss Top",
      "price": "&pound;85.00",
      "EURprice": "&euro;110.00",
      "date_available": "In Store Now",
      "size_range": "Size range: 6-18",
      "colours": {
          "Ivory": ["#FFFFFF", "1403502", "product-is-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "1336165",
      "name": "Iridesa Top",
      "price": "&pound;95.00",
      "EURprice": "&euro;120.00",
      "date_available": "In Store Now",
      "size_range": "Size range: 6-18",
      "colours": {
          "Blush": ["#FFEAEA", "1336165", "product-is-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "1422306",
      "name": "Bliss Skirt",
      "price": "&pound;129.00",
      "EURprice": "&euro;159.00",
      "date_available": "In Store Now",
      "size_range": "Size range: 6-18",
      "colours": {
          "Ivory": ["#FFFFFF", "1422306", "product-is-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "blue-hues-lori-ella-maxi-mid-blue",
      "name": "Lori Ella Maxi Dress",
      "price": "&pound;189.00",
      "EURprice": "&euro;245.00",
      "date_available": "Coming in July",
      "size_range": "Size range: 6-18",
      "colours": {
          "Mid-Blue": ["#8AB8E6", "blue-hues-lori-ella-maxi-mid-blue", "product-not-available", "not-debenhams-exclusive", "Also available in Petite"],
          "Navy": ["#00004A", "blue-hues-lori-ella-maxi-navy", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "Also available in Petite",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "blue-hues-lori-ella-maxi-navy",
      "name": "Lori Ella Maxi Dress",
      "price": "&pound;189.00",
      "EURprice": "&euro;245.00",
      "date_available": "Coming in July",
      "size_range": "Size range: 6-18",
      "colours": {
          "Navy": ["#00004A", "blue-hues-lori-ella-maxi-navy", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Mid-Blue": ["#8AB8E6", "blue-hues-lori-ella-maxi-mid-blue", "product-not-available", "not-debenhams-exclusive", "Also available in Petite"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "blue-hues-giuglia-navy",
      "name": "Giuglia Dress",
      "price": "&pound;95.00",
      "EURprice": "&euro;120.00",
      "date_available": "Coming in July",
      "size_range": "Size range: 6-18",
      "colours": {
          "Navy": ["#00004A", "blue-hues-giuglia-navy", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Pink": ["#E62E8A", "red-and-pink-giuglia", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Cornflower": ["#75A3FF", "blue-hues-giuglia-cornflower", "product-not-available", "not-debenhams-exclusive", "Petite"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "blue-hues-brya-rose",
      "name": "Brya Rose Dress",
      "price": "&pound;179.00",
      "EURprice": "&euro;225.00",
      "date_available": "Coming in July",
      "size_range": "Size range: 6-18",
      "colours": {
          "Mid-Blue": ["#465E76", "blue-hues-brya-rose", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "blue-hues-dena",
      "name": "Dena Dress",
      "price": "&pound;195.00",
      "EURprice": "&euro;245.00",
      "date_available": "Coming in August/September",
      "size_range": "Size range: 6-18",
      "colours": {
          "Navy": ["#00004A", "blue-hues-dena", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "1314520",
      "name": "Lori Lee Lace Short Dress",
      "price": "&pound;129.00",
      "EURprice": "&euro;159.00",
      "date_available": "In Store Now",
      "size_range": "Size range: 6-18",
      "colours": {
          "Navy": ["#00004A", "1314520", "product-is-available", "not-debenhams-exclusive", "no-petite-version"]
       },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "blue-hues-lori-may-maxi",
      "name": "Lori May Maxi Dress",
      "price": "&pound;189.00",
      "EURprice": "&euro;245.00",
      "date_available": "Coming in September",
      "size_range": "Size range: 6-18",
      "colours": {
          "Navy": ["#00004A", "blue-hues-lori-may-maxi", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Grape": ["#440044", "berry-tones-lori-may-maxi", "product-not-available", "not-debenhams-exclusive", "Also available in Petite"],
          "Kingfisher": ["#166161", "greens-lori-may-maxi", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "blue-hues-lori-may-short",
      "name": "Lori May Short Dress",
      "price": "&pound;129.00",
      "EURprice": "&euro;159.00",
      "date_available": "Coming in September",
      "size_range": "Size range: 6-18",
      "colours": {
          "Navy": ["#00004A", "blue-hues-lori-may-short", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Grape": ["#440044", "berry-tones-lori-may-short", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Kingfisher": ["#166161", "greens-lori-may-short", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "blue-hues-minnie-maxi",
      "name": "Minnie Maxi Dress",
      "price": "&pound;225.00",
      "EURprice": "&euro;285.00",
      "date_available": "Coming in July",
      "size_range": "Size range: 6-18",
      "colours": {
          "Navy": ["#00004A", "blue-hues-minnie-maxi", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Blush": ["#FFEAEA", "pale-blush-minnie-maxi", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": true
    },
    {
      "productid": "blue-hues-corwin-maxi-cobalt",
      "name": "Corwin Maxi Dress",
      "price": "&pound;129.00",
      "EURprice": "&euro;159.00",
      "date_available": "Coming in April/May",
      "size_range": "Size range: 6-18",
      "colours": {
          "Cobalt": ["#2E2EB8", "blue-hues-corwin-maxi-cobalt", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Black": ["#000000", "black-and-white-corwin-maxi-black", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Raspberry": ["#B8004A", "red-and-pink-corwin-maxi", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Blush": ["#FFEAEA", "pale-blush-corwin-maxi-blush", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Dusky Pink": ["#EEC9CF", "pale-blush-corwin-maxi-dusky-pink", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Mocha": ["#A07E7E", "black-and-white-corwin-maxi-mocha", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Navy": ["#00004A", "blue-hues-corwin-maxi-navy", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Pale Blue": ["#D1E8FF", "blue-hues-corwin-maxi-pale-blue", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": true,
      "in_5_or_15_images": {
        "in5or15images1": "../images/corwin-15-ways-to-wear/01.jpg",
        "in5or15images2": "../images/corwin-15-ways-to-wear/02.jpg",
        "in5or15images3": "../images/corwin-15-ways-to-wear/03.jpg",
        "in5or15images4": "../images/corwin-15-ways-to-wear/04.jpg",
        "in5or15images5": "../images/corwin-15-ways-to-wear/05.jpg",
        "in5or15images6": "../images/corwin-15-ways-to-wear/06.jpg",
        "in5or15images1": "../images/corwin-15-ways-to-wear/07.jpg",
        "in5or15images2": "../images/corwin-15-ways-to-wear/08.jpg",
        "in5or15images3": "../images/corwin-15-ways-to-wear/09.jpg",
        "in5or15images4": "../images/corwin-15-ways-to-wear/10.jpg",
        "in5or15images5": "../images/corwin-15-ways-to-wear/11.jpg",
        "in5or15images6": "../images/corwin-15-ways-to-wear/12.jpg",
        "in5or15images1": "../images/corwin-15-ways-to-wear/13.jpg",
        "in5or15images2": "../images/corwin-15-ways-to-wear/14.jpg",
        "in5or15images3": "../images/corwin-15-ways-to-wear/15.jpg",
      },
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "blue-hues-corwin-maxi-navy",
      "name": "Corwin Maxi Dress",
      "price": "&pound;129.00",
      "EURprice": "&euro;159.00",
      "date_available": "Coming in July",
      "size_range": "Size range: 6-18",
      "colours": {
          "Navy": ["#00004A", "blue-hues-corwin-maxi-navy", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Cobalt": ["#2E2EB8", "blue-hues-corwin-maxi-cobalt", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Black": ["#000000", "black-and-white-corwin-maxi-black", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Raspberry": ["#B8004A", "red-and-pink-corwin-maxi", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Blush": ["#FFEAEA", "pale-blush-corwin-maxi-blush", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Dusky Pink": ["#EEC9CF", "pale-blush-corwin-maxi-dusky-pink", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Mocha": ["#A07E7E", "black-and-white-corwin-maxi-mocha", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Pale Blue": ["#D1E8FF", "blue-hues-corwin-maxi-pale-blue", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": true,
      "in_5_or_15_images": {
        "in5or15images1": "../images/corwin-15-ways-to-wear/01.jpg",
        "in5or15images2": "../images/corwin-15-ways-to-wear/02.jpg",
        "in5or15images3": "../images/corwin-15-ways-to-wear/03.jpg",
        "in5or15images4": "../images/corwin-15-ways-to-wear/04.jpg",
        "in5or15images5": "../images/corwin-15-ways-to-wear/05.jpg",
        "in5or15images6": "../images/corwin-15-ways-to-wear/06.jpg",
        "in5or15images1": "../images/corwin-15-ways-to-wear/07.jpg",
        "in5or15images2": "../images/corwin-15-ways-to-wear/08.jpg",
        "in5or15images3": "../images/corwin-15-ways-to-wear/09.jpg",
        "in5or15images4": "../images/corwin-15-ways-to-wear/10.jpg",
        "in5or15images5": "../images/corwin-15-ways-to-wear/11.jpg",
        "in5or15images6": "../images/corwin-15-ways-to-wear/12.jpg",
        "in5or15images1": "../images/corwin-15-ways-to-wear/13.jpg",
        "in5or15images2": "../images/corwin-15-ways-to-wear/14.jpg",
        "in5or15images3": "../images/corwin-15-ways-to-wear/15.jpg",
      },
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "blue-hues-corwin-maxi-pale-blue",
      "name": "Corwin Maxi Dress",
      "price": "&pound;129.00",
      "EURprice": "&euro;159.00",
      "date_available": "Coming in April/May",
      "size_range": "Size range: 6-18",
      "colours": {
          "Pale Blue": ["#D1E8FF", "blue-hues-corwin-maxi-pale-blue", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Navy": ["#00004A", "blue-hues-corwin-maxi-navy", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Cobalt": ["#2E2EB8", "blue-hues-corwin-maxi-cobalt", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Black": ["#000000", "black-and-white-corwin-maxi-black", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Raspberry": ["#B8004A", "red-and-pink-corwin-maxi", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Blush": ["#FFEAEA", "pale-blush-corwin-maxi-blush", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Dusky Pink": ["#EEC9CF", "pale-blush-corwin-maxi-dusky-pink", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Mocha": ["#A07E7E", "black-and-white-corwin-maxi-mocha", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": true,
      "in_5_or_15_images": {
        "in5or15images1": "../images/corwin-15-ways-to-wear/01.jpg",
        "in5or15images2": "../images/corwin-15-ways-to-wear/02.jpg",
        "in5or15images3": "../images/corwin-15-ways-to-wear/03.jpg",
        "in5or15images4": "../images/corwin-15-ways-to-wear/04.jpg",
        "in5or15images5": "../images/corwin-15-ways-to-wear/05.jpg",
        "in5or15images6": "../images/corwin-15-ways-to-wear/06.jpg",
        "in5or15images1": "../images/corwin-15-ways-to-wear/07.jpg",
        "in5or15images2": "../images/corwin-15-ways-to-wear/08.jpg",
        "in5or15images3": "../images/corwin-15-ways-to-wear/09.jpg",
        "in5or15images4": "../images/corwin-15-ways-to-wear/10.jpg",
        "in5or15images5": "../images/corwin-15-ways-to-wear/11.jpg",
        "in5or15images6": "../images/corwin-15-ways-to-wear/12.jpg",
        "in5or15images1": "../images/corwin-15-ways-to-wear/13.jpg",
        "in5or15images2": "../images/corwin-15-ways-to-wear/14.jpg",
        "in5or15images3": "../images/corwin-15-ways-to-wear/15.jpg",
      },
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "1411920",
      "name": "Lori Lee Maxi Dress",
      "price": "&pound;185.00",
      "EURprice": "&euro;230.00",
      "date_available": "Coming in July",
      "size_range": "Size range: 6-18",
      "colours": {
          "Navy": ["#00004A", "1411920", "product-is-available", "not-debenhams-exclusive", "no-petite-version"],
          "Dusky Lilac": ["#A389BE", "warm-pastels-lori-lee-maxi-dusky-lilac", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Dusky Pink": ["#EEC9CF", "pale-blush-lori-lee-maxi", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Slate": ["#344E69", "blue-hues-lori-lee-maxi-slate", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Forest Green": ["#003F00", "greens-lori-lee-maxi", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "blue-hues-lori-lee-short-navy",
      "name": "Lori Lee Lace Short Dress",
      "price": "&pound;129.00",
      "EURprice": "&euro;159.00",
      "date_available": "Coming in July",
      "size_range": "Size range: 6-18",
      "colours": {
          "Navy": ["#00004A", "blue-hues-lori-lee-short-navy", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Dusky Lilac": ["#A389BE", "warm-pastels-lori-lee-short", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Dusky Pink": ["#EEC9CF", "pale-blush-lori-lee-short", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Slate": ["#344E69", "blue-hues-lori-lee-short-slate", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Cobalt": ["#2E2EB8", "blue-hues-lori-lee-short-cobalt", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Forest Green": ["#003F00", "greens-lori-lee-short", "product-not-available", "not-debenhams-exclusive", "Also available in Maxi length"]
      },
      "in_5_or_15": false,
      "also_available_in": "Also available in Maxi length",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "blue-hues-lori-lee-maxi-slate",
      "name": "Lori Lee Maxi Dress",
      "price": "&pound;185.00",
      "EURprice": "&euro;230.00",
      "date_available": "Coming in July",
      "size_range": "Size range: 6-18",
      "colours": {
          "Slate": ["#344E69", "blue-hues-lori-lee-maxi-slate", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Navy": ["#00004A", "1411920", "product-is-available", "not-debenhams-exclusive", "no-petite-version"],
          "Dusky Lilac": ["#A389BE", "warm-pastels-lori-lee-maxi-dusky-lilac", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Dusky Pink": ["#EEC9CF", "pale-blush-lori-lee-maxi", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Forest Green": ["#003F00", "greens-lori-lee-maxi", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "blue-hues-lori-lee-short-slate",
      "name": "Lori Lee Short Dress",
      "price": "&pound;129.00",
      "EURprice": "&euro;159.00",
      "date_available": "Coming in July",
      "size_range": "Size range: 6-18",
      "colours": {
          "Slate": ["#344E69", "blue-hues-lori-lee-short-slate", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Navy": ["#00004A", "blue-hues-lori-lee-short-navy", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Dusky Lilac": ["#A389BE", "warm-pastels-lori-lee-short", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Dusky Pink": ["#EEC9CF", "pale-blush-lori-lee-short", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Cobalt": ["#2E2EB8", "blue-hues-lori-lee-short-cobalt", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Forest Green": ["#003F00", "greens-lori-lee-short", "product-not-available", "not-debenhams-exclusive", "Also available in Maxi length"]
      },
      "in_5_or_15": false,
      "also_available_in": "Also available in Maxi length",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "blue-hues-giuglia-cornflower",
      "name": "Giuglia Dress",
      "price": "&pound;95.00",
      "EURprice": "&euro;120.00",
      "date_available": "Coming in July",
      "size_range": "Size range: 6-18",
      "colours": {
          "Cornflower": ["#75A3FF", "blue-hues-giuglia-cornflower", "product-not-available", "not-debenhams-exclusive", "Petite"],
          "Navy": ["#00004A", "blue-hues-giuglia-navy", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Pink": ["#E62E8A", "red-and-pink-giuglia", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "Petite",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "blue-hues-lori-lee-short-cobalt",
      "name": "Lori Lee Short Dress",
      "price": "&pound;129.00",
      "EURprice": "&euro;159.00",
      "date_available": "Coming in July",
      "size_range": "Size range: 6-18",
      "colours": {
          "Cobalt": ["#2E2EB8", "blue-hues-lori-lee-short-cobalt", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Slate": ["#344E69", "blue-hues-lori-lee-short-slate", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Navy": ["#00004A", "blue-hues-lori-lee-short-navy", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Dusky Lilac": ["#A389BE", "warm-pastels-lori-lee-short", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Dusky Pink": ["#EEC9CF", "pale-blush-lori-lee-short", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Forest Green": ["#003F00", "greens-lori-lee-short", "product-not-available", "not-debenhams-exclusive", "Also available in Maxi length"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "blue-hues-issey-mae",
      "name": "Issey Mae Dress",
      "price": "&pound;189.00",
      "EURprice": "&euro;245.00",
      "date_available": "Coming in August/September",
      "size_range": "Size range: 6-18",
      "colours": {
          "Cobalt": ["#2E2EB8", "blue-hues-issey-mae", "product-not-available", "debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": true,
      "limited_edition": false
    },
    {
      "productid": "greens-fernanda",
      "name": "Fernanda Dress",
      "price": "&pound;179.00",
      "EURprice": "&euro;225.00",
      "date_available": "Coming in August/September",
      "size_range": "Size range: 6-18",
      "colours": {
          "Thyme": ["#B5CECE", "greens-fernanda", "product-not-available", "debenhams-exclusive", "no-petite-version"],
          "Smokey Grey": ["#C9D2E6", "warm-pastels-fernanda", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": true,
      "limited_edition": false
    },
    {
      "productid": "greens-lori-lee-maxi",
      "name": "Lori Lee Maxi Dress",
      "price": "&pound;185.00",
      "EURprice": "&euro;230.00",
      "date_available": "Coming in August/September",
      "size_range": "Size range: 6-18",
      "colours": {
          "Forest Green": ["#003F00", "greens-lori-lee-maxi", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Slate": ["#344E69", "blue-hues-lori-lee-maxi-slate", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Navy": ["#00004A", "1411920", "product-is-available", "not-debenhams-exclusive", "no-petite-version"],
          "Dusky Lilac": ["#A389BE", "warm-pastels-lori-lee-maxi-dusky-lilac", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Dusky Pink": ["#EEC9CF", "pale-blush-lori-lee-maxi", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "greens-lori-may-maxi",
      "name": "Lori May Maxi Dress",
      "price": "&pound;189.00",
      "EURprice": "&euro;245.00",
      "date_available": "Coming in October/November",
      "size_range": "Size range: 6-18",
      "colours": {
          "Kingfisher": ["#166161", "greens-lori-may-maxi", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Navy": ["#00004A", "blue-hues-lori-may-maxi", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Grape": ["#440044", "berry-tones-lori-may-maxi", "product-not-available", "not-debenhams-exclusive", "Also available in Petite"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "greens-lori-may-short",
      "name": "Lori May Short Dress",
      "price": "&pound;129.00",
      "EURprice": "&euro;159.00",
      "date_available": "Coming in October/November",
      "size_range": "Size range: 6-18",
      "colours": {
          "Kingfisher": ["#166161", "greens-lori-may-short", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Navy": ["#00004A", "blue-hues-lori-may-short", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Grape": ["#440044", "berry-tones-lori-may-short", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "greens-lori-lee-short",
      "name": "Lori Lee Short Dress",
      "price": "&pound;129.00",
      "EURprice": "&euro;159.00",
      "date_available": "Coming in August/September",
      "size_range": "Size range: 6-18",
      "colours": {
          "Forest Green": ["#003F00", "greens-lori-lee-short", "product-not-available", "not-debenhams-exclusive", "Also available in Maxi length"],
          "Cobalt": ["#2E2EB8", "blue-hues-lori-lee-short-cobalt", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Slate": ["#344E69", "blue-hues-lori-lee-short-slate", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Navy": ["#00004A", "blue-hues-lori-lee-short-navy", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Dusky Lilac": ["#A389BE", "warm-pastels-lori-lee-short", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Dusky Pink": ["#EEC9CF", "pale-blush-lori-lee-short", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "Also available in Maxi length",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "accessories-billie-box-clutch",
      "name": "Billie Box Clutch",
      "price": "&pound;65.00",
      "EURprice": "&euro;80.00",
      "date_available": "Coming in July",
      "size_range": "",
      "colours": {
          "Silver": ["#C0C0C0", "accessories-billie-box-clutch", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "accessories-carrie-tie-cape",
      "name": "Carrie Tie Cape",
      "price": "&pound;85.00",
      "EURprice": "&euro;110.00",
      "date_available": "Coming in August/September",
      "size_range": "",
      "colours": {
          "Blush": ["#FFEAEA", "accessories-carrie-tie-cape", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "accessories-cleo-necklace-gold",
      "name": "Cleo Necklace",
      "price": "&pound;28.00",
      "EURprice": "&euro;36.00",
      "date_available": "Coming in July",
      "size_range": "",
      "colours": {
          "Gold": ["#D4AF37", "accessories-cleo-necklace-gold", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Silver": ["#C0C0C0", "accessories-cleo-necklace-silver", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "accessories-cleo-necklace-silver",
      "name": "Cleo Necklace",
      "price": "&pound;28.00",
      "EURprice": "&euro;36.00",
      "date_available": "Coming in July",
      "size_range": "",
      "colours": {
          "Silver": ["#C0C0C0", "accessories-cleo-necklace-silver", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Gold": ["#D4AF37", "accessories-cleo-necklace-gold", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "accessories-cosmo-off-the-shoulder-snood-merlot",
      "name": "Cosmo Off The Shoulder Snood",
      "price": "&pound;50.00",
      "EURprice": "&euro;60.00",
      "date_available": "Coming in August/September",
      "size_range": "",
      "colours": {
          "Merlot": ["#4E1226", "accessories-cosmo-off-the-shoulder-snood-merlot", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "accessories-deco-necklace",
      "name": "Deco Necklace",
      "price": "&pound;45.00",
      "EURprice": "&euro;55.00",
      "date_available": "Coming in August/September",
      "size_range": "",
      "colours": {
          "Silver": ["#C0C0C0", "accessories-deco-necklace", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "accessories-ellie-envelope-clutch",
      "name": "Ellie Envelope Clutch",
      "price": "&pound;45.00",
      "EURprice": "&euro;55.00",
      "date_available": "Coming in July",
      "size_range": "",
      "colours": {
          "Blush": ["#FFEAEA", "accessories-ellie-envelope-clutch", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "accessories-faith-sequin-minae-clutch",
      "name": "Faith Sequin Minae Clutch",
      "price": "&pound;45.00",
      "EURprice": "&euro;55.00",
      "date_available": "Coming in July",
      "size_range": "",
      "colours": {
          "Silver": ["#C0C0C0", "accessories-faith-sequin-minae-clutch", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "accessories-felicia-clutch",
      "name": "Felicia Clutch",
      "price": "&pound;55.00",
      "EURprice": "&euro;70.00",
      "date_available": "Coming in July",
      "size_range": "",
      "colours": {
          "Silver": ["#C0C0C0", "accessories-felicia-clutch", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "accessories-felicia-pearl-clutch",
      "name": "Felicia Pearl Clutch",
      "price": "&pound;55.00",
      "EURprice": "&euro;70.00",
      "date_available": "Coming in July",
      "size_range": "",
      "colours": {
          "Pearl": ["#EAE0C8", "accessories-felicia-pearl-clutch", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "accessories-felicity-feather-bolero",
      "name": "Felicia Feather Bolero",
      "price": "&pound;60.00",
      "EURprice": "&euro;78.00",
      "date_available": "Coming in July",
      "size_range": "",
      "colours": {
          "Ivory": ["#FFFFFF", "accessories-felicity-feather-bolero", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "accessories-flori-flower-clip",
      "name": "Flori Flower Clip",
      "price": "&pound;20.00",
      "EURprice": "&euro;26.00",
      "date_available": "Coming in May",
      "size_range": "",
      "colours": {
          "Silver": ["#C0C0C0", "accessories-flori-flower-clip", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "accessories-fluerette-flower-headband",
      "name": "Fluerette Flower Headband",
      "price": "&pound;20.00",
      "EURprice": "&euro;26.00",
      "date_available": "Coming in May",
      "size_range": "",
      "colours": {
          "Silver": ["#C0C0C0", "accessories-fluerette-flower-headband", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "accessories-iridescent-flower-necklace",
      "name": "Iridescent Flower Necklace",
      "price": "&pound;35.00",
      "EURprice": "&euro;45.00",
      "date_available": "Coming in July",
      "size_range": "",
      "colours": {
          "Gunmetal": ["#6B7071", "accessories-iridescent-flower-necklace", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "accessories-luella-long-line-scarf-black",
      "name": "Luella Long Line Scarf",
      "price": "&pound;60.00",
      "EURprice": "&euro;78.00",
      "date_available": "Coming in August/September",
      "size_range": "",
      "colours": {
          "Black": ["#000000", "accessories-luella-long-line-scarf-black", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Tan": ["#D2B48C", "accessories-luella-long-line-scarf-tan", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "accessories-luella-long-line-scarf-tan",
      "name": "Luella Long Line Scarf",
      "price": "&pound;60.00",
      "EURprice": "&euro;78.00",
      "date_available": "Coming in October/November",
      "size_range": "",
      "colours": {
          "Tan": ["#D2B48C", "accessories-luella-long-line-scarf-tan", "product-not-available", "not-debenhams-exclusive", "no-petite-version"],
          "Black": ["#000000", "accessories-luella-long-line-scarf-black", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "accessories-milly-metallic-wrap",
      "name": "Milly Metallic Wrap",
      "price": "&pound;38.00",
      "EURprice": "&euro;50.00",
      "date_available": "Coming in May",
      "size_range": "",
      "colours": {
          "Blush": ["#FFEAEA", "accessories-milly-metallic-wrap", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "accessories-nelly-stone-bracelet",
      "name": "Nelly Stone Bracelet",
      "price": "&pound;15.00",
      "EURprice": "&euro;19.00",
      "date_available": "Coming in August/September",
      "size_range": "",
      "colours": {
          "Neutral": ["#FFF3F3", "accessories-nelly-stone-bracelet", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "accessories-ostrich-feather-bag",
      "name": "Ostrich Feather Bag",
      "price": "&pound;80.00",
      "EURprice": "&euro;100.00",
      "date_available": "Coming in July",
      "size_range": "",
      "colours": {
          "Blush": ["#FFEAEA", "accessories-ostrich-feather-bag", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": true
    },
    {
      "productid": "accessories-paloma-pearl-clutch",
      "name": "Paloma Pearl Clutch",
      "price": "&pound;60.00",
      "EURprice": "&euro;78.00",
      "date_available": "Coming in July",
      "size_range": "",
      "colours": {
          "Silver": ["#C0C0C0", "accessories-paloma-pearl-clutch", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "accessories-pearl-headband",
      "name": "Pearl Headband",
      "price": "&pound;25.00",
      "EURprice": "&euro;30.00",
      "date_available": "Coming in May",
      "size_range": "",
      "colours": {
          "Silver": ["#C0C0C0", "accessories-pearl-headband", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "accessories-simple-pearl-single-bangle",
      "name": "Simple Pearl Single Bangle",
      "price": "&pound;18.00",
      "EURprice": "&euro;23.00",
      "date_available": "Coming in August/September",
      "size_range": "",
      "colours": {
          "Silver": ["#C0C0C0", "accessories-simple-pearl-single-bangle", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    },
    {
      "productid": "accessories-star-clip",
      "name": "Star Clip",
      "price": "&pound;12.00",
      "EURprice": "&euro;15.00",
      "date_available": "Coming in May",
      "size_range": "",
      "colours": {
          "Silver": ["#C0C0C0", "accessories-star-clip", "product-not-available", "not-debenhams-exclusive", "no-petite-version"]
      },
      "in_5_or_15": false,
      "also_available_in": "",
      "debenhams_exclusive": false,
      "limited_edition": false
    }

  ]
}