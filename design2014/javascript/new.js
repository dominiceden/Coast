$(document).ready(function(){
	// Alter nav links temporarily. Need to add a delay to overwrite the Qubit nav. Remove when Fresca fix the vanity URL fault.
	setTimeout(function() {
		$('a[href$="/features"]').attr("href", "/fcp/content/features/content");
		$('a[href$="/bridesmaids-stylebook"]').attr("href", "/fcp/content/bridesmaids_stylebook/content");
		$('a[href$="/bridesmaids-stylebook#trend-4"]').attr("href", "/fcp/content/BRIDESMAIDS_STYLEBOOK/content#trend-4");
		$('a[href$="/as-seen-in-press"]').attr("href", "/asseeninthepress/dept/fcp-category/list");
		$('a[href$="/xmas-lookbook"]').attr("href", "/fcp/content/xmas-lookbook/content");
		$('a[href$="/aw14-video"]').attr("href", "/fcp/content/AW14%20Video/content");
		$('a[href$="/coatfeature"]').attr("href", "/fcp/content/coatfeature/content");
		$('a[href$="/lbd-history"]').attr("href", "/fcp/content/lbd-history/content");
		$('a[href$="/wedding"]').attr("href", "/fcp/content/wedding/content");
		$('a[href$="/glossary"]').attr("href", "/fcp/content/page-glossary/content");
		$('a[href$="/laidback-luxe"]').attr("href", "/fcp/content/laidback/content");
		$('a[href$="/aidanmattox"]').attr("href", "/fcp/content/aidan-mattox/content");
		$('a[href$="/christmas_feature"]').attr("href", "/fcp/content/christmas_feature/content");
		$('a[href$="/lacebomber"]').attr("href", "/fcp/content/lacebomber/content");
		$('a[href$="/modern_romance"]').attr("href", "/fcp/content/modern_romance/content");
		$('a[href$="/aw14-lookbook"]').attr("href", "/fcp/content/aw14-lookbook/content");
		$('a[href$="/modernist-mono"]').attr("href", "/fcp/content/momono/content");
		$('a[href$="/bridal-lookbook"]').attr("href", "/fcp/content/bridallookbook/content");
		$('a[href$="/jeya-dress"]').attr("href", "/fcp/content/jeya-dress/content");
		$('a[href$="/a-modern-summer"]').attr("href", "/fcp/content/a%20modern%20summer/content");
		$('a[href$="/jump-in"]').attr("href", "/fcp/content/jump-in/content");
		$('a[href$="/events"]').attr("href", "/fcp/content/events/content");
		$('a[href$="/all-events/dept/fcp-category/list"]').attr("href", "/fcp/content/events/content"); // correct events page
		$('a[href$="/knitwear"]').attr("href", "/fcp/content/knitwear/content");
		$('a[href$="/dresscode"]').attr("href", "/fcp/content/dresscode/content");

		// On glossary page, replace URLs using a loop
		var glossaryItems = $(".full a");
		for (var i = 0; i < glossaryItems.length; i++) {
			var url = glossaryItems[i].href;
			var newUrl = url.replace("glossary-items", "fcp/content/glossary-items-page/content");
			glossaryItems[i].href = newUrl;
		}

	}, 1000);

	// Lookbook functionality
		var $lookbookItems = $('.look-item .large-view'),
			$productItem = $('.look-items li'),
			lookItemUrl = '/pws/AJProductDetails.ice?layout=quickbuy.pop.layout&ProductID=',
			lookPid;

		$productItem.each(function(index, el) {
			lookPid = $(this).data('product-id');

			if ( lookPid ) {
				$(this).find('.quick_buy_link').attr("href", lookItemUrl + lookPid);
			} else {
				$(this).remove();
			};
		});

		$('.look-item')
			.on('click', 'figure', function(event) {
				event.preventDefault();

				var $target = $(this).next('.large-view'); // when a figure is clicked, select the next element in the DOM, which is .large-view

				if ( !$target.hasClass('opened') ) { // if the target variable (see above) isn't opened...
					$lookbookItems.removeClass('opened').fadeOut();
					$target.addClass('opened').fadeIn();
					$target.find('.twelve.columns').css('padding', '0'); // set padding to 0 to avoid alignment issues

					$('html, body').animate({
            scrollTop: $(".opened").offset().top -75
          }, 1000);

				}

			})
			.on('click', '.js-close', function(event) {
				event.preventDefault();

				$lookbookItems.removeClass('opened').fadeOut();
			});

		// $('.lookbook .look-item').on('click', function(event) {
		//     event.preventDefault();

		//     $(this).find('.large-view').fadeIn();
		//     $(this).find('.js-close').on('click', function(event) {
		//         event.preventDefault();
		//         $('.large-view').fadeOut();
		//     });
		// });

	// Events slider
	$('.events-slider').bxSlider({
		pagerCustom: '.bx-pager',
		nextSelector: '#slider-next',
		prevSelector: '#slider-prev',
		nextText: '&#xe604;',
		prevText: '&#xe603;',
		onSliderLoad: function(){
			$(".slider-inner").css("opacity", "1");
			$(".slider-inner .details").css({"opacity": "1", "margin-bottom": "-26px"});
		}
	});

	var lookbook_slider1 = $('#slider1').bxSlider({
		pagerCustom: $('#pager1'),
		controls: false,
		mode: 'fade'
	});

	var lookbook_slider2 = $('#slider2').bxSlider({
		pagerCustom: $('#pager2'),
		controls: false,
		mode: 'fade'
	});

	var lookbook_slider3 = $('#slider3').bxSlider({
		pagerCustom: $('#pager3'),
		controls: false,
		mode: 'fade'
	});

	// Pager
	var lookbook_slider_thumbs1 = $('#pager1').bxSlider({
		mode: 'vertical',
		minSlides: 4,
		maxSlides: 4,
		pager: false,
		preloadImages: 'all'
	});

	var lookbook_slider_thumbs2 = $('#pager2').bxSlider({
		mode: 'vertical',
		minSlides: 4,
		maxSlides: 4,
		pager: false,
		preloadImages: 'all'
	});

	var lookbook_slider_thumbs3 = $('#pager3').bxSlider({
		mode: 'vertical',
		minSlides: 4,
		maxSlides: 4,
		pager: false,
		preloadImages: 'all'
	});

	// On category pages, make sure that sizes six and eight are at the top of the list of sizes.

	var eight = $("li#filter_pws_available_sizes_filter_lang_en_8"); // get the size eight checkbox and store it in a variable
	$("li#filter_pws_available_sizes_filter_lang_en_8").remove(); // remove the size eight checkbox from the DOM
	$("ol#filter_pws_available_sizes_filter_lang_en").prepend(eight); // add it at the top of the list.

	var six = $("li#filter_pws_available_sizes_filter_lang_en_6");
	$("li#filter_pws_available_sizes_filter_lang_en_6").remove();
	$("ol#filter_pws_available_sizes_filter_lang_en").prepend(six);

	// Hide duplicate Standard Delivery option if more than 2 delivery options (this is the case for DC stock).
	// Be careful though as mix of DC and SFS stock will present 2 delivery options, so condition has to be >= 2.

	var deliveryOptions = $('span.delivery-name');

	if (deliveryOptions.length > 2) {
		//$('.delivery-options-form .other:last-child').css('display','none');
	};

	// tweak product details page layout

	$('a#size_guide').appendTo($('#size-stock > label')); // move size guide link to the right of SELECT SIZE header
	$('.availability-key').appendTo($('#size-stock')); // move stock key to underneath sizes
	$('a.email').removeClass('pull-right'); // remove the float right of Email A Friend

});