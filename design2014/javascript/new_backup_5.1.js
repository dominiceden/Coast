// Popups
(function(a) {
    a.fn.contentPopup = function(b) {
        var e = a.extend({}, a.fn.contentPopup.defaults, b),
            c = a(this),
            d = c.attr("href");
        c.click(function() {
            var f = a('<div id="content-popup" class="content-main"></div>');
            a("body").append(f);
            f.fadeIn();
            if (e.contentUrl !== "") {
                d = e.contentUrl
            }
            f.load(d + " " + e.elementToAjax, function() {
                var g = parseInt((a(window).height() - e.popupHeight) / 2, 10),
                    h = parseInt((a(window).width() - f.width()) / 2, 10);
                f.css({
                    top: g + "px",
                    left: h + "px",
                    height: e.popupHeight
                });

                if (e.closeButton) {
                    a('<a href="#" class="close-popup icon-close"></a>').appendTo(f);
                    a("body").block({
                        top: 0
                    });
                    f.find(".close-popup").click(function() {
                        a("body").unblock();
                        f.remove()
                    });
                    a("body").find(".fresca_block").click(function() {
                        f.remove()
                    })
                }

                // Init tabs
                awCoast.Global.defaultTabs();
            });
            return false
        })
    };
    a.fn.contentPopup.defaults = {
        closeButton: true,
        popupHeight: "",
        popupWidth: "",
        elementToAjax: "",
        contentUrl: ""
    }
}(jQuery));


/******* START OF infiniteProductList.js*******/ 
(function (a) {
    a.fn.infiniteProductList = function (q) {
        var g = {
                $listingContainer: a(this).parent("#listings_container"),
                $productsList: a(this),
                url: "",
                page: 1,
                offset: (a(".product:last", this).length > 0) ? a(".product:last", this).offset().top : ""
            },
            e = a("#page_controls"),
            n = a("a", "#page_controls"),
            l = a("#loading_more_products"),
            o = a(".pagination"),
            m, k, b = {},
            c, j;
        b = a.extend({}, g, q);

        function p(s) {
            var i, r = a(".product:first", "#main_products_list_page");
            k = a(".product noscript", s);
            for (j = 0; j < k.length; j++) {
                i = a(k[j]).parents(".product");
                c = a(k[j]).html().replace("&lt;", "<").replace("&gt;", ">");
                if (c === "") {
                    c = r.find(".product_image").clone();
                    c.attr("alt", i.find(".product_title").text());
                    c.attr("src", c.attr("src").replace(new RegExp(r.attr("id").split("_")[1], "g"), i.attr("id").split("_")[1]))
                }
                a(k[j]).after(c).remove();
                i.find(".image_not_loaded").removeClass("image_not_loaded")
            }
        }

        function d() {
            b.url = basePath;
            // console.log(filterParam);
            if (filterParam !== undefined) {
                b.url = b.url + "filters=" + filterParam + "&"
            }
            b.url = a.fn.ajaxFilter.generateAjaxUrl(b.url + "page=" + b.page);
            a.ajax({
                url: b.url,
                async: true,
                error: function (s, i, r) {
                    b.$productsList.addClass("loaded").removeClass("loading")
                },
                success: function (r) {
                    b.page++;
                    var i = a(r);
                    if (a(".product", i).length > 0) {
                        b.$productsList.append(i.find(".product")).removeClass("loading");
                        p(b.$productsList);
                        b.offset = a(".product:last", b.$productsList).offset().top
                    } else {
                        b.$productsList.addClass("loaded").removeClass("loading")
                    } if (postScroll !== undefined) {
                        postScroll()
                    }
                },
                complete: function () {
                    l.toggleClass("hide");
                    n.toggle();
                    // addHoverImages();
                    awCoast.Category.init();
                }
            })
        }

        function f() {
            m = b.$productsList.data("view");
            b.$listingContainer.height(Math.ceil(productsInCategory / m) * a(".product", b.$productsList).outerHeight(true));
        }

        function h() {
            a(window).unbind("scroll").scroll(function () {
                if (a(window).scrollTop() >= 500) {
                    e.fadeIn(300)
                } else {
                    e.fadeOut(300)
                }
                setTimeout(function () {
                    if (!b.$productsList.hasClass("loading") && !b.$productsList.hasClass("loaded") && (a(window).scrollTop()) >= (b.offset * 0.5) && o.length > 0) {
                        b.$productsList.addClass("loading");
                        l.toggleClass("hide");
                        n.toggle();
                        b.$productsList.append(d())
                    }
                    p(b.$productsList)
                }, 500)
            });
            n.click(function (i) {
                i.preventDefault();
                n.addClass("scrolling");
                a.scrollTo(0, 1000, {
                    onAfter: function () {
                        n.removeClass("scrolling")
                    }
                })
            })
        }
        h()
    }
}(jQuery));
/******* END OF infiniteProductList.js*******/ 

/******* START OF ajaxFilter.js*******/ 
(function (a) {
    a.fn.ajaxFilter = function (d) {
        var g, r = basePath,
            m = a(this),
            p, q = "",
            f = true,
            h, i = false,
            n = a.extend({}, a.fn.ajaxFilter.defaults, d);

        function k() {
            g = a("#" + n.listingsContainerId);
            h = a('<div id="' + n.overlayId + '">' + n.overlayMessage + "</div>").css("opacity", 0);
            if (a("#" + n.overlayId).length < 1) {
                g.after(h)
            }
        }

        function o() {
            var z = a(window),
                w = a("#content"),
                v = a("#" + n.overlayLoaderId),
                y = (z.height() - (w.offset().top - z.scrollTop()) - v.height()) / 2,
                x = ((w.width() - z.scrollLeft()) - v.width()) / 2;
            v.css({
                position: "absolute",
                top: y + "px",
                left: x + "px"
            });
            a("#" + n.overlayId).stop(true, true).show().fadeTo(n.fadeSpeed, n.overlayOpacity)
        }

        function s() {
            if (a.cookie("view_format") === null) {
                a.cookie("view_format", a("#main_products_list_page").data("view"), {
                    expires: 31,
                    path: "/"
                })
            }
            a("#" + n.viewingFormatId).find("a#view_" + a.cookie("view_format")).addClass("current");
            a("#views [data-view='4'] span").text("Small").attr('title', 'Small').parent().next(".views_separator").hide();
        }

        function j(v, x) {
            var w = a(v);
            if (w.length > 0) {
                q = a.deparam.querystring(w.attr("href")).addFilter.replace(/ /g, "+");
                if (w.hasClass(n.appliedClass) && w.attr("id") !== "PRICE_SLIDER") {
                    w.removeClass(n.appliedClass);
                    if (w.parents("." + n.filterGroupClass).find("." + n.appliedClass).length === 0) {
                        w.parents("." + n.filterGroupClass).removeClass(n.appliedClass)
                    }
                } else {
                    w.addClass(n.appliedClass);
                    w.parents("." + n.filterGroupClass).addClass(n.appliedClass)
                } if (typeof (x) === "function") {
                    x()
                }
            }
        }

        function c() {
            var B = [],
                v, z = a("." + n.filterGroupClass + "." + n.appliedClass, m),
                I, w = [],
                F, x, C = "",
                G = /[0-9]+/g,
                H, E, D, A = false,
                y = 0;
            for (E = 0; E < z.length; E++) {
                H = a(z[E]);
                I = H.find("." + n.appliedClass + ":not(." + n.disabledClass + ")");
                w = [];
                for (D = 0; D < I.length; D++) {
                    if (I.attr("id") === "PRICE_SLIDER") {
                        C = "PRICE_SLIDER";
                        F = a("#min_price").html().match(G);
                        x = a("#max_price").html().match(G);
                        w.push(F + "-" + x)
                    } else {
                        v = a.deparam.querystring(a(I[D]).attr("href"));
                        w.push(v.filterValue);
                        C = v.addFilter.replace(/ /g, "+");
                        A = true
                    }
                }
                if (w.length > 0) {
                    B.push(C + "!" + w.join(","))
                }
            }
            return {
                filters: B.join("$"),
                page: 0
            }
        }

        function t(v) {
            if (!a(v).hasClass(n.disabledClass)) {
                o();
                i = true;
                j(v, function () {
                    a.bbq.pushState(c())
                })
            }
        }

        function u() {
            a("#" + n.overlayId).stop(true, true).fadeTo(n.fadeSpeed, 0).hide()
        }
        a.fn.ajaxFilter.priceSlider = function () {
            var x = parseInt(a("#min_price").text(), 10),
                y = parseInt(a("#max_price").text(), 10),
                v = 0,
                z = 0,
                w = 0;
            a("#sliders").slider({
                range: true,
                min: originalMinPrice,
                max: originalMaxPrice,
                values: [originalMinPrice, originalMaxPrice],
                slide: function (A, B) {
                    a("#min_price").text(B.values[0]);
                    a("#max_price").text(B.values[1])
                }
            });
            a("#min_price").text(a("#sliders").slider("values", 0));
            a("#max_price").text(a("#sliders").slider("values", 1));
            a("#sliders").bind("slidestart", function (A, B) {
                w = B.values[0];
                z = B.values[1]
            });
            a("#sliders").bind("slidestop", function (A, B) {
                if (w !== B.values[0] || z !== B.values[1]) {
                    t(a("#PRICE_SLIDER"))
                }
            })
        };

        function e(G) {
            var H, B, y, x, I, v, C, D = /[0-9]+/g,
                w, F, E, A, z;
            i = true;
            a("." + n.appliedClass, m).removeClass(n.appliedClass);
            if (a.deparam.fragment().filters !== undefined && a.deparam.fragment().filters !== "" && a.deparam.fragment().filters !== "!") {
                y = a.deparam.fragment().filters.split("$");
                for (A = 0; A < y.length; A++) {
                    F = y[A];
                    w = F.split("!");
                    x = w[1].split(",");
                    if (w.length === 2) {
                        for (z = 0; z < x.length; z++) {
                            E = x[z];
                            if (w[0].replace(/ /g, "+").indexOf("PRICE_SLIDER") > -1) {
                                C = a("#min_price").html().match(D);
                                I = E.replace(/ /g, "+").split("-");
                                H = I[0];
                                B = I[1];
                                v = 'a[href*="addFilter=' + w[0].replace(/ /g, "+") + "&filterValue=" + C;
                                a("#sliders").slider({
                                    values: [H, B]
                                });
                                a("#min_price").html(a("#sliders").slider("values", 0));
                                a("#max_price").html(a("#sliders").slider("values", 1))
                            } else {
                                v = 'a[href*="addFilter=' + w[0].replace(/ /g, "+") + "&filterValue=" + E.replace(/ /g, "+")
                            }
                            j(m.find(v + '"]').not(v + '.5"]'))
                        }
                    }
                }
            }
            if (typeof (G) === "function") {
                G()
            }
        }
        a.fn.ajaxFilter.generateAjaxUrl = function (v) {
            if (!v) {
                v = r + window.location.hash.replace("#", "")
            }
            if (v.indexOf("productsPerPage") > -1 && v.match(/productsPerPage/g).length > 1) {
                v = v.replace("productsPerPage", "originalProductsPerPage")
            }
            return v.replace(n.standardCategoryLayout, n.ajaxCategoryLayout).replace(n.standardSearchLayout, n.ajaxSearchLayout).replace(n.categoryPage, n.ajaxPage).replace(n.searchPage, n.ajaxPage) + "&loadCat=true&view=" + a.cookie("view_format")
        };

        function l(v, w) {
            v = a.fn.ajaxFilter.generateAjaxUrl(v);
            a.get(v, function (A) {
                var z, y, x = A.substring(A.indexOf('<div id="hidden-nextprev">') + 26, A.length);
                x = x.substring(0, x.indexOf("</div>"));
                if (A.indexOf(n.productClass) > -1) {
                    a("#" + n.contentContainerId).html(A).fadeIn(n.fadeSpeed, function () {
                        u();
                        setCategoryCookie()
                    });
                    if (q) {
                        a("." + n.filterGroupClass + ":not(#filter_" + q + ") a:not(.reset)", m).addClass(n.disabledClass)
                    }
                    for (z = 0; z < returnedFilters.length; z++) {
                        y = returnedFilters[z];
                        a('a[href*="' + y.url + '"]').removeClass(n.disabledClass).find(".count").html(y.count);
                    }
                    a("." + n.disabledClass, m).removeClass(n.appliedClass);
                    a("a:not(." + n.disabledClass + ",." + n.appliedClass + ",.reset)", m).addClass("filter");
                    a.cookie("lastSuccessfulCategory", window.location.href, {
                        expires: 31,
                        path: "/"
                    })
                } else {
                    if (a.browser.msie && A.indexOf(n.productDetailsIdentifier) > -1 && a.cookie("productRedirect") && a.cookie("lastSuccessfulCategory") && x === a.cookie("productRedirect")) {
                        location.replace(a.cookie("lastSuccessfulCategory"))
                    } else {
                        if (A.indexOf(n.productDetailsIdentifier) > -1) {
                            a("#" + n.overlayId).html(n.overlayRedirectProductDetails);
                            if (a.browser.msie) {
                                window.location = v
                            } else {
                                location.replace(v)
                            }
                        } else {
                            a("#" + n.overlayId).html(n.overlayRedirectBase);
                            location.replace(basePath)
                        }
                    }
                } if (typeof (w) === "function") {
                    w()
                }
                if (typeof (n.callback) === "function") {
                    n.callback()
                }
                s();
                // DZ
                awCoast.Category.init();
                a.cookie("productRedirect", null, {
                    expires: 31,
                    path: "/"
                })
            })
        }

        function b() {
            a.fn.ajaxFilter.priceSlider();
            k();
            var v = a.cookie("lastSuccessfulCategory");
            a("li.filter_group", m).hover(function () {
                var w = a(this),
                    x = a("ul", w);
                w.addClass("filter_hover");
                if (x.length > 0 && !x.data("positioned")) {
                    x.css({
                        left: -Math.floor((x.outerWidth() - w.outerWidth()) / 2)
                    }).data("positioned", true)
                }
            }, function () {
                if (!i) {
                    a(this).removeClass("filter_hover")
                }
            });
            a("a:not(.reset)", m).click(function () {
                t(this);
                return false
            }).keypress(function (w) {
                if (w.keyCode === 13) {
                    t(this)
                }
            });
            if (n.viewingFormatId !== "" || n.viewingFormatId !== undefined) {
                s();
                a("#" + n.listingsContainerId).delegate("#" + n.viewingFormatId + " a", "click", function () {
                    o();
                    a.cookie("view_format", a(this).data("view"), {
                        expires: 31,
                        path: "/"
                    });
                    l();
                    return false
                })
            }
            a("#" + n.listingsContainerId).delegate("ul." + n.paginationClass + " a", "click", function () {
                o();
                var w = a(this);
                a("#" + n.contentContainerId).fadeOut(n.fadeSpeed, function () {
                    var x = a.deparam.querystring(w.attr("href")).productsPerPage,
                        y = String(x);
                    if (a.deparam.querystring(w.attr("href")).page === undefined) {
                        if (y.indexOf(",") > -1) {
                            x = y.substring(0, y.indexOf(","))
                        }
                        p = {
                            productsPerPage: x,
                            page: 0
                        }
                    } else {
                        p = {
                            page: a.deparam.querystring(w.attr("href")).page
                        }
                    }
                    a.bbq.pushState(p)
                });
                return false
            });
            if (n.sortSelectId !== "") {
                a("#" + n.listingsContainerId).delegate("#" + n.sortSelectId, "change", function () {
                    a.bbq.pushState({
                        sortOrder: a("option:selected", a(this)).val(),
                        page: 0
                    })
                })
            }
            if (n.productsPerPageSelectId !== "") {
                a("#" + n.listingsContainerId).delegate("#" + n.productsPerPageSelectId, "change", function () {
                    a.bbq.pushState({
                        productsPerPage: a("option:selected", a(this)).val(),
                        page: 0
                    })
                })
            }
            a(window).bind("hashchange", function () {
                if (i === false && window.location.hash.indexOf("#") > -1) {
                    o();
                    e(function () {
                        l()
                    });
                    f = false
                } else {
                    if (i === true) {
                        l();
                        f = false
                    } else {
                        if (!f) {
                            a("." + n.appliedClass, m).removeClass(n.appliedClass);
                            o();
                            l(r);
                            a.fn.ajaxFilter.priceSlider()
                        }
                    }
                }
                i = false
            });
            a(window).trigger("hashchange");
            u();
            setCategoryCookie();
            if (v === null) {
                a.cookie("lastSuccessfulCategory", window.location.href, {
                    expires: 31,
                    path: "/"
                })
            }
        }
        b()
    };
    a.fn.ajaxFilter.defaults = {
        disabledClass: "disabled",
        appliedClass: "applied",
        productClass: "product",
        productDetailsIdentifier: "product_info",
        filterGroupClass: "filter_group",
        overlayId: "listings_overlay",
        listingsContainerId: "listings_container",
        contentContainerId: "listings_container",
        paginationClass: "pagination",
        sortSelectId: "sortby",
        productsPerPageSelectId: "products_per_page",
        standardCategoryLayout: "list.layout",
        ajaxCategoryLayout: "ajaxlist.layout",
        standardSearchLayout: "searchresults.layout",
        ajaxSearchLayout: "ajaxlist.layout",
        categoryPage: "ProductCategoryAttributeLink.ice",
        searchPage: "CatalogueSearch.ice",
        ajaxPage: "AJProductFiltering.ice",
        fadeSpeed: 250,
        overlayOpacity: 0.8,
        overlayMessage: '<img id="listings-ajax-loader" src="/pws/images/ajax-loader-listings.gif" alt="Loading..." />',
        overlayLoaderId: "listings-ajax-loader",
        overlayRedirectProductDetails: "<p>Found 1 product matching your criteria.</p><p>Loading product details page...</p>",
        overlayRedirectBase: "<p>Something went wrong, removing all filters...</p>",
        viewingFormatId: "views"
    }
}(jQuery));
/******* END OF ajaxFilter.js*******/ 

/******* START OF positionInCenter.js*******/ 
(function (a) {
    a.fn.positionInCenter = function (c) {
        var b = a(this),
            d = c.outerHeight() / 2,
            e = c.outerWidth() / 2;
        b.css({
            top: "50%",
            left: e + "px",
            "margin-top": "-" + (b.outerHeight() / 2) + "px",
            "margin-left": "-" + (b.outerWidth() / 2) + "px"
        });
        return
    }
}(jQuery));
/******* END OF positionInCenter.js*******/ 


/******* START OF popUps.js*******/ 
 (function (a) {
    a.fn.popUp = function (b) {
        var c;
        a.fn.popUp.initialise = function () {
            c = a.extend({}, a.fn.popUp.defaults, b);
            if (c.$popUpContainer.length < 1) {
                a("body").append(a('<div class="pop_up"></div>'));
                c.$popUpContainer = a("div.pop_up", "body")
            }
            c.$popUpContainer.delegate(".close", "click", function (d) {
                d.preventDefault();
                c.$popUpContainer.hide().empty().attr("id", "").addClass("loading");
                a("body").unblock()
            });
            return c.$triggers.live("click", function (d) {
                d.preventDefault();
                a("body").block();
                a(".fresca_block").css("height", $('html').outerHeight());
                a.fn.popUp.loadContent(a(this).attr("href"), c.callback);
            });
        };
        a.fn.popUp.loadContent = function (d, e) {
            a.ajax({
                url: d,
                beforeSend: function () {
                    c.$popUpContainer.positionInCenter(a(window));
                    c.$popUpContainer.fadeIn()
                },
                success: function (f) {
                    c.$popUpContainer.removeClass("loading").html(f).positionInCenter(a(window));

                    var img = a("#quickbuy_image"),
                        img_src = img.attr('src'),
                        src = img_src.replace("quickbuy", "list3");
                    img.removeAttr('width height');
                    img.attr('src', src);

                    // var str = "Visit Microsoft!";
                    // var res = str.replace("Microsoft", "W3Schools");
         

                    a("#size_key").detach().insertAfter("p:contains('Select Size')");
                    if (a(".close", c.$popUpContainer).length < 1) {
                        c.$popUpContainer.prepend('<span class="close"></span>')
                    }
                    // DZ // 
                    awCoast.Category.storeCheck();
                },
                error: function () {},
                complete: function () {
                    if (typeof e === "function") {
                        e()
                    }

                    // DZ // Remove text
                    a(".close").text(" ");
                }
            })
        };
        a.fn.popUp.attachQuickBuyHandlers = function () {
            a("label", "#select_size").click(function () {
                a(this).addClass("selected").find("radio").attr("checked", "checked");
                a(this).parent("li").siblings().find("label").removeClass("selected").find("radio").attr("checked", "")
            })
        };
        a.fn.popUp.defaults = {
            $triggers: a(this),
            $popUpContainer: a("div.pop_up", "body"),
            callback: function () {}
        };
        a.fn.popUp.initialise()
    }
}(jQuery));
/******* END OF popUps.js*******/ 



// DZ
// unwrapInner function
jQuery.fn.extend({
    unwrapInner: function(selector) {
        return this.each(function() {
            var t = this,
                c = $(t).children(selector);
            if (c.length === 1) {
                c.contents().appendTo(t);
                c.remove();
            }
        });
    }
});

 
jQuery(document).ready(function($) {
    pageSetup();

    $(function() {
        var wmodule = $('.module-poi').width();
        var mappedImages =  $("img[usemap]");
        mappedImages.each(function( index, img ) {
            var $img = $(img),
                $imgmap = $("<div class='imgmap'></div>"),
                imgheight = $img.height(),
                imgwidth = $img.width(),
                imgPosition = $img.position();
            $img.after($imgmap);
            $imgmap.css({
                top: imgPosition.top + "px",
                left: imgPosition.left + "px",
                height: imgheight + "px",
                width: imgwidth + "px"
            });
            
            var mapName = $img.attr("usemap").replace("#", "");

            var circles = $("map[name='" + mapName + "'] area[shape='circle']");
            circles.each(function( index, circle ) {
                var $circle = $(circle),
                    attrs = $circle.attr("coords").split(","),
                    cssclass = $circle.data('class'),
                    title = $circle.attr("alt"),
                    $newa = $("<a class='quick_buy_link mapcircle " + cssclass + "' href='/pws/AJProductDetails.ice?layout=quickbuy.pop.layout&ProductID=" + $circle.attr("href") + "' title='" + title + "'></a>");
                $imgmap.append($newa);
                var size = (attrs[2] * 2) + 'px';
                if ( wmodule <= 768 ) {
                    $newa.css({
                        left: ( attrs[0] / 1.6 ) + 'px',
                        top: ( attrs[1] / 1.6 ) + 'px',
                        width: size,
                        height: size
                    });
                } else if ( wmodule <= 1199 ) {
                    $newa.css({
                        left: ( attrs[0] / 1.2 ) + 'px',
                        top: ( attrs[1] / 1.2 ) + 'px',
                        width: size,
                        height: size
                    });
                } else {
                    $newa.css({
                        left: ( attrs[0] ) + 'px',
                        top: ( attrs[1] ) + 'px',
                        width: size,
                        height: size
                    });
                }
            });
        });
    });


    // Stiky nav
    $('#navigation').sticky({ topSpacing: 34 });
    $('#indexNav').sticky({ topSpacing: 44 });
	
	// Events slider
	$('.events-slider').bxSlider({
		pagerCustom: '.bx-pager',
		nextSelector: '#slider-next',
  		prevSelector: '#slider-prev',
  		nextText: '&#xe604;',
  		prevText: '&#xe603;',
        onSliderLoad: function(){
            $(".slider-inner").css("opacity", "1");
            $(".slider-inner .details").css({"opacity": "1", "margin-bottom": "-26px"});
        }
	});


    if($.cookie("view_format")==='3' || $.cookie("view_format")==='4'){
        // nothing
    }
    else {
        deleteCookie("view_format");
        $.cookie('view_format', '3', {expires: 365, path: '/'});
    }   
    $('#dept_occasion .with_pre').prepend('<li><span class="top_menu">Shop By Collection</span></li>');
    
    $('#navigation a.level_2').click(function(){
        var linkName = $(this).text();
        _gaq.push(['_trackEvent', 'Category', 'Clicked', linkName]);        
    }); 


    // Glossary
    var $glossary_group = $('.glossary .group'),
        $glossary_nav = $('#indexNav'),
        groupID,
        groupName;

    $glossary_group.each(function( index, el ) {
        groupID = $(el).attr('id');
        groupName = groupID.slice(-1);
        $glossary_nav.append('<li><a href="#' + groupID + '">' + groupName + '</a></li>');
    });
    
    $('#indexNav, .sp-nav').singlePageNav({
        offset: '150',
        updateHash: true
    });

    // return setTimeout(function() {
    // $('.stickit').sticky({
    //     // restrictWithin: parent,
    //     // inner_scrolling: false,
    //     offset: 90,
    //     // stack: 'section'
    // })
    // .on("sticky_kit:stick", function(e) {
    //     console.log($(this).parents('section'));
    // })
    // .on("sticky_kit:unstick", function(e) {
    //     // console.log("has unstuck!", e.target);
    // });
    // // }, 50);

    $('.module-guests-grid article').singlePageNav({
        offset: '60'
    });

    $(document).scroll(function () {
        var y = $(this).scrollTop();
        if (y > 400) {
            $('.page_controls').fadeIn();
        } else {
            $('.page_controls').fadeOut();
        }
    });

    $('.page_controls a').on('click', function(event) {
        event.preventDefault();
        $('body, html').animate({ scrollTop: 0 }, '500', 'swing');
    });


    // Hover effects
    $(".full:not(.visible)").hover(function() {
        $(this).find('.details-overlay').fadeIn(200);
    }, function() {
        $(this).find('.details-overlay').fadeOut(200);
    });

    awCoast.init();

    // Global
    awCoast.Global.init();


    // Remove extra markup
    $("button").unwrapInner("span");

	// Remove width / height from the images
	$('#content img').removeAttr('width').removeAttr('height');
	$('#filter_STARRATING').detach().insertAfter('#filter_MASTER_COLOUR');


    // Change Payment button text
    var amount = $("#card_payment_methods button em:eq(0)").text();
    $("#card_payment_methods button").text('Order & Pay ' + amount);

    // Change Login / Register layout
    $('#login_form').wrapAll('<div class="six columns lr-box"/>');
    $('#register_form').wrapAll('<div class="six columns lr-box"/>');
    $('.ly_loginregister .lr-box').wrapAll('<div class="row lr-row"/>')


    // Lookbook functionality
    var $lookbookItems = $('.look-item .large-view'),
        $productItem = $('.look-items li'),
        lookItemUrl = '/pws/AJProductDetails.ice?layout=quickbuy.pop.layout&ProductID=',
        lookPid;

    $productItem.each(function(index, el) {
        lookPid = $(this).data('product-id');

        if ( lookPid ) {
            $(this).find('.quick_buy_link').attr("href", lookItemUrl + lookPid);
        } else {
            $(this).remove();
        };
    });

    $('.look-item')
        .on('click', 'figure', function(event) {
            event.preventDefault();

            var $target = $(this).next('.large-view');

            if ( !$target.hasClass('opened') ) {
                $lookbookItems.removeClass('opened').fadeOut();
                $target.addClass('opened').fadeIn();
            }
        
        })
        .on('click', '.js-close', function(event) {
            event.preventDefault();

            $lookbookItems.removeClass('opened').fadeOut();
        });
    
    // $('.lookbook .look-item').on('click', function(event) {
    //     event.preventDefault();

    //     $(this).find('.large-view').fadeIn();
    //     $(this).find('.js-close').on('click', function(event) {
    //         event.preventDefault();
    //         $('.large-view').fadeOut();
    //     });
    // });

    // Reset filters reposition
    var $reset_filters = $('p.remove_all:eq(0)');
    $reset_filters.children('a').text('Reset all filters');
    $reset_filters.detach().prependTo('#sub_navigation');

    // Remove second one
    $('p.remove_all:eq(1)').remove();


    // Add SEO text at the bottom of category page
    var $more_info = $('.more-info-text');
    $more_info.appendTo('#main');
    setTimeout(function() {
        $more_info.css('visibility', 'visible');
    }, 2000);


    // Smooth scroll to element
    var $root = $('html, body');
    $('.more-info').click(function() {
        $root.animate({
            scrollTop: $( $.attr(this, 'href') ).offset().top
        }, 600);
        return false;
    });


    var lookbook_slider1 = $('#slider1').bxSlider({
        pagerCustom: $('#pager1'),
        controls: false,
        mode: 'fade'
    });

    var lookbook_slider2 = $('#slider2').bxSlider({
        pagerCustom: $('#pager2'),
        controls: false,
        mode: 'fade'
    });

    var lookbook_slider3 = $('#slider3').bxSlider({
        pagerCustom: $('#pager3'),
        controls: false,
        mode: 'fade'
    });

    // Pager
    var lookbook_slider_thumbs1 = $('#pager1').bxSlider({
        mode: 'vertical',
        minSlides: 4,
        maxSlides: 4,
        pager: false,
        preloadImages: 'all'
    });

    var lookbook_slider_thumbs2 = $('#pager2').bxSlider({
        mode: 'vertical',
        minSlides: 4,
        maxSlides: 4,
        pager: false,
        preloadImages: 'all'
    });

    var lookbook_slider_thumbs3 = $('#pager3').bxSlider({
        mode: 'vertical',
        minSlides: 4,
        maxSlides: 4,
        pager: false,
        preloadImages: 'all'
    });


    // Bridesmade lookbook tabs
    $('#tabs .tab-content').hide();
    $('#tabs .tab-content:first').show();
    $('.tabs li:first').addClass('active');
     
    $('.tabs a').click(function( event ) {
        event.preventDefault();

        $('#tabs ul li').removeClass('active');
        $(this).parent().addClass('active');
        var currentTab = $(this).attr('href');
        $('#tabs .tab-content').hide();
        $(currentTab).show();

        lookbook_slider1.reloadSlider();
        lookbook_slider2.reloadSlider();
        lookbook_slider3.reloadSlider();

        lookbook_slider_thumbs1.reloadSlider();
        lookbook_slider_thumbs2.reloadSlider();
        lookbook_slider_thumbs3.reloadSlider();
    });

});

$(function() {
    FastClick.attach(document.body);
});


function pageSetup() {

    var cmCreatePageviewTag = undefined;


    // Main navigation - Super menu
    var $super_menu = $('.super-menu'),
        // $main_menu = $('#navigation li.level_1'),
        $main_menu = $('#navigation ul span.level_1'),
        theNav = $('#navigation'),
        theNavHeight = theNav.height(),
        theMenuHeight = theNav.find('ul.level_2').outerHeight(),
        theImageBlockHeight = $('#top-nav-images').outerHeight(),
        thePosition = (theMenuHeight + theImageBlockHeight) - (theNavHeight + 16);

        // $('#top-nav-images').css({'top': thePosition});

    $main_menu.on({
        click: function (event) {
            event.preventDefault();
            event.stopPropagation();
            // $(this).parent().css({"overflow": "visible"}).addClass('active');
            $(this).parents('#navigation ul.level_1').toggleClass('active');
            $('body').toggleClass('body-overlay');
            
            // Supermenu thumbs
            $('#top-nav-images').toggle();
        }

        // Old Hover effect for posterity
        // click: function () {
        //     mouse_is_inside = false;
        //     // $(this).parent().css({"overflow": "hidden"}).removeClass('active');
        //     $(this).css({"overflow": "hidden"}).removeClass('active');
        //     $('body').removeClass('body-overlay');
        //     console.log(mouse_is_inside);
        // }
    });

    $(document).click(function() {
        /* Act on the event */
        $('body').removeClass('body-overlay');
        $('#navigation ul.level_1').removeClass('active');
        $('#top-nav-images').hide()
    });


    // Homepage video

    $('.video-placeholder').on('click', function(event) {
        $(this).hide().next().fadeIn();
    });
}




var country = countryISO;

function deleteCookie(name) {
    document.cookie = name+'="";-1; path=/';
}


function showNlPopup() {
    var $container = '<div class="nl-popup">' +
                        '<a href="javascript:;" class="icon-close"></a>' +
                        '<div class="module-newsletter">' +
                            '<div class="inner">' +
                                '<h2 class="title">LOVE</h2>' +
                                '<h3 class="subtitle">DRESSING UP?</h3>' +
                                '<p>Us too. Subscribe to our emails and stay in the fashion know. Look forward to the latest style updates, event invites and<br>sale alerts...</p>' +
                                '<form onsubmit="OnSubmitForm("subscribeFormCookie");" name="subscribeFormCookie" method="post" id="subscribeFormCookie" action="http://aurora.ed4.net/dualpost/index.cfm">' +
                                    '<input name="email" type="text" placeholder="Enter your email address here!">' +
                                    '<input type="submit" class="btn btn-brand" value="Submit">' +
                                    '<input type="hidden" name="action" value="signup">' +
                                    '<input type="hidden" name="brand" value="COAST">' +
                                    '<input type="hidden" name="redirect" value="http://www.coast-stores.com/fcp/content/newsletter/content">' +
                                '</form>' +
                            '</div>' +
                        '</div>' +
                    '</div>';

    $('body').block().find('.fresca_block').addClass('fresca_block_white');
    $('body').append($container);

    $('ul.level_1').hover(function() {
        return false;
    });

    $('.icon-close, .fresca_block').on('click', function(event) {
        event.preventDefault();
        $('.nl-popup').fadeOut(400);
        $('body').block().find('.fresca_block').remove();
    });
}

// awCoast // DZ
var awCoast = {


    init: function() {
        awCoast.Homepage.init();
        awCoast.Category.init();
        awCoast.Product.init();
        awCoast.Checkout.init();
    },
    // Homepage
    Homepage: {
        init: function() {
            awCoast.Homepage.slider(); 
            awCoast.Homepage.eventsSlider(); 
        },
        slider: function() {
            // Homepage slider v2
            var homeSlider = $('.hp-slider').bxSlider({
                nextSelector: '#slider-next',
                prevSelector: '#slider-prev',
                nextText: '&#xe604;',
                prevText: '&#xe603;',
                onSliderLoad: function() {
                    $(".slider-inner").css("opacity", "1");
                    $(".slider-inner .details").css({"opacity": "1", "margin-bottom": "-26px"});
                },
                onSlideAfter: function( $slideElement, oldIndex, newIndex ) {
                    if ( oldIndex == '0' && newIndex == '1' ) {
                        setTimeout(function() {
                            homeSlider.goToNextSlide();
                        }, 600);
                        
                    } else if ( oldIndex == '2' && newIndex == '1' ) {
                        setTimeout(function() {
                            homeSlider.goToPrevSlide();
                        }, 600);
					
                    };
					
					
					 if ( oldIndex == '2' && newIndex == '3' ) {
                        setTimeout(function() {
                            homeSlider.goToNextSlide();
                        }, 600);
                        
                    } else if ( oldIndex == '4' && newIndex == '3' ) {
                        setTimeout(function() {
                            homeSlider.goToPrevSlide();
                        }, 600);
					
                    };
					
					
                }
            });
        },
        eventsSlider: function() {
            // Homepage what's new carousel
            var eventsOpts = {
                minSlides: 6,
                maxSlides: 6,
                slideWidth: 200,
                slideMargin: 0,
                controls: false
            }
            $('.whats-new-slider').bxSlider(eventsOpts);
        }
    },

    Checkout: {
        init: function() {
            awCoast.Checkout.loginPageAdditionalInformation();
        },

        // A.Birdi - Adds * notifications to checkout page 
        loginPageAdditionalInformation: function() {
            var theURL = window.location.pathname;

            if (theURL === '/pws/secure/CheckOut.ice') {
                
                var theMarkUp = '<div id="additionalInformation" class="element"><div>*These fields are mandatory.</div><div>**Let us know your phone number in case we need to contact you about your order. This will enable us to get in touch with you as quickly as possible.</div>';

                $(theMarkUp).insertAfter('.element.mlist_element');
            }
        }
    },

    // Category
    Category: {
        init: function() {

            // Reverse the rating but keep the functionality intact
            // Remove text inside the rating elements
            $("#filter_STARRATING li a").text("");
            // var reverse_rating = $($("#filter_STARRATING li").get().reverse());
            var reverse_rating = $("#filter_STARRATING li");
            $('#filter_STARRATING ul').append(reverse_rating);

            $('#content li.product').each(function( index ) {
                $(this).find('.product_title').detach().insertAfter($(this).find('.product_price'));
                $(this).find('.product_tags').detach().insertAfter($(this).find('.star_ratings'));
            });


            // Remove image hovers
            $("#listings_container").find(".product").unbind('mouseenter mouseleave mouseover mouseout mousemove');

            // Get base URI
            if (!window.location.origin) {
                window.location.origin = window.location.protocol+"//"+window.location.host;
            }
            var product_url,
                product_img,
                product_title,
                product_price,
                basePath = window.location.origin;

            $('#content li.product').each(function( index, item ) {

                if ( $(this).find(".share-trigger").length === 0 ) {


                    product_url     = $(item).find('.product_link').attr('href');
                    product_img     = $(item).find('.product_image').length ? $(item).find('.product_image').attr('src') : '/pws/client/design2014/files/no-img.png';
                    product_title   = $(item).find('.product_title').text();
                    product_price   = $(item).find('.product_price .now').text();
                    product_desc    = $(item).find('.product_tag').text();

                    var share_elems = 
                        "<div class='social-wrapper'>" +
                        "<a onclick='window.open(this.href, \"Twitter\", \"width=640,height=300\"); return false;' href='http://twitter.com/home?status=" + product_title + "+" + product_url + "' class='icon-tw' target='_blank'></a>" +
                        "<a onclick='window.open(this.href, \"Facebook\", \"width=640,height=300\"); return false;' href='http://www.facebook.com/sharer/sharer.php?u=" + product_url + "&title=" + product_title + "' class='icon-fb' target='_blank'></a>" +
                        "<a onclick='window.open(this.href, \"Pinterest\", \"width=640,height=300\"); return false;' href='http://pinterest.com/pin/create/bookmarklet/?media=" + basePath + product_img + "&url=" + product_url + "&is_video=false&description=" + product_desc  + " " +  product_price + "' class='icon-pi' target='_blank'></a>" +
                        "<a onclick='window.open(this.href, \"Email\", \"width=640,height=300\"); return false;' href='mailto:?subject=" + product_title + "&body=Check out " + product_url + "' class='icon-email' target='_blank'></a>";

                    
                    $(item).find('.product_actions').append('<span class="share-trigger"><span class="icon-share"></span> <i>Share</i></span>');
                    $(item).find('.product_actions').append(share_elems);
                }
            });

            

            $('.product_actions .share-trigger').toggle(
                function () { 
                    $(this).find('.icon-share').addClass('icon-close').removeClass('icon-share').next('i').text('Close').parents('.product_actions').find('.social-wrapper').animate({ "left": "0" }, 300); 
                    $(this).parents('.product_actions').children('a').animate({ opacity: 0 }, 50); 
                },
                function () { 
                    $(this).find('.icon-close').addClass('icon-share').removeClass('icon-close').next('i').text('Share').parents('.product_actions').find('.social-wrapper').animate({ "left": "280px", opacity: 1 }, 300); 
                    $(this).parents('.product_actions').children('a').animate({ opacity: 1 }, 200); 
                }
            );


            // Filters
            var $filter = $('.ly_list'),
                $colourFilters = $('#filter_MASTER_COLOUR li'),
                $sizeFilters = $('#filter_AVAILABLE_SIZE li');
            if ( $filter.length >= 0 ) {
                $colourFilters.each(function( index, el ) {
                    $(this).find('.count').detach().prependTo($(this));
                });
                $('#filter_beiges_browns a').text(' Gold');
                $('#filter_black a').text(' Black');
                $('#filter_black').after($('#filter_mono'));    
                $('#filter_blues a').text(' Blue');
                $('#filter_blues').after($('#filter_beiges_browns'));
                $('#filter_greens a').text(' Green');
                $('#filter_greys a').text(' Grey');
                $('#filter_mono a').text(' Black & White');
                $('#filter_multi a').text(' Multi');
                $('#filter_naturals a').text(' Neutral');
                $('#filter_oranges a').text(' Orange');
                $('#filter_pinks a').text(' Pink');
                $('#filter_purples_lilacs a').text(' Purple');
                $('#filter_reds a').text(' Red');    
                $('#filter_white a').text(' White'); 
                $('#filter_yellows a').text(' Yellow');  
                $('#filter_other a').text(' Other');  
                $('#filter_metallics a').text(' Metalic');  
                $('#filter_2-3 a').text(' Age 2-3'); 
                $('#filter_3-4 a').text(' Age 3-4'); 
                $('#filter_4-5 a').text(' Age 4-5'); 
                $('#filter_5-6 a').text(' Age 5-6'); 
                $('#filter_6-7 a').text(' Age 6-7'); 
                $('#filter_7-8 a').text(' Age 7-8'); 

                if ( $('#filter_10').length > 0 ) {
                    $('#filter_8').insertBefore('#filter_10');
                    $('#filter_6').insertBefore('#filter_8');
                };


            //     $('#sub_navigation a.level_1').text('Shop by category');
                // $('.level_2 #sub_wc_dept_garments').after($('.level_2 #sub_wc_dept_trousers')).after($('.level_2 #sub_wc_dept_jackets')).after($('.level_2 #sub_wc_dept_cover-ups')).after($('.level_2 #sub_wc_dept_skirts')).after($('.level_2 #sub_wc_dept_all-tops')).after($('.level_2 #sub_wc_dept_all-dresses'));
                // $('select#sortby > option:last').remove();
            };
        },

        // DZ // Check in store form
        storeCheck: function() {
            var $popUpContainer = $(".pop_up"),
                checkinstore = '<form name="storeSearchForm" action="/pws/AJStoreLookup.ice" method="post" class="validate reserve_form" id="reserve_collect">' +
                                    '<input type="hidden" name="action" id="actionSearch" value="search">' +
                                    '<input type="hidden" name="sku" id="sku">' +
                                    '<input type="text" name="postcodeOrTown" id="postcodeOrTown" minlength="2" class="text validate" placeholder="Enter postcode or town">' +
                                    '<button type="submit" class="submit button small_button secondary">Check in store</button>' +
                            '</form>';

            $("#add_to_bag_and_checkout_button").remove();
            $("#quickbuy_form").append(checkinstore);
            $popUpContainer.append('<div class="results"></div>');


            var $sizeLabel = $("#select_size ul li").find('label');

            if ( $sizeLabel.children('input').length > 0  ) {
                $sizeLabel.click(function () {
                    var sku = $(this).children('input').attr("id");
                    sku = sku.replace("sku_", "");
                    $("#reserve_collect #sku").attr("value", sku);
                });
            };


            // Error messages
            $('#summary_warning').prepend('<i class="fa fa-times-circle m-r-s"></i>');


            $("#reserve_collect").submit(function ( event ) {
                event.preventDefault();

                if ( $("input:checked", "#select_size").length > 0 ) {
                    var data = $(this).serialize(),
                        xurl = "/pws/AJStoreLookup.ice";

                    $.ajax({
                        url: xurl,
                        data: data,
                        type: "POST"
                    })
                    .done(function (res) {

                        // Populate with the correct data
                        $(".results").html(res);

                        // Trigger popup reposition
                        $popUpContainer.positionInCenter($(window));
                    })
                    .always(function () {

                    })
                    .fail(function () {

                    })
                    .then(function () {
                        $(".notification").remove();
                    });

                } else {
                    if( $('.notification').length === 0 ) {
                        $("#quickbuy_form").append('<p class="notification warning"><i class="fa fa-times-circle m-r-s"></i> Please select a size</p>');
                    }
                }
            });
        }
    },

    // Product 
    Product: {
        init: function() {
            // Product blocks variables
            var product_details_title           = $('#product_information dt.product_specifics').detach().text(),
                product_details_content         = $('#product_information dd.product_specifics').detach().html(),
                product_delivery_title          = $('#product_bottom_information dt.delivery').detach().text(),
                product_delivery_content        = $('#product_bottom_information dd.delivery').detach().html(),
                product_description_title       = $('#product_information dt.description').detach().text(),
                product_description_content     = $('#product_information dd.description').detach().html();


            // setTimeout(function() {
                $('#additional_information').removeAttr('id').addClass('product-info-wrap').prepend(
                    '<div class="product-info">' +
                     '<div class="info"><h5>' + product_description_title + '</h5><p>' + product_description_content + '</p></div>' +
                    '<div class="info"><h5>' + product_details_title + '</h5>' + product_details_content + '</div>' +
                    '<div class="info"><h5>' + product_delivery_title + '</h5>' + product_delivery_content + '</div>' +
                   
                    '</div>'
                );
            // }, 700);

            // $('#additional_information dl#ymal').detach().insertAfter('#product_details').wrapAll('<div id="Product_AW14" class="related-products" />');
            var $relatedWrap = $('<div class="related-products" />').prepend('<h2 class="center-align">you may also like</h2>');
            var $related = $('<div id="Product_AW14" />');


            $relatedWrap.insertBefore('#BVReviewsContainer');
            $related.appendTo('.related-products');
            $('#additional_information dl#recently_viewed').remove();
            $('#additional_information dl#ymal').remove();

            // Related products prices
            // var lang_en = $('body').data('country', 'GB'),
            //     lang_us = $('body').data('country', 'US');


            // if ( lang_en ) {
            //     $('.ukPriceSpan').show();
            // } else if ( lang_us ) {
            //     $('.usPriceSpan').show();
            // } else {
            //     $('.iePriceSpan').show();
            // }


            // setTimeout(function() {
            //     if ( $('dl#ymal .products_list li').length === 0 ) {
            //         $('.related-products:eq(0)').hide();
            //     };
            // }, 100);

            $("#enlarge_button").on('click', function(event) {
                event.preventDefault();
                $('.related-products').hide();
            });

            $("#enlarged_image_container .close_button").on('click', function(event) {
                event.preventDefault();
                $('.related-products').show();
            });

            $('#product_scroll_imgs li').on('click', function(event) {
                return false;
            });

            $('#size_guide').detach().appendTo('#select_size').css('display', 'inline-block');


            var $check_wrapper = $('#reserve_collect');

            $check_wrapper.detach().appendTo( $('.button_set') );

            // Text changes
            $("#reserve_collect_form_items button span").text("Check in store");
            $("#enlarge_button").text("Enlarge Image");

            $("#size_key").detach().show().insertBefore("#select_size ul");
            $("#colour_variants").detach().show().insertAfter("#select_size");


            // FOR DEMO ONLY!!!!! Product rating 
            $('<span class="prod-rating"/>').insertBefore('#select_size');
            $('<a class="reviews-link" href="#BVReviewsContainer">Reviews <i class="fa fa-angle-right"></i></a>').insertAfter('.prod-rating');


            // Info popups
            setTimeout(function() {
                $('.sfs_delivery').hover(
                function() {
                    console.log('dadada');
                    $(this).parents('.stock_message').find('.sfs_delivery_popup').fadeIn();
                },
                function() {
                    $(this).parents('.stock_message').find('.sfs_delivery_popup').fadeOut();
                });

                $('.sfs_collect').hover(
                function() {
                    console.log('dadada');
                    $(this).parents('.stock_message').find('.sfs_collect_popup').fadeIn();
                },
                function() {
                    $(this).parents('.stock_message').find('.sfs_collect_popup').fadeOut();
                });
            }, 10);


            $('.sfs_collect_popup').removeClass('hide');
            $('.sfs_delivery_popup').removeClass('hide').html('<h3>Delivery from store</h3> <p> The following items are only available from store and will be delivered within 3-5 working days (excluding Bank Holidays and Sundays).</p>');

            // Product thumbs
            var $productThumbs = $('#alternative_images');

            $productThumbs.detach().insertAfter('#image_controls');
        }

    },

    Global: {
        init: function() {
            awCoast.Global.reserveAndCollect();
            awCoast.Global.searchTrigger();
            awCoast.Global.showCookieBar();
            awCoast.Global.signUpOverlayCookie();
            awCoast.Global.navHref();
            awCoast.Global.swapImg();

            // Model / Product view
            $('#model_or_product').prepend('<p>View</p>');
            $('#product_view').text('Product');
            $('#model_view').text('Model');
        },

        // Reserve and collect
        reserveAndCollect: function() {
            $('.sfs_popup.sfs_collect').hide();
            var str = $('.stock_message').html();
            if (typeof str != 'undefined'){
                var rep = str.replace('and', '');
                $('.stock_message').html(rep); 
            }
        },

        // Search trigger
        searchTrigger: function() {
            var body = $("html, body");

            // SLI Search form
            $('.js-search-trigger').on('click', function(event) {
                event.preventDefault();
                event.stopPropagation();
                if ( $('.sticky-wrapper').hasClass('is-sticky') ) {
                    body.animate({ scrollTop: 0 }, '500', 'swing');
                };
                $(this).parents('ul').removeClass('active');
                $(this).parents('ul').find('#top-nav-images').hide();
                $('#searchFormSLI').slideToggle(200);
                $('body').toggleClass('body-overlay-dark').removeClass('body-overlay');

                $("#searchFormSLI").find("[type=text]").focus();
            });

              $('#searchFormSLI [type="text"]').on('click', function(event) {
                event.preventDefault();
                event.stopPropagation();
            });

            $("#searchFormSLI").find("[type=text]").focus(function(event) {
                $(this).val('');
            });


            $('.close').on('click', function(event) {
                event.preventDefault();
                $('#searchFormSLI').slideUp(200);
                $('body').removeClass('body-overlay-dark');


            });


            $(document).click(function() {
                $('body').removeClass('body-overlay-dark');
                $('#searchFormSLI').slideUp(200);
            });
        },


        // Show cookie policy bar
        showCookieBar: function() {
            var hasVisited = $.cookie('cookieBar'),
            $container = '<div class="cookie-bar">' +
                                '<div class="row">' +
                                    '<div class="twelve columns">' +
                                        '<p>Cookies enhance your shopping experience so it’s tailored to you. <a href="#">Learn more here <i class="fa fa-angle-right m-l-s"></i></a></p>' +
                                            '<a href="javascript:;" class="close">Close <span class="icon-close m-l-s"></span></a>' +
                                    '</div>' +
                                '</div>' +
                            '</div>';  

            // Save it in the cookie
            if ( !hasVisited ) {
                $('#navigation').unstick();
                $('#navigation').sticky({ topSpacing: 70 });
                $('#page').addClass('cookie-opened').prepend($container);
                $('#topper').addClass('cookie-opened');
            };

            // Hide the bar
            $('.close').on('click', function(event) {
                event.preventDefault();
                
                $(this).parents('.cookie-bar').hide();
                $(this).parents('#page').removeClass('cookie-opened').find('#topper').removeClass('cookie-opened');

                $('#navigation').unstick();
                $('#navigation').sticky({ topSpacing: 34 });

                $.cookie('cookieBar', 'true', { path: '/', domain: '', expires: 30 });
            });

            // Help widget
            $('.close_chat').on('click', function(event) {
                event.preventDefault();
                
                $.cookie('helpWidget', 'true', { path: '/', domain: '', expires: 30 });
            });
        },

        signUpOverlayCookie: function() {
            var hasVisited = $.cookie('signUpOverlay');
            
            if ( hasVisited === 'true' ) {
                // do nothing
            } else {
                // Wait 20s before showing the popup
                setTimeout(function() {
                    showNlPopup();
                
                
                    // alert('You have not visited in the last 30 days, here is a sign up form!');
                    $.cookie('signUpOverlay', 'true', { path: '/', domain: '', expires: 30 });
                }, 20000);
            }
        },

        // Alter nav links
        navHref: function() {
            $('#wc_range_allevents a, #wc_dept_all-events a').attr('href', '/events');
            $('#wc_dept_asseeninthepress a').attr('href', '/as-seen-in-press');

            if ( window.location.href.toString().split(window.location.host)[1] == '/events' ) {
                $('#wc_range_allevents, #wc_dept_all-events a').addClass('selected');
            } else if ( window.location.href.toString().split(window.location.host)[1] == '/as-seen-in-press' ) {
                $('#wc_dept_asseeninthepress').addClass('selected');
            }
        },

        // Swap image on hover
        swapImg: function() {
            var $container = $('.swap-img'),
                $altImg = $('.alt-img'),
                $origImg = $('.original-img'),
                speed = 300;


            $container.hover(
            function() {
                $(this).find($origImg).stop().animate({ 'opacity': '0' }, speed);
                $(this).find($altImg).stop().animate({ 'opacity': '1' }, speed);
            },
            function() {
                $(this).find($origImg).stop().animate({ 'opacity': '1' }, speed);
                $(this).find($altImg).stop().animate({ 'opacity': '0' }, speed);
            });

        },

        // Default tabs
        defaultTabs: function() {
            var $tabsTrigger = $('.default-tabs a');
            $tabsTrigger.on('click', function( event ) {

                event.preventDefault();
                
                var tab = $(this).attr("href");
                $(this).parent().addClass("active");
                $(this).parent().siblings().removeClass("active");
                $(".tab-content").not(tab).css("display", "none");
                $(tab).fadeIn();
            });
        }
    }
}

function postScroll() {}


/* CM logging of error messages */
// if ($('#validation_errors').length){
//         var page_type2 = $('body').attr('class');
//         var pagetypearray2 = new Array();
//         pagetypearray2 = page_type2.split(' ');
//         var page2 = new Array();
//         var page2 = pagetypearray2[1].split('_');
//         var CMpageID2 = page2[1];
        
//         if (pathN.indexOf("AddPromotionCodeToBasket") !== -1){
//             if (getUrlVars()["code"].length){
//             cmCreateConversionEventTag ($('#validation_errors_content').text() + " Code = " + getUrlVars()["code"],"2","Error - " + CMpageID2,"20");    
//             }
//         }
//         else
//         {
//             if (ctry == "CA"){
//                 if ($('#validation_errors_content').text().search("there has been a problem processing the card specified") != -1){
//                         $('#validation_errors_content').html($('#validation_errors_content').html() + "<ul class='validationErrorsList'><li>For all orders to Canada your delivery and billing address must be in Canada. If you are trying to deliver to a different country please change to that country using the flag selector at the top of your screen</li></ul>")
//                 }
//             }
//         cmCreateConversionEventTag ($('#validation_errors_content').text(),"2","Error - " + CMpageID2,"20");
//         }
// }

// var sliJsHost = (("https:" == document.location.protocol) ? "https://"  : "http://" );
// document.write(unescape('%3Clink rel="stylesheet" type="text/css" href="' + sliJsHost + 'assets.resultspage.com/js/rac/sli-rac.0.4.css" /%3E'));
// document.write(unescape('%3Clink rel="stylesheet" type="text/css" href="' + sliJsHost + 'coast-stores.resultspage.com/rac/sli-rac.css" /%3E'));


// function categoryOverlayCTA_jeya() {
//  //   console.log('categoryOverlayCTA');
//     // 1. Define the campaign to avoid conflicts
//     var theCampaign = 'jeya'
//         // 2. Put your desired content here 
//     var theContent = '<li id="init_' + theCampaign + '_overlay" class="first custom-content product"><img src="/pws/client/images/jeya-cta.jpg" alt="Meet the JEYA DRESS" /></li>'
//         // 3. Put the content for the overlay here 
//     var theOverlayContent = '<img src="/pws/client/images/jeya-popup.jpg" alt="THE JEYA DRESS" style="height:auto;width:100%;" />'
//         // 4. Append the clickable image to the product list
//     $('#main_products_list_page').find('li.first').removeClass('first');
//     $('#main_products_list_page').find('li.last').removeClass('last').next().addClass('last');
//     $('#main_products_list_page').prepend(theContent);
//     // 5. Append the body with the overlay markup
//     $('body').append('<div id="" class="the_' + theCampaign + '_overlay the_campaign_overlay"></div><div class="the_' + theCampaign + '_overlay_content the_campaign_overlay_content">' + theOverlayContent + '</div>');
//     $('.the_campaign_overlay_content').append('<span class="the_campaign_overlay_close">X</span>');
//     // 6. Let the click function below do the rest
//     $('#init_jeya_overlay').on('click', function () {
//         $('div.the_campaign_overlay').css('z-index', '9998').show().animate({
//             opacity: 0.7
//         }, 500, function () {
//             $('div.the_campaign_overlay_content').css('z-index', '9998').show().animate({
//                 opacity: 1.0
//             }, 900);
//             campaign_overlayClose();
//         });
//     });

//     function campaign_overlayClose() {
//         $('.the_campaign_overlay_close, div.the_campaign_overlay').on('click', function () {
//             $('div.the_campaign_overlay, div.the_campaign_overlay_content').animate({
//                 opacity: 0
//             }, function () {
//                 $(this).css({
//                     'z-index': '-1'
//                 }).hide();
//             });
//         });
//     }
//     $(window).resize(function () {
//         var docH = $(document).height();
//         $('.the_campaign_overlay').css('height', docH);
//         var $el = $('.the_campaign_overlay_content');
//         $el.css({
//             left: ($('html,body').width() - $el.width()) / 2,
//         });
//     });
//     $(window).trigger('resize');
// }


// function categoryOverlayCTA_ascot() {
//  //   console.log('categoryOverlayCTA');
//     // 1. Define the campaign to avoid conflicts
//     var theCampaign = 'ascot'
//         // 2. Put your desired content here 
//     var theContent = '<li id="init_' + theCampaign + '_overlay" class="first custom-content product"><img src="/pws/client/images/ascot-cta.jpg" alt="Royal Ascot Rules" /></li>'
//         // 3. Put the content for the overlay here 
//     var theOverlayContent = '<img src="/pws/client/images/ascot-popup.jpg" alt="Royal Ascot" style="height:auto;width:100%;" />'
//         // 4. Append the clickable image to the product list
//     $('#main_products_list_page').find('li.first').removeClass('first');
//     $('#main_products_list_page').find('li.last').removeClass('last').next().addClass('last');
//     $('#main_products_list_page').prepend(theContent);
//     // 5. Append the body with the overlay markup
//     $('body').append('<div id="" class="the_' + theCampaign + '_overlay the_campaign_overlay"></div><div class="the_' + theCampaign + '_overlay_content the_campaign_overlay_content">' + theOverlayContent + '</div>');
//     $('.the_campaign_overlay_content').append('<span class="the_campaign_overlay_close">X</span>');
//     // 6. Let the click function below do the rest
//     $('#init_ascot_overlay').on('click', function () {
//         $('div.the_campaign_overlay').css('z-index', '9998').show().animate({
//             opacity: 0.7
//         }, 500, function () {
//             $('div.the_campaign_overlay_content').css('z-index', '9998').show().animate({
//                 opacity: 1.0
//             }, 900);
//             campaign_overlayClose();
//         });
//     });

//     function campaign_overlayClose() {
//         $('.the_campaign_overlay_close, div.the_campaign_overlay').on('click', function () {
//             $('div.the_campaign_overlay, div.the_campaign_overlay_content').animate({
//                 opacity: 0
//             }, function () {
//                 $(this).css({
//                     'z-index': '-1'
//                 }).hide();
//             });
//         });
//     }
//     $(window).resize(function () {
//         var docH = $(document).height();
//         $('.the_campaign_overlay').css('height', docH);
//         var $el = $('.the_campaign_overlay_content');
//         $el.css({
//             left: ($('html,body').width() - $el.width()) / 2,
//         });
//     });
//     $(window).trigger('resize');
// }

// function categoryOverlayCTA_petite() {
//  //   console.log('categoryOverlayCTA');
//     // 1. Define the campaign to avoid conflicts
//     var theCampaign = 'petite'
//         // 2. Put your desired content here 
//     var theContent = '<li id="init_' + theCampaign + '_overlay" class="first custom-content product"><img src="/pws/client/images/petite-cta.jpg" alt="Petite" /></li>'
//         // 3. Put the content for the overlay here 
//     var theOverlayContent = '<img src="/pws/client/images/petite-popup.jpg" alt="Royal Ascot" style="height:auto;width:100%;" />'
//         // 4. Append the clickable image to the product list
//     $('#main_products_list_page').find('li.first').removeClass('first');
//     $('#main_products_list_page').find('li.last').removeClass('last').next().addClass('last');
//     $('#main_products_list_page').prepend(theContent);
//     // 5. Append the body with the overlay markup
//     // $('body').append('<div id="" class="the_' + theCampaign + '_overlay the_campaign_overlay"></div><div class="the_' + theCampaign + '_overlay_content the_campaign_overlay_content">' + theOverlayContent + '</div>');
//     // $('.the_campaign_overlay_content').append('<span class="the_campaign_overlay_close">X</span>');
//     // 6. Let the click function below do the rest
//     $('#init_' + theCampaign + '_overlay').on('click', function () {
        
    
        
//         $('div.the_campaign_overlay').css('z-index', '9998').show().animate({
//             opacity: 0.7
//         }, 500, function () {
//             $('div.the_campaign_overlay_content').css('z-index', '9998').show().animate({
//                 opacity: 1.0
//             }, 900);
//             campaign_overlayClose();
            
//             $('.tabs .tab-links a').on('click', function(e)  {
                    
                    
                    
//                     var currentAttrValue = $(this).data('tab');
                
             
//                     // Show/Hide Tabs
//                     $('.tabs #'+currentAttrValue).show().siblings().hide();
//                     // console.log('#',currentAttrValue);
                
//                     // Change/remove current tab to active
//                     $(this).parent().addClass('active').siblings().removeClass('active');
//                     e.preventDefault();
                    
//                 });
                
            
//         });
//     });

//     function campaign_overlayClose() {
//         $('.the_campaign_overlay_close, div.the_campaign_overlay').on('click', function () {
//             $('div.the_campaign_overlay, div.the_campaign_overlay_content').animate({
//                 opacity: 0
//             }, function () {
//                 $(this).css({
//                     'z-index': '-1'
//                 }).hide();
                
                
                
                
                
//             });
//         });
//     }
//     $(window).resize(function () {
//         var docH = $(document).height();
//         $('.the_campaign_overlay').css('height', docH);
//         var $el = $('.the_campaign_overlay_content');
//         $el.css({
//             left: ($('html,body').width() - $el.width()) / 2,
//         });
//     });
//     $(window).trigger('resize');
// }


$(document).ready(function () {
    $.fn.popUp({
        $triggers: $("#listings_container .quick_buy_link, #quickbuy_main .product_link, .lookbook .quick_buy_link, .module-poi .quick_buy_link"),
        callback: function () {
            $.fn.popUp.attachQuickBuyHandlers();
            handleUpdateBasket($("#quickbuy_form"))
        }
    });
});
// $(document).ready(function () {
//     var theURL = window.location.pathname
//     if (theURL === '/race-day/dept/fcp-category/list') {
//         // categoryOverlayCTA_ascot();
//     }
//     if (theURL === '/petite/dept/fcp-category/list') {
//       // categoryOverlayCTA_petite();
//     }

//     if (theURL === '/garments/dept/fcp-category/list') {
//       // categoryOverlayCTA_jeya();
//     }
// });







function handleUpdateBasket(d) {
    var c = (d !== undefined ? d : $('form[action*="/UpdateBasket.ice"]'));
    if (c.length > 0) {
        $.each(c, function () {
            var a = $(this);
            a.attr("action", a.attr("action").replace("/UpdateBasket.ice", "/AJUpdateBasket.ice"));
            a.submit(function (b) {
                var e;
                b.preventDefault();
                $("#basket_response").remove();
                $.ajax({
                    url: a.attr("action") + "?" + a.serialize()
                }).done(function (f) {
                    $("#basket_update").html(f);
                    if ($("#add_to_bag_and_checkout_button.submitted").length > 0 && $("#summary_link").length > 0) {
                        window.location.href = $("#summary_link").find("a").attr("href")
                    }
                    loadMiniBasket(true)
                })
            });
            a.delegate(".add_to_bag_button", "click", function () {
                $(".add_to_bag_button", "#quickbuy_form").removeClass("submitted");
                $(this).addClass("submitted")
            })
        })
    }
}

function otherFormTitles() {
    $("#title").change(function () {
        if ($(this).val() === "Other") {
            $("#other_title_element").show()
        } else {
            $("#other_title_element").hide()
        }
    })
}

function qbInit() {
    $(".quickbuy_link").click(function () {
        var a = $(this).attr("href");
        $.ajax({
            url: a,
            cache: false,
            success: function (c, d, b) {
                if (typeof (c) !== "undefined" && (c !== "")) {
                    $('<div class="quickbuy_wrapper"></div>').appendTo("body");
                    qbWidth = $(".quickbuy_wrapper").width();
                    qbWidth = Math.round(qbWidth);
                    qbHeight = $(".quickbuy_wrapper").height();
                    qbheight = Math.round(qbHeight);
                    scrWidth = $(window).width();
                    scrHeight = $(window).height();
                    qbLeft = (scrWidth / 2) - (qbWidth / 2);
                    qbTop = (scrHeight / 2) - (qbheight / 2);
                    $(".quickbuy_wrapper").css("left", qbLeft);
                    $(".quickbuy_wrapper").css("top", qbTop);
                    $(".quickbuy_wrapper").html(c);
                    $(".quickbuy_wrapper .close").html("X");
                    $(".quickbuy_wrapper").fadeIn(500);
                    initSizes();
                    initClosePopup();
                    qbValidation()
                }
            }
        });
        return false
    })
}

function initSizes() {
    $("#select_size ul li input").click(function () {
        $("#select_size ul li input").parent().removeClass("selected");
        $(this).parent().addClass("selected")
    })
}

function initClosePopup() {
    $(".quickbuy_wrapper .close").click(function () {
        $(".quickbuy_wrapper").fadeOut(500, function () {
            $(this).remove()
        })
    })
}

function qbValidation() {
    $("#quickbuy_main #add_to_bag").click(function () {
        if ($("#select_size input:checked").length > 0) {
            $("#quickbuy_basket_response").empty();
            $("#basket_update").html("");
            var a = "/pws/AJUpdateBasket.ice";
            $.ajax({
                type: "POST",
                url: a,
                data: $("#quickbuy_form").serialize(),
                success: function (b) {
                    loadMiniBasket(true);
                    $("#basket_update").html("Added to basket")
                }
            })
        } else {
            $("#basket_update").html("Please select size")
        }
        return false
    })
};




// Product video
function onMediaComplete() {}

function onTemplateLoaded(id) {
    player = brightcove.getExperience(id);
    modVP = player.getModule(APIModules.VIDEO_PLAYER);
    player.getModule(APIModules.VIDEO_PLAYER).addEventListener(BCVideoEvent.VIDEO_COMPLETE, onMediaComplete);
}

if ( typeof UNIQUE_PRODUCT !== "undefined" ) {

    var ref = UNIQUE_PRODUCT;
    var vid_exists = "";

    var BCL = {};
        // Set the default token and handler for calls
        BCMAPI.token = "p-MXccoOfwzJqXEyAbkZM3YJ-4Ovpx3IrXwtJvksOtHH2pEH9Hxk5g..";
        BCMAPI.callback = "BCL.onSearchResponse";

        BCL.method = "find_video_by_reference_id";
        BCL.params = {};
        BCL.params.reference_id = ref;
        BCL.params.video_fields = "id";
        // With a token and callback function set, we can use find()'s params as a selector
        BCMAPI.find(BCL.method, BCL.params);

    BCL.onSearchResponse = function (jsonData) {

        // output request and response
        vid_exists = JSON.stringify(jsonData, null, 2);


        if ( vid_exists !== 'null' ) {

            if ( $('#playBtn').length === 0 ) {
                $('#image_controls').append('<li id="playBtn">| Watch Video</li>');
            };
            
            // Trigger the video player
            $('#playBtn').click(function() { playplayer(); });


            var player;
            var videodivcode;

            videodivcode = '<object id="myExperience" class="BrightcoveExperience"><param name="bgcolor" value="#FFFFFF" /><param name="width" value="360" /><param name="isVid" value="true" /><param name="height" value="480" /><param name="playerID" value="1761156172001" /><param name="isUI" value="true" /><param name="dynamicStreaming" value="true" /><param name="wmode" value="transparent"><param name="autoStart" value="true"><param name="htmlFallback" value="true" /><param name="@videoPlayer" value="ref:' + ref + '" /><param name="wmode" value="transparent" /></object>'

            function playplayer() {
                $('.alt_product_img:first').addClass('selected');
                $('div#video_popup').css('display','block');

                if (typeof (modVP) == 'undefined') {
                    if ( navigator.userAgent.indexOf('iPad') != -1 ) {
                        if ($('#myExperience').length > 0)
                            $('#myExperience').remove();
                    }          
                   $("#video_popup").html(videodivcode);            
                }
            
                if ((navigator.userAgent.indexOf('iPhone') != -1) || (navigator.userAgent.indexOf('iPod') != -1) || (navigator.userAgent.indexOf('iPad') != -1)) {
                    brightcove.createExperiences();
                } else {
                    if (typeof (modVP) !== 'undefined') {
                        modVP.loadVideo(ref, "referenceId");
                    } else {
                        brightcove.createExperiences();
                    }
                }
            }
        }
    }
}