$(document).ready(function(){

  setTimeout(function () { // We have to add a brief delay before we fire Flexslider after Colorbox has fired, otherwise it doesn't work.
     startFlexslider();
  }, 500);

  function startFlexslider() { // fire Flexslider
     $('.flexslider').flexslider({
     prevText: "",
     nextText: ""
     });
  }

// create a function that shows the current slide number (var index) and the total slides in the slide deck (var total)

  var showSlide = "<div id='show-slide'></div>"; // create a div to hold this information
  $(".slides").append(showSlide); // append it to the ul.slides element
  $("#show-slide").css({"position": "absolute", "bottom" : "10px", "right" : "10px", "z-index" : "2", "font-family" : "GuadelupePro", "font-size" : "20px", "color" : "#232323"}); // style it

  setInterval(function () { // Run this every 500ms
     getSlideNumber();
  }, 500);

  function getSlideNumber() { // define the function
  var index = $('li:has(.flex-active)').index('.flex-control-nav li') + 1; // get current slide (+ 1 as arrays start at 0)
  var total = $('.flex-control-nav li').length; // get number of li elements and hence number of slides

  $("#show-slide").html(index + "/" + total); // write this HTML to the #show-slide element we created earlier.
  };

});
