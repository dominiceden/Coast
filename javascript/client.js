/* Add capitalisation function for product title breadcrumbs */

function toTitleCase(productTitleUppercase) {
  return productTitleUppercase.replace(/\w\S*/g, function(txt){
    return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
  });
}

$(document).ready(function(){
  var productTitleUppercase = $('ol#crumbs li.last').text().trim();
  $('ol#crumbs li.last').text(toTitleCase(productTitleUppercase));
});

/* End capitalisation function */

function showNlPopup() {

    var $container = '<div class="nl-popup">' +

                        '<a href="javascript:;" class="icon-close"></a>' +

                        '<div class="module-newsletter">' +

                            '<div class="inner">' +

                                '<h2 class="title">LOVE</h2>' +

                                '<h3 class="subtitle">DRESSING UP?</h3>' +

                                '<p>Us too. Subscribe to our emails and stay in the fashion know. Look forward to the latest style updates, event invites and<br>sale alerts...</p>' +

                                '<form onsubmit="OnSubmitForm("subscribeFormCookie");" name="subscribeFormCookie" method="post" id="subscribeFormCookie" action="http://aurora.ed4.net/dualpost/index.cfm">' +

                                    '<input name="email" type="text" placeholder="Enter your email address here!">' +

                                    '<input type="submit" class="btn btn-brand" value="Submit">' +

                                    '<input type="hidden" name="action" value="signup">' +

                                    '<input type="hidden" name="brand" value="COAST">' +

                                    '<input type="hidden" name="redirect" value="http://www.coast-stores.com/fcp/content/newsletter/content">' +

                                '</form>' +

                            '</div>' +

                        '</div>' +

                    '</div>';



    $('body').block().find('.fresca_block').addClass('fresca_block_white');

    $('body').append($container);



    $('ul.level_1').hover(function() {

        return false;

    });



    $('.icon-close, .fresca_block').on('click', function(event) {

        event.preventDefault();

        $('.nl-popup').fadeOut(400);

        $('body').block().find('.fresca_block').remove();

    });

}



var utils = {



    init: function() {

        utils.Slider.init();

        utils.Category.init();

        utils.Category.socialIcons();

        utils.Product.init();

        utils.Checkout.init();

        utils.Global.init();

    },

    // Homepage

    Slider: {

        init: function() {

            utils.Slider.promoSlider();

            utils.Slider.productSlider();

        },

        promoSlider: function() {

            // Main promo slider on the homepage

            var homeSlider = $('.hp-slider').bxSlider({

                // Convert prev/next labels into Arrows.

                // nextSelector: '#slider-next',

                // prevSelector: '#slider-prev',

                // nextText: '&#xe604;',

                // prevText: '&#xe603;',

				controls: false,



                onSliderLoad: function() {

                    $(".slider-inner").css("opacity", "1");

                    $(".slider-inner .details").css({"opacity": "1", "margin-bottom": "-26px"});

                },

                onSlideAfter: function( $slideElement, oldIndex, newIndex ) {

                    if ( oldIndex == '0' && newIndex == '1' ) {

                        setTimeout(function() {

                            homeSlider.goToNextSlide();

                        }, 600);



                    } else if ( oldIndex == '2' && newIndex == '1' ) {

                        setTimeout(function() {

                            homeSlider.goToPrevSlide();

                        }, 600);



                    };





                     if ( oldIndex == '2' && newIndex == '3' ) {

                        setTimeout(function() {

                            homeSlider.goToNextSlide();

                        }, 600);



                    } else if ( oldIndex == '3' && newIndex == '2' ) {

                        setTimeout(function() {

                            homeSlider.goToPrevSlide();

                        }, 600);



                    };



                }

            });

        },

        productSlider: function() {

            // Product slider on homepage

            $('.whats-new-slider').bxSlider({
                minSlides: 6,
                maxSlides: 6,
                slideWidth: 180,
                slideMargin: 0,
        				nextText: "",
        				prevText: "",
        				auto: true
            });

        }

    },



    Checkout: {

        init: function() {

            utils.Checkout.loginPageAdditionalInformation();

        },



        // A.Birdi - Adds * notifications to checkout page

        loginPageAdditionalInformation: function() {

            var theURL = window.location.pathname;



            if (theURL === '/pws/secure/CheckOut.ice') {



                var theMarkUp = '<div id="additionalInformation" class="element"><div>*These fields are mandatory.</div><div>**Let us know your phone number in case we need to contact you about your order. This will enable us to get in touch with you as quickly as possible.</div>';



                $(theMarkUp).insertAfter('.element.mlist_element');

            }

        }

    },



    // Category

    Category: {

        init: function() {



            // Reverse the rating but keep the functionality intact

            // Remove text inside the rating elements

            $("#filter_STARRATING li a").text("");

            // var reverse_rating = $($("#filter_STARRATING li").get().reverse());

            var reverse_rating = $("#filter_STARRATING li");

            $('#filter_STARRATING ul').append(reverse_rating);



            $('#content li.product').each(function( index ) {

                $(this).find('.product_title').detach().insertAfter($(this).find('.product_price'));

                $(this).find('.product_tags').detach().insertAfter($(this).find('.star_ratings'));

            });





            // Remove image hovers

            $("#listings_container").find(".product").unbind('mouseenter mouseleave mouseover mouseout mousemove');



            // Get base URI

            if (!window.location.origin) {

                window.location.origin = window.location.protocol+"//"+window.location.host;

            }

            var product_url,

                product_img,

                product_title,

                product_price,

                basePath = window.location.origin;



            // Filters

            var $filter = $('.ly_list'),

                $colourFilters = $('#filter_MASTER_COLOUR li'),

                $sizeFilters = $('#filter_AVAILABLE_SIZE li');

            if ( $filter.length >= 0 ) {

                $colourFilters.each(function( index, el ) {

                    $(this).find('.count').detach().prependTo($(this));

                });

                $('#filter_beiges_browns a').text(' Gold');

                $('#filter_black a').text(' Black');

                $('#filter_black').after($('#filter_mono'));

                $('#filter_blues a').text(' Blue');

                $('#filter_blues').after($('#filter_beiges_browns'));

                $('#filter_greens a').text(' Green');

                $('#filter_greys a').text(' Grey');

                $('#filter_mono a').text(' Black & White');

                $('#filter_multi a').text(' Multi');

                $('#filter_naturals a').text(' Neutral');

                $('#filter_oranges a').text(' Orange');

                $('#filter_pinks a').text(' Pink');

                $('#filter_purples_lilacs a').text(' Purple');

                $('#filter_reds a').text(' Red');

                $('#filter_white a').text(' White');

                $('#filter_yellows a').text(' Yellow');

                $('#filter_other a').text(' Other');

                $('#filter_metallics a').text(' Metalic');

                $('#filter_2-3 a').text(' Age 2-3');

                $('#filter_3-4 a').text(' Age 3-4');

                $('#filter_4-5 a').text(' Age 4-5');

                $('#filter_5-6 a').text(' Age 5-6');

                $('#filter_6-7 a').text(' Age 6-7');

                $('#filter_7-8 a').text(' Age 7-8');



                if ( $('#filter_10').length > 0 ) {

                    $('#filter_8').insertBefore('#filter_10');

                    $('#filter_6').insertBefore('#filter_8');

                };





            //     $('#sub_navigation a.level_1').text('Shop by category');

                // $('.level_2 #sub_wc_dept_garments').after($('.level_2 #sub_wc_dept_trousers')).after($('.level_2 #sub_wc_dept_jackets')).after($('.level_2 #sub_wc_dept_cover-ups')).after($('.level_2 #sub_wc_dept_skirts')).after($('.level_2 #sub_wc_dept_all-tops')).after($('.level_2 #sub_wc_dept_all-dresses'));

                // $('select#sortby > option:last').remove();

            };

        },



        // Attach click event to toggle social icons

        socialIcons: function() {

            $('.product-actions .share-trigger').unbind();

            $('.product-actions .share-trigger').click(function() {

                var isOpen = $(this).find('.icon-share');

                if (isOpen.length > 0) {

                    $(this).find('.icon-share').addClass('icon-close').removeClass('icon-share').next('i').text('Close').parents('.product-actions').find('.social-wrapper').animate({ "left": "0" }, 300);

                    $(this).parent().find('.product-actions-info').animate({ opacity: 0 }, 50);

                } else {

                    $(this).find('.icon-close').addClass('icon-share').removeClass('icon-close').next('i').text('Share').parents('.product-actions').find('.social-wrapper').animate({ "left": "280px", opacity: 1 }, 300);

                    $(this).parent().find('.product-actions-info').animate({ opacity: 1 }, 200);

                }

                return false;

            });

        },



        // DZ // Check in store form

        storeCheck: function() {

            var $popUpContainer = $(".pop_up"),

                checkinstore = '<form name="storeSearchForm" action="/pws/AJStoreLookup.ice" method="post" class="validate reserve_form" id="reserve_collect">' +

                                    '<input type="hidden" name="action" id="actionSearch" value="search">' +

                                    '<input type="hidden" name="sku" id="sku">' +

                                    '<input type="text" name="postcodeOrTown" id="postcodeOrTown" minlength="2" class="text validate" placeholder="Enter postcode or town">' +

                                    '<button type="submit" class="submit button small_button secondary">Check in store</button>' +

                            '</form>';



            $("#add_to_bag_and_checkout_button").remove();

            $("#quickbuy_form").append(checkinstore);

            $popUpContainer.append('<div class="results"></div>');





            var $sizeLabel = $("#select_size ul li").find('label');



            if ( $sizeLabel.children('input').length > 0  ) {

                $sizeLabel.click(function () {

                    var sku = $(this).children('input').attr("id");

                    sku = sku.replace("sku_", "");

                    $("#reserve_collect #sku").attr("value", sku);

                });

            };





            // Error messages

            $('#summary_warning').prepend('<i class="fa fa-times-circle m-r-s"></i>');





            $("#reserve_collect").submit(function ( event ) {

                event.preventDefault();



                if ( $("input:checked", "#select_size").length > 0 ) {

                    var data = $(this).serialize(),

                        xurl = "/pws/AJStoreLookup.ice";



                    $.ajax({

                        url: xurl,

                        data: data,

                        type: "POST"

                    })

                    .done(function (res) {



                        // Populate with the correct data

                        $(".results").html(res);



                        // Trigger popup reposition

                        $popUpContainer.positionInCenter($(window));

                    })

                    .always(function () {



                    })

                    .fail(function () {



                    })

                    .then(function () {

                        $(".notification").remove();

                    });



                } else {

                    if( $('.notification').length === 0 ) {

                        $("#quickbuy_form").append('<p class="notification warning"><i class="fa fa-times-circle m-r-s"></i> Please select a size</p>');

                    }

                }

            });

        }

    },



    // Product

    Product: {

        init: function() {

            // Product blocks variables

            var product_details_title           = $('#product_information dt.product_specifics').detach().text(),

                product_details_content         = $('#product_information dd.product_specifics').detach().html(),

                product_delivery_title          = $('#product_bottom_information dt.delivery').detach().text(),

                product_delivery_content        = $('#product_bottom_information dd.delivery').detach().html(),

                product_description_title       = $('#product_information dt.description').detach().text(),

                product_description_content     = $('#product_information dd.description').detach().html();





            // setTimeout(function() {

                $('#additional_information').removeAttr('id').addClass('product-info-wrap').prepend(

                    '<div class="product-info">' +

                     '<div class="info"><h5>' + product_description_title + '</h5><p>' + product_description_content + '</p></div>' +

                    '<div class="info"><h5>' + product_details_title + '</h5>' + product_details_content + '</div>' +

                    '<div class="info"><h5>' + product_delivery_title + '</h5>' + product_delivery_content + '</div>' +



                    '</div>'

                );

            // }, 700);



            // $('#additional_information dl#ymal').detach().insertAfter('#product_details').wrapAll('<div id="Product_AW14" class="related-products" />');

            $('.cms-lower').show();

            var $relatedWrap = $('<div class="related-products" />').prepend('<h2 class="center-align">you may also like</h2>');

            var $related = $('<div id="Product_AW14" />');





            $relatedWrap.insertBefore('#BVReviewsContainer');

            $related.appendTo('.related-products');

            $('#additional_information dl#recently_viewed').remove();

            $('#additional_information dl#ymal').remove();



            // Related products prices

            // var lang_en = $('body').data('country', 'GB'),

            //     lang_us = $('body').data('country', 'US');





            // if ( lang_en ) {

            //     $('.ukPriceSpan').show();

            // } else if ( lang_us ) {

            //     $('.usPriceSpan').show();

            // } else {

            //     $('.iePriceSpan').show();

            // }





            // setTimeout(function() {

            //     if ( $('dl#ymal .products_list li').length === 0 ) {

            //         $('.related-products:eq(0)').hide();

            //     };

            // }, 100);



            $("#enlarge_button").on('click', function(event) {

                event.preventDefault();

                $('.related-products').hide();

            });



            $("#enlarged_image_container .close_button").on('click', function(event) {

                event.preventDefault();

                $('.related-products').show();

            });



            $('#product_scroll_imgs li').on('click', function(event) {

                return false;

            });



            var $check_wrapper = $('#reserve_collect');



            $check_wrapper.detach().appendTo( $('.button_set') );



            // Text changes

            $("#reserve_collect_form_items button span").text("Check in store");

            $("#enlarge_button").text("Enlarge Image");



            $("#size_key").detach().show().insertBefore("#select_size ul");

            $("#colour_variants").detach().show().insertAfter("#select_size");





            // FOR DEMO ONLY!!!!! Product rating

            $('<span class="prod-rating"/>').insertBefore('#select_size');

            $('<a class="reviews-link" href="#BVReviewsContainer">Reviews <i class="fa fa-angle-right"></i></a>').insertAfter('.prod-rating');





            // Info popups

            setTimeout(function() {

                $('.sfs_delivery').hover(

                function() {

                    console.log('dadada');

                    $(this).parents('.stock_message').find('.sfs_delivery_popup').fadeIn();

                },

                function() {

                    $(this).parents('.stock_message').find('.sfs_delivery_popup').fadeOut();

                });



                $('.sfs_collect').hover(

                function() {

                    console.log('dadada');

                    $(this).parents('.stock_message').find('.sfs_collect_popup').fadeIn();

                },

                function() {

                    $(this).parents('.stock_message').find('.sfs_collect_popup').fadeOut();

                });

            }, 10);





            $('.sfs_collect_popup').removeClass('hide');

            $('.sfs_delivery_popup').removeClass('hide').html('<h3>Delivery from store</h3> <p> The following items are only available from store and will be delivered within 3-5 working days (excluding Bank Holidays and Sundays).</p>');



            // Product thumbs

            var $productThumbs = $('#alternative_images');



            $productThumbs.detach().insertAfter('#image_controls');

        }



    },



    Global: {

        init: function() {

            utils.Global.reserveAndCollect();

            utils.Global.searchTrigger();

            utils.Global.showCookieBar();

            utils.Global.signUpOverlayCookie();

            utils.Global.navHref();

            utils.Global.swapImg();



            // Model / Product view

            $('#model_or_product').prepend('<p>View</p>');

            $('#product_view').text('Product');

            $('#model_view').text('Model');

        },



        // Reserve and collect

        reserveAndCollect: function() {

            $('.sfs_popup.sfs_collect').hide();

            var str = $('.stock_message').html();

            if (typeof str != 'undefined'){

                var rep = str.replace('and', '');

                $('.stock_message').html(rep);

            }

        },



        // Search trigger

        searchTrigger: function() {

            var body = $("html, body");



            // SLI Search form

            $('.js-search-trigger').on('click', function(event) {

                event.preventDefault();

                event.stopPropagation();

                if ( $('.sticky-wrapper').hasClass('is-sticky') ) {

                    body.animate({ scrollTop: 0 }, '500', 'swing');

                };

                $(this).parents('ul').removeClass('active');

                $(this).parents('ul').find('#top-nav-images').hide();

                $('#searchFormSLI').slideToggle(200);

                $('body').toggleClass('body-overlay-dark').removeClass('body-overlay');



                $("#searchFormSLI").find("[type=text]").focus();

            });



              $('#searchFormSLI [type="text"]').on('click', function(event) {

                event.preventDefault();

                event.stopPropagation();

            });



            $("#searchFormSLI").find("[type=text]").focus(function(event) {

                $(this).val('');

            });





            $('.close').on('click', function(event) {

                event.preventDefault();

                $('#searchFormSLI').slideUp(200);

                $('body').removeClass('body-overlay-dark');





            });





            $(document).click(function() {

                $('body').removeClass('body-overlay-dark');

                $('#searchFormSLI').slideUp(200);

                $('#top-nav-images').hide();

            });

        },





        // Show cookie policy bar

        showCookieBar: function() {

            var hasVisited = $.cookie('cookieBar'),

            $container = '<div class="cookie-bar">' +

                                '<div class="row">' +

                                    '<div class="twelve columns">' +

                                        '<p>Cookies enhance your shopping experience so it’s tailored to you. <a href="#">Learn more here <i class="fa fa-angle-right m-l-s"></i></a></p>' +

                                            '<a href="javascript:;" class="close">Close <span class="icon-close m-l-s"></span></a>' +

                                    '</div>' +

                                '</div>' +

                            '</div>';



            // Save it in the cookie

            if ( !hasVisited ) {

                $('.nav-container').unstick();

                $('.nav-container').sticky({ topSpacing: 70 });

                $('#page').addClass('cookie-opened').prepend($container);

                $('.masthead').addClass('cookie-opened');

            }



            // Hide the bar

            $('.close').on('click', function(event) {

                event.preventDefault();



                $(this).parents('.cookie-bar').hide();

                $(this).parents('#page').removeClass('cookie-opened').find('.masthead').removeClass('cookie-opened');



                $('.nav-container').unstick();

                $('.nav-container').sticky({ topSpacing: 34 });



                $.cookie('cookieBar', 'true', { path: '/', domain: '', expires: 30 });

            });



            // Help widget

            $('.close_chat').on('click', function(event) {

                event.preventDefault();



                $.cookie('helpWidget', 'true', { path: '/', domain: '', expires: 30 });

            });

        },



        signUpOverlayCookie: function() {

            var hasVisited = $.cookie('signUpOverlay');



            if ( hasVisited === 'true' ) {

                // do nothing

            } else {

                // Wait 20s before showing the popup

                setTimeout(function() {

                showNlPopup();



                    // alert('You have not visited in the last 30 days, here is a sign up form!');

                    $.cookie('signUpOverlay', 'true', { path: '/', domain: '', expires: 30 });

                }, 20000);

            }

        },



        // Alter nav links

        navHref: function() {

          //  $('#wc_range_allevents a, #wc_dept_all-events a').attr('href', '/events');

          //  $('#wc_dept_asseeninthepress a').attr('href', '/as-seen-in-press');



            if ( window.location.href.toString().split(window.location.host)[1] == '/events' ) {

                $('#wc_range_allevents, #wc_dept_all-events a').addClass('selected');

            } else if ( window.location.href.toString().split(window.location.host)[1] == '/as-seen-in-press' ) {

                $('#wc_dept_asseeninthepress').addClass('selected');

            }

        },



        // Swap image on hover

        swapImg: function() {

            var $container = $('.swap-img'),

                $altImg = $('.alt-img'),

                $origImg = $('.original-img'),

                speed = 300;





            $container.hover(

            function() {

                $(this).find($origImg).stop().animate({ 'opacity': '0' }, speed);

                $(this).find($altImg).stop().animate({ 'opacity': '1' }, speed);

            },

            function() {

                $(this).find($origImg).stop().animate({ 'opacity': '1' }, speed);

                $(this).find($altImg).stop().animate({ 'opacity': '0' }, speed);

            });



        },



        // Default tabs

        defaultTabs: function() {

            var $tabsTrigger = $('.default-tabs a');

            $tabsTrigger.on('click', function( event ) {



                event.preventDefault();



                var tab = $(this).attr("href");

                $(this).parent().addClass("active");

                $(this).parent().siblings().removeClass("active");

                $(".tab-content").not(tab).css("display", "none");

                $(tab).fadeIn();

            });

        }

    }

}



// // Product video

// function onMediaComplete() {}



function onTemplateLoaded(id) {

    player = brightcove.getExperience(id);

    modVP = player.getModule(APIModules.VIDEO_PLAYER);

    player.getModule(APIModules.VIDEO_PLAYER).addEventListener(BCVideoEvent.VIDEO_COMPLETE, onMediaComplete);

}



if ( typeof UNIQUE_PRODUCT !== "undefined" ) {



    var ref = UNIQUE_PRODUCT;

    var vid_exists = "";



    var BCL = {};

        // Set the default token and handler for calls

        BCMAPI.token = "p-MXccoOfwzJqXEyAbkZM3YJ-4Ovpx3IrXwtJvksOtHH2pEH9Hxk5g..";

        BCMAPI.callback = "BCL.onSearchResponse";



        BCL.method = "find_video_by_reference_id";

        BCL.params = {};

        BCL.params.reference_id = ref;

        BCL.params.video_fields = "id";

        // With a token and callback function set, we can use find()'s params as a selector

        BCMAPI.find(BCL.method, BCL.params);



    BCL.onSearchResponse = function (jsonData) {



        // output request and response

        vid_exists = JSON.stringify(jsonData, null, 2);





        if ( vid_exists !== 'null' ) {



            if ( $('#playBtn').length === 0 ) {

                $('#image_controls').append('<li id="playBtn">| Watch Video</li>');

            };



            // Trigger the video player

            $('#playBtn').click(function() { playplayer(); });





            var player;

            var videodivcode;



            videodivcode = '<object id="myExperience" class="BrightcoveExperience"><param name="bgcolor" value="#FFFFFF" /><param name="width" value="360" /><param name="isVid" value="true" /><param name="height" value="480" /><param name="playerID" value="1761156172001" /><param name="isUI" value="true" /><param name="dynamicStreaming" value="true" /><param name="wmode" value="transparent"><param name="autoStart" value="true"><param name="htmlFallback" value="true" /><param name="@videoPlayer" value="ref:' + ref + '" /><param name="wmode" value="transparent" /></object>'



            function playplayer() {

                $('.alt_product_img:first').addClass('selected');

                $('div#video_popup').css('display','block');



                if (typeof (modVP) == 'undefined') {

                    if ( navigator.userAgent.indexOf('iPad') != -1 ) {

                        if ($('#myExperience').length > 0)

                            $('#myExperience').remove();

                    }

                   $("#video_popup").html(videodivcode);

                }



                if ((navigator.userAgent.indexOf('iPhone') != -1) || (navigator.userAgent.indexOf('iPod') != -1) || (navigator.userAgent.indexOf('iPad') != -1)) {

                    brightcove.createExperiences();

                } else {

                    if (typeof (modVP) !== 'undefined') {

                        modVP.loadVideo(ref, "referenceId");

                    } else {

                        brightcove.createExperiences();

                    }

                }

            }

        }

    }

};





$(document).ready(function() {

    // Hover effects

    $(".full:not(.visible)").hover(function() {

        $(this).find('.details-overlay').fadeIn(200);

    }, function() {

        $(this).find('.details-overlay').fadeOut(200);

    });



    // Homepage video

    $('.video-placeholder').on('click', function(event) {

        $(this).hide().next().fadeIn();

    });



    // Add btn class to footer signup form button

    $('.footer_signup_submit').addClass('btn');





    utils.init();





    // Add SEO text at the bottom of category page

    var $more_info = $('.more-info-text');

    $more_info.appendTo('#main');

    setTimeout(function() {

        $more_info.css('visibility', 'visible');

    }, 2000);



});



//CST-3083

$(document).ready(function(){

if ($('#apply_promo').length > 0 && $('#promo_code').length > 0) {
  $('#apply_promo').attr('action', '/pws/AddPromotionCodeToBasket.ice');
  $('#promo_code').attr('name', 'code');
 }



    var $mobilegiftvoucher_payment = $('.mobilegiftvoucher-payment'),

        $giftcard_payment = $('.giftcard-payment'),

        $gift_card = $('.gift-card'),

        $mobile_voucher = $('.mobile-voucher'),

        $card_payment = $('li.card-payment'),

        $paypal_payment = $('li.paypal-payment'),

        $giftcard_payment = $('li.giftcard-payment');



    if ($('html').data('country') === 'US' || $('html').data('country') === 'AU' ) {

        $mobilegiftvoucher_payment.hide();

        $giftcard_payment.hide();

        $gift_card.hide();

        $mobile_voucher.hide();

        $card_payment.removeClass('col-sm-3').addClass('col-sm-6');

        $paypal_payment.removeClass('col-sm-3').addClass('col-sm-6');

    }

    if ($('html').data('country') === 'IE' ) {

        $mobilegiftvoucher_payment.hide();

        $mobile_voucher.hide();

        $card_payment.removeClass('col-sm-3').addClass('col-sm-4');

        $paypal_payment.removeClass('col-sm-3').addClass('col-sm-4');

        $giftcard_payment.removeClass('col-sm-3').addClass('col-sm-4');

    }

});



// Cross domain fix

(function ($) {



    /** Check File Status

     *

     * @param fileLocation string - the file to be checked

     * @return returnValue - will either be a single valid file or false if the url doesn't exist.

     */

    $.fn.checkFileStatus = function (fileLocation) {

        var returnValue;

        fileLocation = fileLocation.replace('http://media.coast-stores.com', '');



        $.ajax({

            url: fileLocation,

            type: 'HEAD',

            async: false,

            error: function (xhr, status, errorThrown) {

                returnValue = false;

            },

            success: function () {

                returnValue = fileLocation;

            }

        });



        return returnValue;

    };



}(jQuery));







// AJAX Postcode lookup fix, proper POST request

btf.forms.postcodeLookup = (function () {

    var $postcodeSubmit = $('#postcode-lookup-submit');



    // Build URL's for search or fetch

    function ajaxLookup(type, checkoutAddressMode, searchAddressId, searchPostcode) {

        var returnVal;

        if (type === 'search') {

            // Postcode search request

            returnVal = 'CreateAddress.ice?fromCreateAddress=true&search=true&isSearchPostcode=true&searchPostcode=' + searchPostcode + '&country=' + $('#ctry').val() + '&searchBuilding=' + $('#searchBuilding').val() + '&OWASP_CSRFTOKEN=' + $('[name="OWASP_CSRFTOKEN"]').val();

        } else {

            // Postcode fetch request

            returnVal = 'CreateAddress.ice?fromCreateAddress=true&search=true&isSearchPostcode=false&searchAddressId=' + searchAddressId + '&postcodeAddressID=' + searchAddressId + '&country=' + $('#ctry').val() + '&OWASP_CSRFTOKEN=' + $('[name="OWASP_CSRFTOKEN"]').val();

        }

        return returnVal;

    }



    // Assign returned data to form fields

    function assignData(data) {

        var address1 = !data.address1 ? $(data).find('#address1').val() : data.address1,

                address2 = !data.address2 ? $(data).find('#address2').val() : data.address2,

                address3 = !data.address3 ? $(data).find('#address3').val() : data.address3,

                towncity = !data.towncity ? $(data).find('#towncity').val() : data.towncity,

                county = !data.county ? $(data).find('#county').val() : data.county,

                postcode = !data.postcode ? $(data).find('#postcode').val() : data.postcode;



        $('#address1').val(address1);

        $('#address2').val(address2);

        $('#address3').val(address3);

        $('#towncity').val(towncity);

        $('#county').val(county);

        $('#postcode').val(postcode);

    }



    // Send address ID and return actual address details

    function postcodeFetch() {

        $('#postcodeResultID').unbind('click').click(function () {

            var ajaxData = ajaxLookup('fetch', 'postcodeFetch', $(this).val(), '');

            $.post('CreateAddress.ice', ajaxData.replace('CreateAddress.ice?', ''), function (data) {

                assignData(data);

            });

            return false;

        });

    }



    // First submission for postcode search

    $postcodeSubmit.unbind('click').click(function (e) {

        var ajaxData = ajaxLookup('search', 'postcodeSearch', '', $('#searchPostcode').val());

        $.post('CreateAddress.ice', ajaxData.replace('CreateAddress.ice?', ''), function (data) {

            $('#postcode-result').remove();

            $('.postcode-error').remove();

            if ((data.addressIdList !== "undefined" && data.addressIdList !== null) || (data.address && data.address.address1)) {

                if ($('#searchBuilding').val() === '') {

                    $('#postcode-lookup').after(Mustache.render(btf.tpl.postcodeLookupAddresses, data));

                } else {

                    assignData(data.address);

                }

                postcodeFetch();

            } else {

                $('#searchPostcode').after('<p class="postcode-error">Please enter a valid postcode to search</p>');

            }

        });

        e.preventDefault();

    });

}());



// Product page wishlist click trigger fix

$(document).ready(function(){

    if ($('body').hasClass('ly_productdetails')) {

        $(document).on('click', '.wishlist', function() {

            if($('.product-wishlist').hasClass('collapse')) {

                $('.product-wishlist').removeClass('collapse');

            } else {

                $('.product-wishlist').addClass('collapse');

            }

        });

    }

})